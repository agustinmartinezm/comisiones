<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes(); //Las rutas estan en vendor\laravel\framework\src\Illuminate\Routing\Router.php

Route::get('/vue', 'vueController@index')->name('vue');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/programarSesion', 'Sesiones\ProgramarSesionController@index')->name('programarSesion');

Route::post('/guardarSesion', 'Sesiones\ProgramarSesionController@guardarSesion')->name('guardarSesion');

Route::post('/CambiarContrasena', 'CambiarContrasenaController@updatePassword')->name('CambiarContrasena');

Route::get('seguimientoSesion/consultaSesion', 'Sesiones\SeguimientoSesionController@consultaSesion')->name('seguimientoSesion.consultaSesion');
Route::get('seguimientoSesion/acciones/{id_tsesion}/{id_puntoOrdenDia}/{num_punto}/{bandera}', 'Sesiones\SeguimientoSesionController@acciones')->name('seguimientoSesion.acciones');

Route::get('seguimientoSesion/downloadSoporte/{ruta}', 'Sesiones\SeguimientoSesionController@downloadSoporte')->name('seguimientoSesion.downloadSoporte');

Route::post('seguimientoSesion/editaPunto', 'Sesiones\SeguimientoSesionController@editaPunto')->name('seguimientoSesion.editaPunto');

Route::get('seguimientoSesion/eliminaArchivo/{id_tsoporte}', 'Sesiones\SeguimientoSesionController@eliminaArchivo')->name('seguimientoSesion.eliminaArchivo');

Route::get('seguimientoSesion/liberar', 'Sesiones\SeguimientoSesionController@liberar')->name('seguimientoSesion.liberar'); 

Route::resource('seguimientoSesion', 'Sesiones\SeguimientoSesionController');

Route::get('/generateWordOrdenDia', 'Sesiones\generarWordOrdenDia@createWordDocx')->name('generateWordOrdenDia');

Route::get('EditarSesiones', 'Sesiones\EditarSesionesController@index')->name('EditarSesiones');

Route::post('editarSesion', 'Sesiones\EditarSesionesController@editarSesion')->name('editarSesion');

Route::get('consultarSesion', 'Sesiones\ConsultarSesionesController@index')->name('consultarSesion');

Route::get('consultaSesion/downloadSoporte/{ruta}', 'Sesiones\ConsultarSesionesController@downloadSoporte')->name('consultaSesion.downloadSoporte');

Route::post('acuerdoSesion/capturaAcuerdo', 'Sesiones\AcuerdosSesionController@capturaAcuerdo')->name('acuerdoSesion.capturaAcuerdo');

Route::post('acuerdoSesion/capturaObservacion', 'Sesiones\AcuerdosSesionController@capturaObservacion')->name('acuerdoSesion.capturaObservacion');
Route::post('acuerdoSesion/guardaObservacion', 'Sesiones\AcuerdosSesionController@guardaObservacion')->name('acuerdoSesion.guardaObservacion');

Route::get('acuerdoSesion/liberarMinuta', 'Sesiones\AcuerdosSesionController@liberarMinuta')->name('acuerdoSesion.liberarMinuta');
Route::get('acuerdoSesion/liberarObserva', 'Sesiones\AcuerdosSesionController@liberarObserva')->name('acuerdoSesion.liberarObserva');

Route::resource('acuerdoSesion', 'Sesiones\AcuerdosSesionController');

Route::get('/createMinutaWordDocx', 'Sesiones\createMinutaWordDocx@createMinutaWordDocx')->name('createMinutaWordDocx');
Route::get('/info', 'Sesiones\info@index')->name('info');


Route::post('CerrarSesion', 'Sesiones\EditarSesionesController@CerrarSesion')->name('CerrarSesion');

Route::post('ConsutaSesionFinal', 'Sesiones\ConsultarSesionesController@ConsutaSesionFinal')->name('ConsutaSesionFinal');

Route::post('downloadMinuta', 'Sesiones\ConsultarSesionesController@downloadMinuta')->name('downloadMinuta');

Route::get('comentariosSesion/{id}', 'Sesiones\ComentariosSesionController@index')->name('comentariosSesion');

Route::post('guardaComentarioSesion', 'Sesiones\ComentariosSesionController@guardaComentario')->name('guardaComentarioSesion');

Route::get('eliminaComentarioSesion', 'Sesiones\ComentariosSesionController@eliminaComentario')->name('eliminaComentarioSesion');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/manuales','ManualesController@index')->name('manuales');
Route::get('muestraEnvioApi', 'Sesiones\ConsumoApi@index')->name('muestraEnvioApi');
Route::post('enviaSesionApi', 'Sesiones\ConsumoApi@enviaSesion')->name('enviaSesionApi');
Route::post('buscador', 'Buscador@buscar')->name('buscador');
Route::get('/admUsuarios', 'AdmUsuarios@index')->name('admUsuarios');
Route::get('/usuariosDeshabilitados', 'AdmUsuarios@usuariosDeshabilitadosIndex')->name('usuariosDeshabilitados');
Route::post('/habilitarUsuario', 'AdmUsuarios@habilitarUsuario')->name('habilitarUsuario');
Route::post('/eliminarUsuario', 'AdmUsuarios@eliminarUsuario')->name('eliminarUsuario');
Route::post('/consultaUsuario', 'AdmUsuarios@consultaUsuarios')->name('consultaUsuario');
Route::post('/resetearContrasenia', 'AdmUsuarios@resetearContrasenia')->name('resetearContrasenia');

Route::post('editarUsuario','AdmUsuarios@editarUsuario')->name('editarUsuario');

Route::post('update','AdmUsuarios@update')->name('update');

Route::get('consultar','Sesiones\EditarSesionesController@consultar')->name('consultar');//Consulta de representate 

Route::get('crear','Sesiones\EditarSesionesController@crear')->name('crear');//Crear representate

Route::get('gene','Sesiones\EditarSesionesController@gene')->name('gene');//Devolver datos de crear

// Route::get('editar','Sesiones\EditarSesionesController@editar')->name('editar');//Editar Rrepresentante usuario

Route::get('Eliminar','Sesiones\EditarSesionesController@Eliminar')->name('Eliminar');//Eliminar representate

Route::get('consultapri','Sesiones\EditarSesionesController@consultapri')->name('consultapri');//Usuarios y sus privilegios

Route::get('EditarPrivilegios','Sesiones\EditarSesionesController@EditarPrivilegios')->name('EditarPrivilegios'); // Editar la parte del usuario

Route::get('CrearPri','Sesiones\EditarSesionesController@CrearPri')->name('CrearPri');

Route::get('Insertpri','Sesiones\EditarSesionesController@Insertpri')->name('Insertpri');

Route::get('Updatepri','Sesiones\EditarSesionesController@Updatepri')->name('Updatepri');

Route::get('EliminarPrivilegios','Sesiones\EditarSesionesController@EliminarPrivilegios')->name('EliminarPrivilegios'); //Updatepri