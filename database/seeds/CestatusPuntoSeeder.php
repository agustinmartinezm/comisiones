<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CestatusPuntoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cestatus_puntos')->insert([
            'descripcion'  => 'Capturado',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_puntos')->insert([
            'descripcion'  => 'Liberado',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_puntos')->insert([
            'descripcion'  => 'Eliminado',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

    }
}
