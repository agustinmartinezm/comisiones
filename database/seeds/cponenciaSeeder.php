<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Modelos\Catalogos\Cponencia;

class cponenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cponencias')->insert([
            'id_cponencia' => 0,
            'descripcion'  => 'Presidencia',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 1,
            'descripcion'  => 'Ponencia 1',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 2,
            'descripcion'  => 'Ponencia 2',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 3,
            'descripcion'  => 'Ponencia 3',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 4,
            'descripcion'  => 'Ponencia 4',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 5,
            'descripcion'  => 'Ponencia 5',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 6,
            'descripcion'  => 'Ponencia 6',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 7,
            'descripcion'  => 'Secretaría General',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cponencias')->insert([
            'id_cponencia' => 8,
            'descripcion'  => 'Gestión Tecnológica',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //factory(Cponencia::class, 5)->create();
    }
}
