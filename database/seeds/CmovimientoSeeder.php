<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CmovimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cmovimiento')->insert([
            'id_cmovimiento' =>1,
            'descripcion'    =>'Orden del día Liberada por',
            'estatus' 		 =>1,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
        ]);

        DB::table('cmovimiento')->insert([
            'id_cmovimiento' => 2,
            'descripcion'    => 'Minuta Liberada para Observaciones por',
            'estatus'        => 1,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
        ]);

        DB::table('cmovimiento')->insert([
            'id_cmovimiento' => 3,
            'descripcion'    => 'Observaciones Liberadas por ',
            'estatus'        => 1,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
        ]);

        DB::table('cmovimiento')->insert([
            'id_cmovimiento' => 4,
            'descripcion'    => 'Observaciones Liberadas para Secretaría General',
            'estatus'        => 1,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
        ]);

        DB::table('cmovimiento')->insert([
            'id_cmovimiento' => 5,
            'descripcion'    => 'Minuta Liberada',
            'estatus'        => 1,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
        ]);

        DB::table('cmovimiento')->insert([
            'id_cmovimiento' => 6,
            'descripcion'    => 'Comentario Agregado por ',
            'estatus'        => 1,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
        ]);
    }
}
