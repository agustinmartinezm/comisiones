<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CtipoSesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ctipos_sesiones')->insert([
            'descripcion'  => 'Ordinaria',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ctipos_sesiones')->insert([
            'descripcion'  => 'Extraordinaria',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
    }
}
