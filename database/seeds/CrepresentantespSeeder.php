<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CrepresentantespSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('crepresentantesp')->insert([
            'id_cponencia' => 0,
            'id_ccomision' => 1,
            'titulo'       => 'Lic.',
            'nombre'       => 'Representante',
            'paterno'      => 'Presidencia',
            'materno'      => 'Presidencia',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);*/


        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 2,
            'id_ccomision' => 1,
            'titulo'       => 'Dr.',
            'nombre'       => 'Jorge',
            'paterno'      => 'Martínez',
            'materno'      => 'Arreguín',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 2,
            'id_ccomision' => 5,
            'titulo'       => 'Dr.',
            'nombre'       => 'Jorge',
            'paterno'      => 'Martínez',
            'materno'      => 'Arreguín',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 2,
            'id_ccomision' => 6,
            'titulo'       => 'Dr.',
            'nombre'       => 'Jorge',
            'paterno'      => 'Martínez',
            'materno'      => 'Arreguín',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);


        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 4,
            'id_ccomision' => 1,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Ana Yadira',
            'paterno'      => 'Alarcón',
            'materno'      => 'Márquez',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 4,
            'id_ccomision' => 2,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Ana Yadira',
            'paterno'      => 'Alarcón',
            'materno'      => 'Márquez',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 4,
            'id_ccomision' => 3,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Ana Yadira',
            'paterno'      => 'Alarcón',
            'materno'      => 'Márquez',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 4,
            'id_ccomision' => 6,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Ana Yadira',
            'paterno'      => 'Alarcón',
            'materno'      => 'Márquez',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 4,
            'id_ccomision' => 7,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Ana Yadira',
            'paterno'      => 'Alarcón',
            'materno'      => 'Márquez',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 5,
            'id_ccomision' => 2,
            'titulo'       => 'Dra.',
            'nombre'       => 'Blanca Estela del Rosario',
            'paterno'      => 'Zamudio',
            'materno'      => 'Valdés',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 5,
            'id_ccomision' => 3,
            'titulo'       => 'Dra.',
            'nombre'       => 'Blanca Estela del Rosario',
            'paterno'      => 'Zamudio',
            'materno'      => 'Valdés',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 5,
            'id_ccomision' => 4,
            'titulo'       => 'Dra.',
            'nombre'       => 'Blanca Estela del Rosario',
            'paterno'      => 'Zamudio',
            'materno'      => 'Valdés',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 5,
            'id_ccomision' => 5,
            'titulo'       => 'Dra.',
            'nombre'       => 'Blanca Estela del Rosario',
            'paterno'      => 'Zamudio',
            'materno'      => 'Valdés',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 5,
            'id_ccomision' => 6,
            'titulo'       => 'Dra.',
            'nombre'       => 'Blanca Estela del Rosario',
            'paterno'      => 'Zamudio',
            'materno'      => 'Valdés',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 6,
            'id_ccomision' => 4,
            'titulo'       => 'Dr.',
            'nombre'       => 'Miguel',
            'paterno'      => 'Arroyo',
            'materno'      => 'Martínez',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 6,
            'id_ccomision' => 7,
            'titulo'       => 'Dr.',
            'nombre'       => 'Miguel',
            'paterno'      => 'Arroyo',
            'materno'      => 'Martínez',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 8,
            'id_ccomision' => 6,
            'titulo'       => 'Ing.',
            'nombre'       => 'Federico',
            'paterno'      => 'Vargas',
            'materno'      => 'Ortiz',
            'sexo'         => 'M',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        

    }
}
