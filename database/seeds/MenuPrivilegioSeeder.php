<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MenuPrivilegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_privilegios')->insert([
            'id_menuModulo' => 1,
            'id_crol'       => 1,
            'estatus'       => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('menu_privilegios')->insert([
            'id_menuModulo' => 2,
            'id_crol'       => 1,
            'estatus'       => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('menu_privilegios')->insert([
            'id_menuModulo' => 2,
            'id_crol'       => 2,
            'estatus'       => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('menu_privilegios')->insert([
            'id_menuModulo' => 2,
            'id_crol'       => 3,
            'estatus'       => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
    }
}
