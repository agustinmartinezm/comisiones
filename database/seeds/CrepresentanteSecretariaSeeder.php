<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CrepresentanteSecretariaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 1,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 2,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 3,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 4,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 5,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 6,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('crepresentantesp')->insert([
            'id_cponencia' => 7,
            'id_ccomision' => 7,
            'titulo'       => 'Mtra.',
            'nombre'       => 'Zaira Liliana',
            'paterno'      => 'Jiménez',
            'materno'      => 'Seade',
            'sexo'         => 'F',
            'estatus'      => 1,
            'firma'        => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
    }
}
