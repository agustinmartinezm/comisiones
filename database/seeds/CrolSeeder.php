<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CrolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('croles')->insert([
            'descripcion' => 'SECRETARIA TECNICA',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now(),
        ]);

        DB::table('croles')->insert([
            'descripcion' => 'ADMINISTRADOR',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now(),
        ]);

        DB::table('croles')->insert([
            'descripcion' => 'CONSULTA',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now(),
        ]);
    }
}
