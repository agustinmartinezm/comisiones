<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CesatusSesionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cestatus_sesiones')->insert([
            'descripcion'  => 'Capturada',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_sesiones')->insert([
            'descripcion'  => 'Orden Día Liberada',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_sesiones')->insert([
            'descripcion'  => 'Acuerdos Liberados',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_sesiones')->insert([
            'descripcion'  => 'Observaciones Liberadas',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_sesiones')->insert([
            'descripcion'  => 'Cerrada',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('cestatus_sesiones')->insert([
            'descripcion'  => 'Cancelada',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
    }
}
