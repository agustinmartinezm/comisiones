<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsuarioPrivilegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('usuario_privilegios')->insert([
            'id'            => 3,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 3,
            'id_ccomision'  => 7,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);*/
        //PONENCIA 2
        //Comisiones MIGUEL 
        DB::table('usuario_privilegios')->insert([
            'id'            => 5,
            'id_ccomision'  => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones FERNANDO
        DB::table('usuario_privilegios')->insert([
            'id'            => 6,
            'id_ccomision'  => 4,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones ALBERTO
        DB::table('usuario_privilegios')->insert([
            'id'            => 7,
            'id_ccomision'  => 5,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones PILAR
        DB::table('usuario_privilegios')->insert([
            'id'            => 8,
            'id_ccomision'  => 5,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 8,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones MARCO ANTONIO
        DB::table('usuario_privilegios')->insert([
            'id'            => 9,
            'id_ccomision'  => 6,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones DANIEL
        DB::table('usuario_privilegios')->insert([
            'id'            => 10,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //PONENCIA 4
        //Comisiones JAVIER 
        DB::table('usuario_privilegios')->insert([
            'id'            => 11,
            'id_ccomision'  => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 11,
            'id_ccomision'  => 2,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 11,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 11,
            'id_ccomision'  => 6,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 11,
            'id_ccomision'  => 7,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones VICTOR MIGUEL 
        DB::table('usuario_privilegios')->insert([
            'id'            => 12,
            'id_ccomision'  => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 12,
            'id_ccomision'  => 2,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 12,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 12,
            'id_ccomision'  => 6,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 12,
            'id_ccomision'  => 7,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones JORGE IVÁN
        DB::table('usuario_privilegios')->insert([
            'id'            => 13,
            'id_ccomision'  => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 13,
            'id_ccomision'  => 2,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 13,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 13,
            'id_ccomision'  => 6,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 13,
            'id_ccomision'  => 7,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //PONENCIA 5
        //Comisiones SILVIA
        DB::table('usuario_privilegios')->insert([
            'id'            => 14,
            'id_ccomision'  => 5,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones JUAN RENATO 
        DB::table('usuario_privilegios')->insert([
            'id'            => 15,
            'id_ccomision'  => 2,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 15,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 15,
            'id_ccomision'  => 4,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 15,
            'id_ccomision'  => 6,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //Comisiones SANDRA 
        DB::table('usuario_privilegios')->insert([
            'id'            => 16,
            'id_ccomision'  => 2,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 16,
            'id_ccomision'  => 3,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 16,
            'id_ccomision'  => 4,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 16,
            'id_ccomision'  => 5,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 16,
            'id_ccomision'  => 6,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        //PONENCIA 6
        //Comisiones JOSÉ ROGELIO
        DB::table('usuario_privilegios')->insert([
            'id'            => 17,
            'id_ccomision'  => 4,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('usuario_privilegios')->insert([
            'id'            => 17,
            'id_ccomision'  => 7,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
    }
}
