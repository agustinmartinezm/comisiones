<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CsedeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('csedes')->insert([
            'descripcion'  => 'Juárez No. 8',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('csedes')->insert([
            'descripcion'  => 'Niños Héroes 119',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('csedes')->insert([
            'descripcion'  => 'Niños Héroes 132',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('csedes')->insert([
            'descripcion'  => 'Niños Héroes 150',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

    }
}