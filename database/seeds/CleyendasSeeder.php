<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CleyendasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tleyendas')->insert([
            'leyenda'  => '“El Poder Judicial de la Ciudad de México a la vanguardia en los Juicios Orales”',
            'estatus'      => 1,
            'anio_leyenda'  => 2018,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tleyendas')->insert([
            'leyenda'  => '“El Poder Judicial de la CDMX, Órgano Democrático de Gobierno”',
            'estatus'      => 1,
            'anio_leyenda'  => 2019,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);


    }
}
