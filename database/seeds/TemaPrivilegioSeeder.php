<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TemaPrivilegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Temas para la Comisión de Vigilancia del Instituto de Ciencias Forenses 
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 1,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 1,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //Temas para la Comisión de Protección Civil
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 2,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 2,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 2,
            'id_ctema'     => 3,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 2,
            'id_ctema'     => 4,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //Temas para la Comisión de Seguridad
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 3,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 3,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 3,
            'id_ctema'     => 6,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //Temas para la Comisión de Medios Alternos
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 4,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 4,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //Temas para la Comisión de Seguridad de los CENDI
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 5,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 5,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //Temas para la Comisión de Seguimiento a la Implementación de Tecnología Judicial
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 6,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 6,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 6,
            'id_ctema'     => 5,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        //Temas para la Comisión de Seguimiento de Asuntos Jurídicos del Consejo de la Judicatura de la Ciudad de México (Acuerdo 41-14/2017)
        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 7,
            'id_ctema'     => 1,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('tema_privilegios')->insert([
            'id_ccomision' => 7,
            'id_ctema'     => 2,
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

    }
}
