<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(cponenciaSeeder::class);
    	$this->call(CrolSeeder::class);
        $this->call(MenuModuloSeeder::class);
        $this->call(MenuPrivilegioSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CcaracterSesionSeeder::class);
        $this->call(CtipoSesionSeeder::class);
        $this->call(CsedeSeeder::class);
        $this->call(CcomisionSeeder::class);
        $this->call(CesatusSesionesSeeder::class);
        $this->call(CtemaSeeder::class);
        $this->call(CestatusPuntoSeeder::class);
        $this->call(CrepresentantespSeeder::class);
        $this->call(UsuarioPrivilegioSeeder::class);
        $this->call(TemaPrivilegioSeeder::class);       
        $this->call(CmovimientoSeeder::class);
        $this->call(CleyendasSeeder::class);
        $this->call(CleyendasSeeder::class);
        $this->call(CrepresentanteSecretariaSeeder::class);

        // $this->call(UsersTableSeeder::class);
    }
}
