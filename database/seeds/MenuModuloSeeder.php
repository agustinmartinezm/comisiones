<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MenuModuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_modulos')->insert([
            'modulo'     => 'Programar Sesión',
            'ruta'       => 'programarSesion',
            'estatus'    => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('menu_modulos')->insert([
            'modulo'     => 'Seguimiento a Sesiones',
            'ruta'       => 'seguimientoSesion.index',
            'estatus'    => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
