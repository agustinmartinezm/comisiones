<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CcaracterSesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ccaracter_sesiones')->insert([
            'descripcion'  => 'Pública',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ccaracter_sesiones')->insert([
            'descripcion'  => 'Privada',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
    }
}
