<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CtemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ctemas')->insert([
            'descripcion'  => 'Punto',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ctemas')->insert([
            'descripcion'  => 'Asunto Adicional',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ctemas')->insert([
            'descripcion'  => 'Reporte de Visita de Supervisión',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ctemas')->insert([
            'descripcion'  => 'Reporte de Simulacro',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ctemas')->insert([
            'descripcion'  => 'Sistema Informático de Intercambio de Pedimentos y Libertades',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ctemas')->insert([
            'descripcion'  => 'Resguardo de Videos',
            'estatus'      => 1,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
    }
}
