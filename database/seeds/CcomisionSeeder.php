<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CcomisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ccomisiones')->insert([//1
            'descripcion'  => 'Comisión de Vigilancia del Instituto de Ciencias Forenses',
            'estatus'      => 1,
            'siglas'       => 'CVINCIFO',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ccomisiones')->insert([//2
            'descripcion'  => 'Comisión de Protección Civil',
            'estatus'      => 1,
            'siglas'       => 'CPC',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ccomisiones')->insert([//3
            'descripcion'  => 'Comisión de Seguridad',
            'estatus'      => 1,
            'siglas'       => 'CS',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
        DB::table('ccomisiones')->insert([//4
            'descripcion'  => 'Comisión de Medios Alternos',
            'estatus'      => 1,
            'siglas'       => 'COMA',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ccomisiones')->insert([//5
            'descripcion'  => 'Comisión de Seguridad de los CENDI',
            'estatus'      => 1,
            'siglas'       => 'CENDI',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        DB::table('ccomisiones')->insert([//6
            'descripcion'  => 'Comisión de Seguimiento a la Implementación de Tecnología Judicial',
            'estatus'      => 1,
            'siglas'       => 'CSITJ',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

         DB::table('ccomisiones')->insert([//7
            'descripcion'  => 'Comisión de Seguimiento de Asuntos Jurídicos del Consejo de la Judicatura de la Ciudad de México (Acuerdo 41-14/2017)',
            'estatus'      => 1,
            'siglas'       => 'CSAJ',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);
    }
}
