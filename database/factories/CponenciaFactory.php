<?php

use Faker\Generator as Faker;

$factory->define(App\Modelos\Catalogos\Cponencia::class, function (Faker $faker) {
    return [
    	'id_cponencia' => $faker->unique()->randomNumber,
        'descripcion' => $faker->sentence,
        'estatus' => $faker->randomElement([0, 1]),
    ];
});
