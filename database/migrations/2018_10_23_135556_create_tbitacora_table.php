<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbitacora', function (Blueprint $table) {
            $table->increments('id_tbitacora');
            $table->integer('id_cmovimiento')->unsigned();
            $table->foreign('id_cmovimiento')->references('id_cmovimiento')->on('cmovimiento');
            $table->integer('id')->unsigned()->nullable();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->integer('id_tsesion')->unsigned();
            $table->foreign('id_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbitacora');
    }
}
