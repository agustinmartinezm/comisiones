<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTobservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tobservaciones', function (Blueprint $table) {
            $table->increments('id_tobservacion');
            $table->integer('id_cponencia');
            $table->foreign('id_cponencia')->references('id_cponencia')->on('cponencias');
            $table->integer('id_tsesion')->unsigned();
            $table->foreign('id_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->integer('id_puntoOrdenDia')->unsigned();
            $table->foreign('id_puntoOrdenDia')->references('id_puntoOrdenDia')->on('tpuntos_orden_dia');
            $table->integer('id_tacuerdo')->unsigned();
            $table->foreign('id_tacuerdo')->references('id_tacuerdo')->on('tacuerdos');
            $table->text('observacion');
            $table->integer('estatus');
            $table->integer('id')->unsigned();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tobservaciones');
    }
}
