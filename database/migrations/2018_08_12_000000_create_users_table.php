<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('paterno');
            $table->string('materno');
            $table->string('email');
            $table->integer('id_cponencia');
            $table->foreign('id_cponencia')->references('id_cponencia')->on('cponencias');
            $table->integer('id_crol')->unsigned()->nullable();
            $table->foreign('id_crol')->references('id_crol')->on('croles');
            $table->string('usuario')->unique();
            $table->string('password');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
