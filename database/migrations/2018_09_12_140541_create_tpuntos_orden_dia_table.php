<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpuntosOrdenDiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tpuntos_orden_dia', function (Blueprint $table) {
            $table->increments('id_puntoOrdenDia');
            $table->integer('id_tsesion')->unsigned();
            $table->foreign('id_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->integer('id_ctema')->unsigned();
            $table->foreign('id_ctema')->references('id_ctema')->on('ctemas');
            $table->integer('numero_punto');
            $table->text('punto');
            $table->integer('id_cestatusPunto')->unsigned();
            $table->foreign('id_cestatusPunto')->references('id_cestatusPunto')->on('cestatus_puntos');
            $table->integer('id')->unsigned();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tpuntos_orden_dia');
    }
}
