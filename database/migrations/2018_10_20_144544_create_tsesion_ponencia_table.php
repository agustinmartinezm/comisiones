<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsesionPonenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsesion_ponencia', function (Blueprint $table) {
            $table->increments('id_tsesion_ponencia');
            $table->integer('id_tsesion')->unsigned();
            $table->foreign('id_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->integer('id_cponencia');
            $table->foreign('id_cponencia')->references('id_cponencia')->on('cponencias');
            $table->integer('estatus');
            $table->integer('id')->unsigned();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsesion_ponencia');
    }
}
