<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemaPrivilegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tema_privilegios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ccomision')->unsigned();
            $table->foreign('id_ccomision')->references('id_ccomision')->on('ccomisiones');
            $table->integer('id_ctema')->unsigned();
            $table->foreign('id_ctema')->references('id_ctema')->on('ctemas');
            $table->integer('estatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tema_privilegios');
    }
}
