<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTacuerdosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tacuerdos', function (Blueprint $table) {
            $table->increments('id_tacuerdo');
            $table->integer('id_tsesion')->unsigned();
            $table->foreign('id_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->integer('id_puntoOrdenDia')->unsigned();
            $table->foreign('id_puntoOrdenDia')->references('id_puntoOrdenDia')->on('tpuntos_orden_dia');
            $table->text('acuerdo');
            $table->integer('version');
            $table->integer('estatus');
            $table->integer('id')->unsigned();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tacuerdos');
    }
}
