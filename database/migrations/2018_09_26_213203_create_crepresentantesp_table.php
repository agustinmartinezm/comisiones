<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrepresentantespTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crepresentantesp', function (Blueprint $table) {
            $table->increments('id_crepresentantep');
            $table->integer('id_cponencia');
            $table->foreign('id_cponencia')->references('id_cponencia')->on('cponencias');
            $table->integer('id_ccomision')->unsigned();
            $table->foreign('id_ccomision')->references('id_ccomision')->on('ccomisiones');
            $table->string('titulo',8);
            $table->string('nombre',30);
            $table->string('paterno',30);
            $table->string('materno',30)->nullable();
            $table->string('sexo',30);
            $table->integer('estatus');
            $table->integer('firma');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crepresentantesp');
    }
}
