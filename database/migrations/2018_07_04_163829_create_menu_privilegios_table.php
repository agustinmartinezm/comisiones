<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuPrivilegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_privilegios', function (Blueprint $table) {           
            $table->integer('id_menuModulo')->unsigned();
            $table->foreign('id_menuModulo')->references('id_menuModulo')->on('menu_modulos');
            $table->integer('id_crol')->unsigned()->nullable();
            $table->foreign('id_crol')->references('id_crol')->on('croles');
            $table->integer('estatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_privilegios');
    }
}
