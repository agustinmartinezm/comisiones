<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsoportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsoportes', function (Blueprint $table) {
            $table->increments('id_tsoporte');
            $table->integer('id_tsesion')->unsigned();
            $table->foreign('id_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->integer('id_puntoOrdenDia')->nullable()->unsigned();
            $table->foreign('id_puntoOrdenDia')->references('id_puntoOrdenDia')->on('tpuntos_orden_dia');
            $table->string('ruta');
            $table->string('nombre');
            $table->integer('id')->unsigned();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->integer('estatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsoportes');
    }
}
