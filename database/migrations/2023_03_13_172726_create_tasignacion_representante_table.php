<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasignacionRepresentanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasignacion_representante', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_tsesion')->unsigned();
            $table->foreign('fk_tsesion')->references('id_tsesion')->on('tsesiones');
            $table->integer('fk_crepresentantep')->unsigned();
            $table->foreign('fk_crepresentantep')->references('id_crepresentantep')->on('crepresentantesp');
            $table->integer('estatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasignacion_representante');
    }
}
