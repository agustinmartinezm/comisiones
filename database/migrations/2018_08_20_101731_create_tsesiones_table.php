<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsesionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsesiones', function (Blueprint $table) {
            $table->increments('id_tsesion');
            $table->integer('id_ccomision')->unsigned();
            $table->foreign('id_ccomision')->references('id_ccomision')->on('ccomisiones');
            $table->integer('id_csede')->unsigned();
            $table->foreign('id_csede')->references('id_csede')->on('csedes');
            $table->integer('id_ctipoSesion')->unsigned();
            $table->foreign('id_ctipoSesion')->references('id_ctipoSesion')->on('ctipos_sesiones');
            $table->integer('id_ccaracterSesion')->unsigned();
            $table->foreign('id_ccaracterSesion')->references('id_ccaracterSesion')->on('ccaracter_sesiones');
            $table->date('fecha_programada')->nullable();
            $table->time('hora_programada')->nullable();
            $table->time('hora_fin_estimada')->nullable();
            $table->time('resceso_inicio')->nullable();
            $table->time('resceso_fin')->nullable();
            $table->integer('id_cestatusSesion')->unsigned();
            $table->foreign('id_cestatusSesion')->references('id_cestatusSesion')->on('cestatus_sesiones');
            $table->string('num_sesion');
            $table->integer('anio_sesion');
            $table->integer('id')->unsigned();//usuario
            $table->foreign('id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsesiones');
    }
}
