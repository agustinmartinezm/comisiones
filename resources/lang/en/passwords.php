<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe de tener un mínimo de 6 caracteres y debe coincidir con la confirmación.'/*'Passwords must be at least six characters and match the confirmation.'*/,
    'reset' => 'Se ha cambiado la contraseña.'/*'Your password has been reset!'*/,
    'sent' => 'Hemos enviado un correo con el link para restablecer su contraseña.'/*'We have e-mailed your password reset link!'*/,
    'token' => 'El token es invalido.'/*'This password reset token is invalid.'*/,
    'user' => 'No se ha encontrado este correo electrónico en nuestros registros.'/*"We can't find a user with that e-mail address."*/,

];
