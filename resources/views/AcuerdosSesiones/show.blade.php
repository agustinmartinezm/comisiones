@extends('contenedor')
@section('titulo')
<h4>Acuerdos</h4>
@endsection
@section('panel')
    <div id="datosSesion" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <td>Comisión</td>
                <td>No.</td>
                <td>Tipo</td>
                <td>Caracter</td>
                <td>Fecha y Hora de Inicio</td>
                <td>Fecha y Hora de Termino</td>
            </tr>

            @foreach ($datosSesion as $datosSesion)
  
            <tr>
                <td>{{$datosSesion->comision->descripcion}}</td>
                <td>{{$datosSesion->num_sesion."/".$datosSesion->anio_sesion }}</td>
                <td>{{$datosSesion->tipoSesion->descripcion}}</td>
                <td>{{$datosSesion->caracterSesion->descripcion}}</td>
                <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_programada}}</td>
                <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_fin_estimada}}</td>
            </tr>
            
            @endforeach

        </table>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success" >
            {{ $message }}
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger" >
            {{ $message }}
        </div>
    @endif

    <form method="POST" action="{{ route('acuerdoSesion.capturaAcuerdo') }}" enctype="multipart/form-data">                  
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-1">
                <label>No. Tema:</label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2" style="float: left">
                <select  name="id_puntoOrdenDia" class="form-control" required>
                    <option value=''>Seleccione una opción</option>
                        @foreach ($listaTemas as $listaTemas)
                            <option value="{{ $listaTemas->id_puntoOrdenDia }}">{{ $listaTemas->numero_punto}}</option> 
                        @endforeach
                </select>
                <input type="hidden" name="id_tsesion" value="{{$datosSesion->id_tsesion}}">
            </div>
            <div class="col-md-3" style="float: left">
                <button type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon-search"></span> Consultar
                </button>
            </div>
    </form>

    <form method="GET" action="{{ route('createMinutaWordDocx') }}">                  
            <div  class="col-md-3" id = "descargar">
                <button type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon-download-alt"></span> Descargar Minuta
                </button>
                <input type="hidden" name="id_tsesion" value="{{$datosSesion->id_tsesion}}">
            </div>
    </form>        
        @if($estatusSesion <= 2 || $estatusSesion >= 4)
            <div  class="col-md-3" id = "Liberar" class="text-right"  style="float: right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#Confirmacion">
                    <span class="glyphicon glyphicon-share-alt"></span>Liberar Minuta
                </button>
            </div>
        </div>
        @endif

    <br>
    <br>
    <br>

    @if(!is_null($id_puntoOrdenDia))
        <label>No. Tema: </label>
        {{$datosTema[0]->numero_punto}}
        <br><br>
        <label>Punto: </label>
        {!!nl2br(e($datosTema[0]->punto))!!}
        
        <br><br>
        <form method="POST" action="{{ route('acuerdoSesion.store') }}" enctype="multipart/form-data">                  
            {{ csrf_field() }}
       
         @if(isset($listaObservaciones))
                @foreach ($listaObservaciones as $obs)
                        <label>Observación {{$obs->ponencia->descripcion}}:</label>
                        {{$obs->observacion}}
                        <br><br>
                @endforeach

         @endif


            <br>

            <div class="col-md-12">
                <label>Acuerdo:</label>
                <textarea class="form-control" name="acuerdo" rows="3" value="" required>@if(isset($datosTema[0]->acuerdo->acuerdo)){{$datosTema[0]->acuerdo->acuerdo}}@endif</textarea>
                <input type="hidden" name="id_tsesion" value="{{$datosTema[0]->id_tsesion}}">
                <input type="hidden" name="id_puntoOrdenDia" value="{{$id_puntoOrdenDia}}">
            </div>
            <br><br><br><br><br><br>

    

            <div class="col-md-3">
            <br><br>
                <button type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon glyphicon-floppy-disk"></span> Guardar
                </button>
            </div>
        </form>
    @endif

    <!--Modal confirmaciín-->
    <div class="modal fade" id="Confirmacion" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-sm ">    
        <!-- Contenido-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Liberar</h4>
                </div>
                <form method="GET" action="{{ route('acuerdoSesion.liberarMinuta') }}" enctype="multipart/form-data">
                    <div class="modal-body">                    
                        {{ csrf_field() }}
                        ¿Desea liberar la minuta?
                        <div class="row">
                            <div class="col-md-4">
                                <input type="id_tsesion" name="id_tsesion" value="{{$datosSesion->id_tsesion}}" hidden>
                            </div>
                        </div>
                        <br>
                                             
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Cancelar
                        </button>
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-share-alt"></span> Liberar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
