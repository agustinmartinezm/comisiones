<p>Se ha registrado una sesión de la {{ $notificacion->comision }}.</p>
<p>Una vez sea liberada la orden del día para esta sesión se podra consultar en el sistema.</p>
Saludos,
<br/>
Sistema de Comisiones Transitorias del CJCDMX
<p>*No responda a este correo, es un envío automático.</p> 