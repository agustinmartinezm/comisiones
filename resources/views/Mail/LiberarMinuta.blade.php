<p>Se ha liberado la minuta de la sesión {{$notificacion->num_sesion}}/{{ $notificacion->anio_sesion}} de la {{$notificacion->comision}}.</p>
<p>Para consultar la minuta liberada deberá ingresar al sistema. </p>
Saludos,
<br/>
Sistema de Comisiones Transitorias del CJCDMX

<p>*No responda a etse correo, es un envio automático</p>