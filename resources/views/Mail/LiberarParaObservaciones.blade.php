<p>Se ha liberado la observación de la sesión {{$notificacion->num_sesion}}/{{ $notificacion->anio_sesion}} de la {{$notificacion->comision}}.</p>
<p>Para consultar la observación liberada deberá ingresar al sistema </p>
Saludos,
<br/>
Sistema de Comisiones Transitorias del CJCDMX

<p>*No responda a este correo, es un envio automático</p>