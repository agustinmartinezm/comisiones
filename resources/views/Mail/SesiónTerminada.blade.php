<p>Ha concluido la sesión {{$notificacion->num_sesion}}/{{ $notificacion->anio}} de la {{$notificacion->comision}} de la {{$notificacion->comision}}.</p>

Saludos,
<br/>
Sistema de Comisiones Transitorias del CJCDMX

<p>*No responda a este correo, es un envio automático</p>