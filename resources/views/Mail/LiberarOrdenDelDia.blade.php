<p>Se ha liberado la orden del día de la sesión {{$notificacion->num_sesion}}/{{ $notificacion->anio_sesion}} de la {{$notificacion->comision}}.</p>
<p>Para consultar los puntos liberados deberá ingresar al sistema.</p> 
Saludos,
<br/>
Sistema de Comisiones Transitorias del CJCDMX

<p>*No responda a este correo, es un envio automático</p>