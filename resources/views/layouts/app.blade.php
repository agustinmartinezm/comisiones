<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    
    <link rel="stylesheet" href="{{asset('datepicker/dist/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('datepicker/dist/css/bootstrap-datepicker3.standalone.css')}}">
    <link rel="shortcut icon" href="{{ asset('ESCUDO_CJCDMX.png') }}" >
<!--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">-->

{!! Html::style( asset('/css/bootstrap.min.css')) !!}


<style>

#header {
    margin-bottom: 170px;

}

#footer {
    margin-top: 80px;
}


#signup {
    padding: 0px 25px 25px;
    background: #fff;
    box-shadow: 
        0px 0px 0px 5px rgba( 255,255,255,0.4 ), 
        0px 4px 20px rgba( 0,0,0,0.33 );
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    display: table;
    position: static;
    background-image: url('/../imagenes/fondo.jpg');
}

#signup .header {
    margin-bottom: 20px;
    text-align: center;
    font-size: 10px;

}


#signup .header h3 {
    color: #a55b1a;
    font-size: 20px;
    margin-bottom: 20px;
    font-weight: 600;
}


#signup .header p {
    color: #5B5B5E;
    font-size: 14px;
    font-weight: 400;
}

#signup .sep {
    height: 2px;
    background: #e8e8e8;
    width: 406px;
    margin: 0px -25px;
}

#signup .inputs {
    margin-top: 25px;
}

#signup .inputs label {
    color: #8f8f8f;
    font-size: 15px;
    font-weight: 300;
    letter-spacing: 1px;
    margin-bottom: 7px;
    display: block;
}

input:-webkit-input-placeholder {
    color:    #b5b5b5;
}

input:-moz-placeholder {
    color:    #b5b5b5;
}

#signup .inputs input[type=usuario], input[type=password] {
    background: #ffffff;
    font-size: 0.9rem;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    border: none;
    padding: 13px 10px;
    width: 330px;
    margin-bottom: 20px;
    box-shadow: inset 0px 2px 3px rgba( 0,0,0,0.1 );
    clear: both;
}

#signup .inputs input[type=usuario]:focus, input[type=password]:focus {
    background: #fff;
    box-shadow: 0px 0px 0px 3px #b1b1b2, inset 0px 2px 3px rgba( 0,0,0,0.2 ), 0px 5px 5px rgba( 0,0,0,0.15 );
    outline: none;   
}

#signup .inputs .checkboxy {
    display: block;
    position: static;
    height: 25px;
    margin-top: 10px;
    clear: both;
}

#signup .inputs input[type=checkbox] {
    float: left;
    margin-right: 10px;
    margin-top: 3px;
}

#signup .inputs label.terms {
    float: left;
    font-size: 14px;
    
}

#signup .inputs #submit {
    width: 100%;
    margin-top: 20px;
    padding: 15px 0;
    color: #fff;
    font-size: 16px;
    font-weight: 500;
    text-align: center;
    text-decoration: none;
        background: -moz-linear-gradient(
        top,
        #b9c5dd 0%,
        #a4b0cb);
    background: -webkit-gradient(
        linear, left top, left bottom, 
        from(#a55b1a),
        to(#a55b1a));
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid #737b8d;
    -moz-box-shadow:
        0px 5px 5px rgba(000,000,000,0.1),
        inset 0px 1px 0px rgba(255,255,255,0.5);
    -webkit-box-shadow:
        0px 5px 5px rgba(000,000,000,0.1),
        inset 0px 1px 0px rgba(255,255,255,0.5);
    box-shadow:
        0px 5px 5px rgba(000,000,000,0.1),
        inset 0px 1px 0px rgba(255,255,255,0.5);
    text-shadow:
        0px 1px 3px rgba(000,000,000,0.3),
        0px 0px 0px rgba(255,255,255,0);
    display: table;
    position: static;
    clear: both;
}

#signup .inputs #submit:hover {

 background: -webkit-gradient(
      linear,
      center bottom,
      center top,
      from(#a55b1a),
      to(#a55b1a));
}

</style>



<!--<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">-->
   <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"
></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
              
                

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->

                  

                    <!-- Right Side Of Navbar -->

                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                      
                        @guest
                            <!--<li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Registrar</a></li>-->
                        @else
                        
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->nombre }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('home') }}"
                                            onclick="">
                                            Inicio
                                        </a>
                                    </li>
                                    
                                    @if (Auth::user()->status === 1)
                                    @foreach ($listaMenu as $menu)

                                    <li>
                                        <a href="{{ route($menu->ruta) }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('{{$menu->modulo}}').submit();">
                                            {{$menu->modulo}} 
                                        </a>

                                        <form id="{{$menu->modulo}}" action="{{ route($menu->ruta) }} " method="GET" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    @endforeach
                                    @endif

                                    <li>
                                        <a href="{{ route('manuales') }}"
                                            onclick="">
                                            Manual
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endguest

                    </ul>

                      <ul>
                              <img src="{{URL::asset('/imagenes/Comisiones-Transitorias.png')}}"width="1000" />&nbsp;
                      </ul>

                </div>
            </div>
        </nav>

        @yield('content')
 <br>
            <center>
        <ul>
            <img src="{{URL::asset('/imagenes/Footer.png')}}"width="1200"  />&nbsp;
        </ul></center>
  
    </div>
    <script type="text/javascript"  src="{{ asset('jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="{{asset('bootstrap-datepicker.es.min.js')}}"></script>
    <script src="{{asset('js/validaciones.js')}}"></script>

    <script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            todayHighlight: true,
            language: "es",
            autoclose: true
        });       
    </script>
    
</body>
</html>
