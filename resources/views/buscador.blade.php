@extends('contenedor')
@section('titulo',"Buscador/Bitacora")


@section('panel')

    <form method="POST" action="{{ route('buscador') }}">
    {{ csrf_field() }}

        @if ($message = Session::get('error'))
            <div class="alert alert-danger" >
                {{ $message }}
            </div>
        @endif  
        <div class="row">
            <div class="col-md-3">
                <label>Comisión:</label>
                    <!-- Si el usuario es de secretaria general-->
                @if (Auth::user()->id_crol == 1)
                    {!! Form::select('comision_id', $listaComisiones, null, ['class' => 'form-control']) !!}
                @endif
                <!-- Si el usuario es de ponencia o de consulta-->
                @if ((Auth::user()->id_crol == 2) || (Auth::user()->id_crol == 3))
                    <select required="required" name="comision_id" class="form-control">
                            @foreach ($listaComisiones as $listaComisiones)
                            <option value="{{ $listaComisiones->id_ccomision }}">{{ $listaComisiones->descripcion }}</option>
                            @endforeach
                    </select>
                @endif    
            </div>
            <div class="col-md-3">
                <label>Busqueda:</label>
                <input type="text" class="form-control" name="busca" placeholder="Ingrese texto" required>
            </div>    
            <div class="col-md-3">
                <label>Año:</label>
                <input type="text" class="form-control" name="anioSesion" >
            </div>    
            <div class="col-md-3">
                <br>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>       
        </div>
    </form>
    <br>
    
    <div id="resultadoBusqueda" class="table-responsive">

        @if ($message = Session::get('success'))
            <div class="alert alert-success" >
                {{ $message }}
            </div>
        @endif    
        <table class="table table-hover">
            <tr>
                <th>Comisión</th>
                <th>Sesión</th>
                <th>Tipo de Sesión</th>
                <th>Caracter de Sesión</th>
                <th>Resultado</th>               
            </tr>
            @foreach ($resultados_busqueda as $resultados_busqueda)
                <tr>
                    <td>{{$resultados_busqueda->descripcion_comision}}</td>
                    <td>{{$resultados_busqueda->num_sesion.'/'.$resultados_busqueda->anio_sesion}}</td>
                    <td>{{$resultados_busqueda->descripcion_tipoSesion}}</td>
                    <td>{{$resultados_busqueda->descripcion_caracterSesion}}</td>
                    <td>{{$resultados_busqueda->acuerdo}}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

  
  
          