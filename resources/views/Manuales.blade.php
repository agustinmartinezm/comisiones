@section('content')
@section('panel')
@extends('layouts.app')
@section('argument',"Manual")
@endsection

<div class="container">
    <div class="row">
        <div class="col-md-12 ">
                <div class="panel panel-default">
                <div class="panel-heading">@yield('argument')</div>
                 @if ($id_crol == 1)<center> 
                  <iframe src="{{URL::asset('/Manual/Manual de usuario Secretaria general.pdf')}}"width="600" height="600"></iframe>
                  </center>@else <center> 
                  <iframe src="{{URL::asset('/Manual/Manual de usuario Ponencias.pdf')}}"width="600" height="600"></iframe>
                 </center> @endif

                 <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                          {{ session('status') }}
                        </div>
                    @endif
                     @yield('panel')
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection
