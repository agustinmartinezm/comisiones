@extends('contenedor')
@section('titulo',"Administración de Usuarios")


@section('panel')
    <a href="{{ URL::route('register') }}" class="btn btn-primary">Registrar Usuario</a>

    <a href="{{ URL::route('usuariosDeshabilitados') }}" class="btn btn-primary">Usuarios Deshabilitados</a><br><br>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger" >
            {{ $message }}
        </div>
    @endif

    <form method="POST" action="{{ route('consultaUsuario') }}">
    {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
                <label>Área:</label>
                {!! Form::select('ponencia_id', $listaPonencias, null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-3">
                <label>Rol:</label>
                {!! Form::select('rol_id', $listaRoles, null, ['class' => 'form-control']) !!}
            </div>   
            <div class="col-md-3">
                <br>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>       
        </div>
    </form><br><br>

    <div id="UsuariosActivos" class="table-responsive">  
        
        @if($listaUsuarios != null)
            <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Ponencia</th>
                    <th>Rol</th>
                    <th>Acciones</th>
                   
                </tr>
            @foreach ($listaUsuarios as $listaUsuarios)
                <tr>
                    <td>{{$listaUsuarios->nombre.' '.$listaUsuarios->paterno.' '.$listaUsuarios->materno}}</td>
                    <td>{{$listaUsuarios->ponencia->descripcion}}</td>
                    <td>{{$listaUsuarios->rol->descripcion}}</td>
                    <td>
                        <div class="row">
                            
                            <div class="col-md-4">
                                <form method="POST" action="{{ route('resetearContrasenia') }}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-warning">Resetear</button>
                                    <input type="text" name="idUsuario" value={{$listaUsuarios->id}} hidden>
                                </form>
                            </div>

                            <div class="col-md-4">
                                <form method="POST" action="{{ route('eliminarUsuario') }}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                    <input type="text" name="idUsuario" value={{$listaUsuarios->id}} hidden>
                                </form>
                            </div>

                            <div class="col-md-4">
                                <form method="POST" action="{{ route('editarUsuario') }}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-success">Editar</button>
                                    <input type="text" name="idUsuario" value={{$listaUsuarios->id}} hidden>
                                </form>
                            </div>

                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </table>
    </div>
@endsection

  
  
          