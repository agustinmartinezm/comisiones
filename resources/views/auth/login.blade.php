@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


          


                       <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="signup">

                         <div class="header">
            
                              <h3>Bienvenido al Sistema de Comisiones Transitorias del CJCDMX</h3>
                
                              <p>Para iniciar ingresa tu usuario y contraseña</p>

                
                         </div>
            
                         <div class="sep"></div>

                          <div class="inputs">
            
                              

                    
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('usuario') ? ' has-error' : '' }}" style="justify-content: center;display:flex;align-items: center">
                            <div class="col-md-6">
                                <input type="usuario" class="form-control" placeholder="Usuario" name="usuario" value="{{ old('usuario') }}" autofocus>

                                @if ($errors->has('usuario'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('usuario') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
            
                              

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"style="justify-content: center;display:flex;align-items: center">
                          

                            <div class="col-md-6">
                                <input type="password" class="form-control"  placeholder="Contraseña" name="password" autofocus>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                    <div class="checkboxy">
                        <a class="btn btn-link" href="{{ url('/password/reset') }}">¿Olvidó su contraseña?</a>
                    </div>

                    <button type="submit" class="btn btn-primary" id="submit">
                        <i class="fa fa-btn fa-sign-in"></i>INGRESAR
                    </button>
                    
            </div>

        </form>



        </div>
    </div>
</div>

@endsection