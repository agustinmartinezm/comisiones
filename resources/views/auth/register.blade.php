@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registro de Usuarios</div>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success" >
                        {{ $message }}
                    </div>
                @endif

                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @endif

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" style = "text-transform: uppercase" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="form-group{{ $errors->has('paterno') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Paterno</label>

                            <div class="col-md-6">
                                <input id="paterno" type="text" class="form-control" name="paterno" value="{{ old('paterno') }}" style = "text-transform: uppercase" required autofocus>

                                @if ($errors->has('paterno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('paterno') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="form-group{{ $errors->has('materno') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Materno</label>

                            <div class="col-md-6">
                                <input id="materno" type="text" class="form-control" name="materno" value="{{ old('materno') }}" style = "text-transform: uppercase" required autofocus>

                                @if ($errors->has('materno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('materno') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('usuario') ? ' has-error' : '' }}">
                            <label for="usuario" class="col-md-4 control-label">Usuario</label>

                            <div class="col-md-6">
                                <input id="usuario" type="text" class="form-control" name="usuario" value="{{ old('usuario') }}" onkeypress="return numberonly(event);" maxlength="7" required autofocus>

                                @if ($errors->has('usuario'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('usuario') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>

                        <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
                            <label for="rol" class="col-md-4 control-label">Rol</label>

                            <div class="col-md-6">
                                    {!! Form::select('id_crol',$listaRoles,null,array('id' => 'id_crol','name' => 'id_crol', 'class' => 'form-control'))!!} 
                            </div>                           
                        </div> 

                        <div class="form-group{{ $errors->has('ponencia') ? ' has-error' : '' }}">
                            <label for="ponencia" class="col-md-4 control-label">Área</label>

                            <div class="col-md-6">
                                    {!! Form::select('id_cponencia',$listaPonencias,null,array('id' => 'id_cponencia','name' => 'id_cponencia', 'class' => 'form-control'))!!} 
                            </div>                           
                        </div> 

                        <div class="form-group">
                            <label class="col-md-4 control-label">Comisiones:</label>
                        </div>
                        
                        <?php $countComisiones = 1;?>  
                        @foreach($listaComisiones as $comision)
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <label class="col-md-4 control-label">{{ $comision->descripcion}}</label>
                            <input type ="checkbox" name="{{'comision'.$countComisiones}}" value="{{$comision->id_ccomision}}" class="col-md-2"><br>
                            </div>
                            <?php $countComisiones ++;?>  
                        @endforeach     

                        <input id="countComisiones" type="hidden" class="form-control" name="countComisiones" value = {{$countComisiones}}>          
                        
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirma Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
