@extends('contenedor')
@section('titulo') 
<h4>Orden del día</h4>
@endsection
@section('panel')

    <div id="datosSesion" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <td>Comisión</td>
                <td>No.</td>
                <td>Estatus</td>
                <td>Tipo</td>
                <td>Caracter</td>
                <td>Fecha y Hora de Inicio</td>
                <td>Fecha y Hora de Termino</td>
            </tr>

            @foreach ($datosSesion as $datosSesion)
  
            <tr>
                <td>{{$datosSesion->comision->descripcion}}</td>
                <td>{{$datosSesion->num_sesion."/".$datosSesion->anio_sesion }}</td>
                <td>{{$datosSesion->estatusSesion->descripcion}}</td>
                <td>{{$datosSesion->tipoSesion->descripcion}}</td>
                <td>{{$datosSesion->caracterSesion->descripcion}}</td>
                @if(is_null($datosSesion->fecha_programada))
                    <td>Por definir</td>
                    <td>Por definir</td>
                @else
                    <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_programada}}</td>
                    <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_fin_estimada}}</td>
                @endif
            </tr>
            
            @endforeach

        </table>
    </div>
    <br>

    @if ($message = Session::get('success'))
        <div class="alert alert-success" >
            {{ $message }}
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @endif
    <div id = "Botones">
        <div id = "Agregar" class="text-left"  style="float: left">
            <label>Captura de Tema</label>
              <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#CapturaTemas">
                <span class="glyphicon glyphicon-plus"></span>Agregar
            </button>
        </div>
          <!-- Trigger the modal with a button -->
        <div id = "Liberar" class="text-right"  style="float: right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#Confirmacion">
                <span class="glyphicon glyphicon-share-alt"></span>Liberar
            </button>
        </div>
    </div>
    <br>
    <br>
    <br>
    <label>Temas Capturados</label>
    <br>
    <br>
    <div id="temasCapturados" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>No.</th>
                <th>Estatus</th>
                <th>Tema</th>
                <th>Descripción</th>
                <th>Acción</th>
            </tr>

            @foreach ($listaPuntos as $listaPuntos)
  
                <tr>
                    <td width="5%">{{$listaPuntos->numero_punto}}</td>
                    <td width="5%">{{$listaPuntos->estatusPunto->descripcion}}</td>
                    <td width="10%">{{$listaPuntos->tema->descripcion}}</td>
                    <td width="60%"> {!! nl2br(e($listaPuntos->punto)) !!}</td>
                    <td width="20%">
                        <form method="GET" action="{{ route('seguimientoSesion.acciones',[$listaPuntos->id_tsesion,$listaPuntos->id_puntoOrdenDia,$listaPuntos->numero_punto,1]) }}"  style="float: left">
                            <button type="submit" class="btn btn-info" title="Subir"> 
                                <span class="glyphicon glyphicon-arrow-up"></span>
                            </button>
                        </form>
                        <form method="GET" action="{{ route('seguimientoSesion.acciones',[$listaPuntos->id_tsesion,$listaPuntos->id_puntoOrdenDia,$listaPuntos->numero_punto,2]) }}"  style="float: left">
                            <button type="submit" class="btn btn-info" title="Bajar"> 
                                <span class="glyphicon glyphicon-arrow-down"></span>
                            </button>
                        </form>

                        <input type="hidden" name="tema2" value="{{$listaPuntos->punto}}">
                        <input type="hidden" name="id_orden" value="{{$listaPuntos->id_puntoOrdenDia}}">
                        <input type="hidden" name="idtsesion" value="{{$listaPuntos->id_tsesion}}">

                        @foreach ($listaSoporte as $Soporte)
                            @if($listaPuntos->id_puntoOrdenDia == $Soporte->id_puntoOrdenDia)
                                <input type="hidden" name="ruta" value="{{$Soporte->ruta}}">
                            @endif
                        @endforeach
       
                        <button type="button" class="btn btn-info" title="Editar" data-toggle="modal" data-target="#{{$listaPuntos->id_puntoOrdenDia}}"> 
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                      
                        <form method="GET" action="{{ route('seguimientoSesion.acciones',[$listaPuntos->id_tsesion,$listaPuntos->id_puntoOrdenDia,$listaPuntos->numero_punto,3]) }}"  style="float: left">
                            <button type="submit" class="btn btn-info" title="Eliminar"> 
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>

                    <td>
                        <div class="modal" id="{{$listaPuntos->id_puntoOrdenDia}}" role="dialog">

                            <div class="modal-dialog modal-md">    
                                <!-- Contenido-->
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edicion de temas</h4>
                                    </div>

                                    <form method="POST" action="{{ route('seguimientoSesion.editaPunto') }}" enctype="multipart/form-data">                                                                         
                               
                                        <div class="modal-body">                    
                                            {{ csrf_field() }}                            
                                            <br>
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <label>{{$listaPuntos->tema->descripcion}}</label>                                    
                                                </div>
                                                <br>
                                                <div class="col-md-12">
                                                    <label>Descripción:</label>
                                                    <input type="hidden" name="id_sesion" value="{{$listaPuntos->id_tsesion}}">
                                                    <input type="hidden" name="id_orden1" value="{{$listaPuntos->id_puntoOrdenDia}}">
                                                    <textarea class="form-control" name="tema1" rows="3"  value= "" required>{{$listaPuntos->punto}}</textarea>
                                                </div>
                                                <br>
                                            </div>
                                        
                                            <div class="row">
                                                <br>
                                                <div class="col-sm-8">
                                                    <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple="">
                                                </div>
                                            </div>                       
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <span class="glyphicon glyphicon-remove"></span> Cancelar
                                            </button>
                                            <button type="submit" class="btn btn-success">
                                                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                                            </button>
                                        </div>
                                    </form>

                                    <div class="modal-footer" align="left">
                                        <table> 
                                            @foreach ($listaSoporte as $Soporte)                          
                                                @if($listaPuntos->id_puntoOrdenDia == $Soporte->id_puntoOrdenDia)
                                                    <tr>
                                                        <td> <label>{{ $Soporte->nombre}}</label></td>
                                                        <td>
                                                            <form method="GET" action="{{ route('seguimientoSesion.downloadSoporte', $Soporte->id_tsoporte) }}"  style="float: left">  
                                                 
                                                                <button type="submit" class="btn btn-info" title="Descargar archivo"> 
                                                                    <span class="glyphicon glyphicon-download-alt"></span>
                                                                </button>
                                                            </form>
                                                        </td>
                                                        <td>
                                                            <form method="GET" action="{{ route('seguimientoSesion.eliminaArchivo', $Soporte->id_tsoporte) }}"  style="float: left">
                                                                <input type="hidden" name="id_sesion" value="{{$listaPuntos->id_tsesion}}">
                                                                <input type="hidden" name="id_orden1" value="{{$listaPuntos->id_puntoOrdenDia}}">
                                                                <button type="submit" class="btn btn-info" title="Eliminar"> 
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </form>
                                                        </td>                             
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>


    <!-- Modales -->
    <div class="modal fade" id="CapturaTemas" role="dialog">
        <div class="modal-dialog modal-md">    
        <!-- Contenido-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Captura de temas</h4>
                </div>
                <form method="POST" action="{{ route('seguimientoSesion.store') }}" enctype="multipart/form-data">
                    <div class="modal-body">                    
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-4">
                                <select required="required" name="id_ctema" class="form-control">
                                    <option>Seleccione una opción</option>
                                    @foreach ($listaTemas as $listaTemas)
                                    <option value="{{ $listaTemas->id_ctema }}">{{ $listaTemas->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <input type="id_tsesion" name="id_tsesion" value="{{$datosSesion->id_tsesion}}" hidden>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                             <div class="col-md-12">
                                <label>Descripción:</label>
                                <textarea class="form-control" name="tema" rows="3" required></textarea>
                            </div>
                            <br>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple="">
                            </div>
                        </div>                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Cancelar
                        </button>
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <div class="modal fade" id="Confirmacion" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-sm ">    
        <!-- Contenido-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Liberar</h4>
                </div>
                <form method="GET" action="{{ route('seguimientoSesion.liberar') }}" enctype="multipart/form-data"> 
                    <div class="modal-body">                    
                        {{ csrf_field() }} 
                        ¿Desea liberar la orden del día? 
                        <div class="row">
                            <div class="col-md-4">
                                <input type="id_tsesion" name="id_tsesion" value="{{$datosSesion->id_tsesion}}" hidden>
                            </div>
                        </div>
                        <br>
                                             
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Cancelar
                        </button>
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-share-alt"></span> Liberar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
