@extends('contenedor')
@section('titulo')
<h4>Comentarios Orden del día</h4>
@endsection
@section('panel')

    <div id="datosSesion" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <td>Comisión</td>
                <td>No.</td>
                <td>Estatus</td>
                <td>Tipo</td>
                <td>Caracter</td>
                <td>Fecha y Hora de Inicio</td>
                <td>Fecha y Hora de Termino</td>
            </tr>

            @foreach ($datosSesion as $datosSesion)
  
            <tr>
                <td>{{$datosSesion->comision->descripcion}}</td>
                <td>{{$datosSesion->num_sesion."/".$datosSesion->anio_sesion }}</td>
                <td>{{$datosSesion->estatusSesion->descripcion}}</td>
                <td>{{$datosSesion->tipoSesion->descripcion}}</td>
                <td>{{$datosSesion->caracterSesion->descripcion}}</td>
                <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_programada}}</td>
                <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_fin_estimada}}</td>
            </tr>
            
            @endforeach

        </table>
    </div>
    <br>

    @if ($message = Session::get('success'))
        <div class="alert alert-success" >
            {{ $message }}
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @endif
    <div id = "Botones">
        <div id = "Agregar" class="text-left"  style="float: left">
            <label>Captura Comentario</label>
              <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#CapturaComentarios">
                <span class="glyphicon glyphicon-plus"></span>Agregar
            </button>
        </div>
    </div>
    <br>
    <br>
    <br>
    <label>Comentarios</label>
    <br>
    <br>
    <div id="comentariosCapturados" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Fecha y hora</th>
                <th>Área</th>
                <th>Comentario</th>
                <th>Eliminar</th>
            </tr>
		    @foreach ($listaComentarios as $Comentarios)
		    	<tr>
		    		<td width="15%">{{$Comentarios->created_at}}</td>
		    		<td width="15%">{{$Comentarios->usuario->ponencia->descripcion}}</td>
		    		<td width="60%">{{$Comentarios->comentario}}</td>
		    		<td width="10%">
		    			@if($Comentarios->id == Auth::user()->id)
		    				<form method="GET" action="{{ route('eliminaComentarioSesion')}}" style="float: left">
		    					<input type="id_tsesion" name="id_tsesion" value="{{$Comentarios->id_tsesion}}" hidden>
		    					<input type="id_tcomentario" name="id_tcomentario" value="{{$Comentarios->id_tcomentario}}" hidden>
			    				<button type="submit" class="btn btn-info" data-dismiss="modal">
		                            <span class="glyphicon glyphicon-trash"></span>
		                        </button>
		                    </form>
		    			@endif
		    		</td>
		    	</tr>
		    @endforeach
        </table>
    </div>

    <!-- Modales -->
    <div class="modal fade" id="CapturaComentarios" role="dialog">
        <div class="modal-dialog modal-md">    
        <!-- Contenido-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Captura de comentarios</h4>
                </div>
                <form method="POST" action="{{ route('guardaComentarioSesion') }}" enctype="multipart/form-data">
                <div class="modal-body">                    
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-4">
                            <input type="id_tsesion" name="id_tsesion" value="{{$datosSesion->id_tsesion}}" hidden>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                         <div class="col-md-12">
                            <label>Descripción:</label>
                            <textarea class="form-control" name="comentario" rows="3" required></textarea>
                        </div>
                        <br>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
