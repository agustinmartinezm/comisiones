@extends('contenedor')
@section('titulo')
<h4>Seguimiento de Sesiones</h4>
@endsection
@section('panel')

<form method="GET" action="{{ route('seguimientoSesion.consultaSesion') }}"> 

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger" >
            {{ $message }}
        </div>
    @endif

    <div class="row">
        <div class="col-md-3">
            <label>Comision:</label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <!-- Si el usuario es de secretaria general-->
            @if (Auth::user()->id_crol == 1)
                {!! Form::select('comision_id', $listaComisiones, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required']) !!}
            @endif
            <!-- Si el usuario es de ponencia o de consulta-->
            @if ((Auth::user()->id_crol == 2) || (Auth::user()->id_crol == 3))
                <select required="required" name="comision_id" class="form-control">
                        <option>Seleccione una opción</option>
                        @foreach ($listaComisiones as $listaComisiones)
                        <option value="{{ $listaComisiones->id_ccomision }}">{{ $listaComisiones->descripcion }}</option>
                        @endforeach
                </select>
            @endif
        </div>

        <div class="col-md-3">
            <input type="text" class="form-control" name="anio" placeholder="Año" onkeypress="return numberonly(event);" maxlength="4" required>
        </div>

        <div class="col-md-3">
            <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-search"></span> Consultar
            </button>
        </div>
    </div>
</form>
    @if(!is_null($listaSesiones))

        <br>
        @foreach ($comision as $comision)
            <h5><b>Sesiones de la {{$comision->descripcion}}</b></h5>

        @endforeach
        <br>
        <div id="tablaSesion" class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <td>No.</td>
                    <td>Estatus</td>
                    <td>Tipo</td>
                    <td>Caracter</td>
                    <td>Fecha y Hora Programada</td>
                    @if (Auth::user()->id_crol === 1)
                    <td>Editar Sesión</td>
                    @endif
                    <td>Orden del día</td>
                    <td>Desarrollo de Sesión</td>
                </tr>
             
        @foreach ($listaSesiones as $listaSesiones2)
          
                <tr>
                    <td>{{$listaSesiones2->num_sesion."/".$listaSesiones2->anio_sesion }}</td>
                    <td>{{$listaSesiones2->estatusSesion->descripcion}}</td>
                    <td>{{$listaSesiones2->tipoSesion->descripcion}}</td>
                    <td>{{$listaSesiones2->caracterSesion->descripcion}}</td>
                    <td><?php
                    $hora = new DateTime($listaSesiones2->hora_programada);
                    if(is_null($listaSesiones2->fecha_programada)){//Si viene nulo es fecha Por definir
                        echo "Por definir";
                        //$fechaLimite="25/12/2350"; 
                        //$fechaSesion="26/12/2350"; 
                        
                    }else{
                        $fechaSesion = new DateTime($listaSesiones2->fecha_programada);
                        $fechaSesion = new DateTime($fechaSesion->format('Y-m-d') .' ' .$hora->format('H:i:s'));

                        //$fechaLimite=new DateTime(); 
                        //$fechaLimite->modify('+48 hours');

                        echo $fechaSesion->format('d-m-Y H:i:s');
                    }
                    
                    ?></td>

                    @if (Auth::user()->id_crol === 1)
                        @if ($listaSesiones2->id_cestatusSesion < 5)
                        <td>
                            <form method="GET" action="{{ route('EditarSesiones') }}"  style="float: left">
                                
                                <input type="hidden" name="comision" value="{{base64_encode($listaSesiones2->comision->id_ccomision)}}">
                                <input type="hidden" name="descComision" value="{{base64_encode($listaSesiones2->comision->descripcion)}}">
                                <input type="hidden" name="descSede" value="{{base64_encode($listaSesiones2->sede->descripcion)}}">
                                <input type="hidden" name="sede" value="{{$listaSesiones2->sede->id_csede}}">
                                <input type="hidden" name="descCaracter" value="{{$listaSesiones2->caracterSesion->descripcion}}">
                                <input type="hidden" name="caracter" value="{{$listaSesiones2->caracterSesion->id_ccaracterSesion}}">
                                <input type="hidden" name="descTipo" value="{{$listaSesiones2->tipoSesion->descripcion}}">
                                <input type="hidden" name="tipo" value="{{$listaSesiones2->tipoSesion->id_ctipoSesion}}">
                                <input type="hidden" name="num_sesion" value="{{$listaSesiones2->num_sesion}}">
                                @if(is_null($listaSesiones2->fecha_programada))
                                    <input type="hidden" name="fecha" value="Por Definir">                            
                                @else
                                <input type="hidden" name="fecha" value="{{$listaSesiones2->fecha_programada->format('d/m/Y')}}">
                                @endif
                                <input type="hidden" name="hora" value="{{$listaSesiones2->hora_programada}}">
                                <input type="hidden" name="horaF" value="{{$listaSesiones2->hora_fin_estimada}}">
                                <input type="hidden" name="anio_sesion" value="{{$listaSesiones2->anio_sesion}}">
                                <input type="hidden" name="id_tsesion" value="{{$listaSesiones2->id_tsesion}}">
                                <input type="hidden" name="resceso_inicio" value="{{$listaSesiones2->resceso_inicio}}">
                                <input type="hidden" name="resceso_fin" value="{{$listaSesiones2->resceso_fin}}">

                               
                                <button type="submit" class="btn btn-info" title="Editar"> 
                                    <span class="glyphicon glyphicon-edit"></span>
                                </button>
                            </form>
                        </td>
                        @endif
                    @elseif($listaSesiones2->id_cestatusSesion > 4)
                    <!--<td>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#Error">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                    </td>-->
                    @endif
                    <!-- Validación 48 horas, agregar @ -->
                    <!--if($fechaSesion<$fechaLimite && Auth::user()->id_crol === 1)
                    <td>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ErrorHoras">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                    </td>
                    endif-->
                    <td>
                    @if ($listaSesiones2->id_cestatusSesion < 5)
                    @if (Auth::user()->id_crol === 1)
                        <form method="GET" action="{{ route('seguimientoSesion.show',$listaSesiones2->id_tsesion) }}"  style="float: left">
                            <button type="submit" class="btn btn-info" title="Agregar/Editar"> 
                                <span class="glyphicon glyphicon-edit"></span>
                            </button>
                        </form>
                    @endif
                        
                    @elseif($listaSesiones2->id_cestatusSesion > 4 && Auth::user()->id_crol === 1)
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#Error">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                    @endif
                    <!-- Validación 48 horas, agregar @-->
                    <!--if($fechaSesion<$fechaLimite && Auth::user()->id_crol === 1)
                    
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ErrorHoras">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                    
                    endif-->
                        <form method="GET" action="{{ route('consultarSesion') }}"  style="float: left">
                            <input type="hidden" name="num_sesion" value="{{$listaSesiones2->num_sesion}}">
                            <input type="hidden" name="anio_sesion" value="{{$listaSesiones2->anio_sesion}}">
                            <input type="hidden" name="comision" value="{{$listaSesiones2->comision->id_ccomision}}">
                            <input type="hidden" name="descComision" value="{{$listaSesiones2->comision->descripcion}}">
                            <input type="hidden" name="tipo" value="{{$listaSesiones2->tipoSesion->id_ctipoSesion}}">
                            @if(is_null($listaSesiones2->fecha_programada))
                                <input type="hidden" name="fecha" value="Por Definir">
                            @else
                                <input type="hidden" name="fecha" value="{{$listaSesiones2->fecha_programada->format('d/n/Y')}}">
                            @endif
                            <input type="hidden" name="hora" value="{{$listaSesiones2->hora_programada}}">
                            <input type="hidden" name="sede" value="{{$listaSesiones2->sede->id_csede}}">
                            <input type="hidden" name="tsesion" value="{{$listaSesiones2->id_tsesion}}">

                           
                            <button type="submit" class="btn btn-info" title="Visualizar"> 
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </form>

                        <form method="GET" action="{{ route('comentariosSesion',$listaSesiones2->id_tsesion) }}"  style="float: left">                         
                            <button type="submit" class="btn btn-info" title="Comentarios"> 
                                <span class="glyphicon glyphicon glyphicon-inbox"></span>
                            </button>
                        </form>
                    </td>
                    <td>
                        @if ($listaSesiones2->id_cestatusSesion < 5)

                        <form method="GET" action="{{ route('acuerdoSesion.show',$listaSesiones2->id_tsesion) }}"  style="float: left">
                            <button type="submit" class="btn btn-info" title="Captura Acuerdos "> 
                                <span class="glyphicon glyphicon-list"></span> 
                            </button>
                        </form>
                        @elseif($listaSesiones2->id_cestatusSesion > 4 && Auth::user()->id_crol === 1)
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#Error">
                            <span class="glyphicon glyphicon-list"></span>
                        </button>
                        @endif
                        @if ($listaSesiones2->id_cestatusSesion > 3 && $listaSesiones2->id_cestatusSesion < 5)
                            @if (Auth::user()->id_crol === 1)
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#Cerrar{{$listaSesiones2->id_tsesion}}" title="Terminar Sesión">
                                    <span class="glyphicon glyphicon-floppy-saved"></span>
                                </button>
                            @endif
                        @endif
                        @if ($listaSesiones2->id_cestatusSesion >= 5)
                        <form method="POST" action="{{ route('ConsutaSesionFinal') }}"  style="float: left">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_tsesion" value="{{$listaSesiones2->id_tsesion}}">
                            <button type="submit" class="btn btn-info" title="Ver Sesión"> 
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </form>
                        @endif
                    </td>

                    <td>                            <!--Modal cerrar sesión-->
                        <div class="modal fade" id="Cerrar{{$listaSesiones2->id_tsesion}}" role="dialog">
                            <div class="modal-dialog modal-dialog-centered modal-sm ">
                            <!-- Contenido-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        
                                        <h4 class="modal-title">Terminar Sesión</h4>
                                    </div>
                                    <form method="POST" action="{{ route('CerrarSesion') }}" enctype="multipart/form-data"> 
                                        <div class="modal-body">                    
                                            {{ csrf_field() }}  
                                            ¿Desea terminar la sesión?
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input  name="id_tsesion" value="{{$listaSesiones2->id_tsesion}}" hidden>
                                                </div>
                                                <br>
                                            </div>
                                            
                                            <div class="row">
                                                <br>
                                                <div class="col-sm-8">
                                                    <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple="">
                                                </div>
                                            </div>                 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <span class="glyphicon glyphicon-remove"></span> Cancelar
                                            </button>
                                            <button type="submit" class="btn btn-success">
                                                <span class="glyphicon glyphicon-share-alt"></span> Terminar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td>
                                                <!--Modal cerrar sesión-->
                        <div class="modal fade" id="Error" role="dialog">
                            <div class="modal-dialog modal-dialog-centered modal-sm ">    
                                <!-- Contenido-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">La sesión ha sido terminada</h4>
                                    </div>
                                    <div class="modal-body">                    
                                        {{ csrf_field() }}  
                                        Esta función no esta disponible para sesiones terminadas          
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                                            <span class="glyphicon glyphicon-remove"></span> Aceptar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td><!--Modal menos de 48 horas para la sesion-->
                        <div class="modal fade" id="ErrorHoras" role="dialog">
                            <div class="modal-dialog modal-dialog-centered modal-sm ">    
                            <!-- Contenido-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Faltan menos de 48 hrs para la sesión</h4>
                                    </div>
                                    <div class="modal-body">                    
                                        {{ csrf_field() }}  
                                        Esta función no esta disponible para sesiones cuya fecha programada es menor a 48 hrs          
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                                             <span class="glyphicon glyphicon-remove"></span> Aceptar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
        @endforeach
            </table>
        </div>  
    @endif

    <br>
@endsection
