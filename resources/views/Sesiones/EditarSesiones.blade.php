
@extends('contenedor')
@section('titulo')

<h4>Edición de Sesión {{ $num_sesion }} / {{ $anio_sesion }}</h4>
@endsection
@section('panel')

<form method="POST" action="{{ route('editarSesion') }}">
{{ csrf_field() }}

    <h5><b>Datos Generales</b></h5>

    <br>
    <input type="hidden" name="id_tsesion" value="{{ $id_tsesion }}">

    <div class="row">
        <div class="col-md-3">
            <label>Comisión:</label>
                <select value="" required="required" name="comision_id" class="form-control">
                    <option value="{{ base64_decode($comision)}}" >{{ base64_decode($descComision)}}</option>
                </select>
                    @if ($errors->has('comision_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('comision_id') }}</strong>
                        </span>
                    @endif
        </div>
        <div class="col-md-3">
            <label>Sede:</label>
            <select value="" required="required" name="sede_id" class="form-control">
                    <option value=<?php echo $sede; ?> >{{ base64_decode($descSede) }}</option>
                    @foreach($listaSedes as $listaSedes)
                        <option value="{{ $listaSedes->id_csede }}">{{ $listaSedes->descripcion }}</option>
                    @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <label>Tipo de sesión:</label>
            <select value="" required="required" name="tipo_id" class="form-control">
                    <option value=<?php echo $tipo; ?> >{{ $descTipo }}</option>
                    @foreach($listaTipoSesiones as $listaTipoSesiones)
                        <option value="{{ $listaTipoSesiones->id_ctipoSesion }}">{{ $listaTipoSesiones->descripcion }}</option>
                    @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <label>Caracter de sesión:</label>
            <select value="" required="required" name="caracterSesion_id" class="form-control">
                    <option value=<?php echo $caracter; ?> >{{ $descCaracter }}</option>
                    @foreach($listaCaracterSesiones as $listaCaracterSesiones)
                        <option value="{{ $listaCaracterSesiones->id_ccaracterSesion }}">{{ $listaCaracterSesiones->descripcion }}</option>
                    @endforeach
            </select>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-3">
            <label>Fecha programada:</label>
            <input type="text" class="form-control datepicker" name="fechaProg" value=<?php echo $fecha ?> required>
        </div>
        <div class="col-md-3">
            <label>Hora programada:</label>
            <input type="time" name="horaProg" class="form-control" value=<?php echo $hora ?> required>
        </div>
        
        <div class="col-md-3 form-group{{ $errors->has('horaTerm') ? ' has-error' : '' }}">
            <label>Hora estimada de término:</label> 
            <input type="time" name="horaTerm" class="form-control" value=<?php echo $horaF ?> > 

             @if ($errors->has('horaTerm'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('horaTerm') }}</strong>
                                    </span>
                                @endif      
        </div>
      
    </div>

    <br>

    <h5><b>Resceso</b></h5>

    <br>

    <div class="row">
        <div class="col-md-3 form-group{{ $errors->has('horaInicioRec') ? ' has-error' : '' }}">
            <label>Hora Inicio:</label>
            <input type="time" name="horaInicioRec" class="form-control" value=<?php echo $resceso_inicio ?> >
            @if ($errors->has('horaInicioRec'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('horaInicioRec') }}</strong>
                                    </span>
                                @endif
        </div>
        <div class="col-md-3 form-group{{ $errors->has('horaTermRec') ? ' has-error' : '' }}">
            <label>Hora Fin:</label> 
            <input type="time" name="horaTermRec" class="form-control" value=<?php echo $resceso_fin ?> >    
        @if ($errors->has('horaTermRec'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('horaTermRec') }}</strong>
                                    </span>
                                @endif   

        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-3">
            <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
            </button>
        </div>
    </div>

</form>

@endsection
