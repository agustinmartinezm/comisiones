@extends('contenedor')
@section('titulo')
<h4>Detalles de sesión terminada</h4>
@endsection
@section('panel')

    <div id="datosSesion" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <td>Comisión</td>
                <td>No.</td>
                <td>Estatus</td>
                <td>Tipo</td>
                <td>Caracter</td>
                <td>Fecha y Hora de Inicio</td>
                <td>Fecha y Hora de Termino</td>
            </tr>

            @foreach ($datosSesion as $datosSesion)
  
            <tr>
                <td>{{$datosSesion->comision->descripcion}}</td>
                <td>{{$datosSesion->num_sesion."/".$datosSesion->anio_sesion }}</td>
                <td>{{$datosSesion->estatusSesion->descripcion}}</td>
                <td>{{$datosSesion->tipoSesion->descripcion}}</td>
                <td>{{$datosSesion->caracterSesion->descripcion}}</td>
                <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_programada}}</td>
                <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_fin_estimada}}</td>
            </tr>
            
            @endforeach

        </table>
    </div>
    <br>
    <form method="POST" action="{{ route('downloadMinuta') }}">                  
            <div  class="col-md-3" id = "descargar">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$minuta[0]->id_tsoporte}}">
                <button type="submit" class="btn btn-success">
                    <span class="glyphicon glyphicon-download-alt"></span> Descargar Minuta
                </button>
            </div>
    </form>
    <br>
    <br>
    <br>
    <label>Temas Capturados</label>
    <br>
    <br>
    <div id="temasCapturados" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>No.</th>
                <th>Estatus</th>
                <th>Tema</th>
                <th>Descripción</th>
                <th>Acuerdo</th>
            </tr>

            @foreach ($listaPuntos as $listaPuntos)
  
            <tr>
                <td width="5%">{{$listaPuntos->numero_punto}}</td>
                <td width="5%">{{$listaPuntos->estatusPunto->descripcion}}</td>
                <td width="10%">{{$listaPuntos->tema->descripcion}}</td>
                <td width="50%">{{$listaPuntos->punto}}</td>
                <td width="30%">@if($listaPuntos->numero_punto >= 3)
                                    @foreach ($acuerdos as $acuerdos1)
                                        @if($listaPuntos->id_puntoOrdenDia == $acuerdos1->id_puntoOrdenDia)
                                            {{$acuerdos1->acuerdo}}
                                        @endif
                                    @endforeach
                                @endif
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection