
@extends('contenedor')
@section('titulo')

<h4>Orden del día {{ $num_sesion }} / {{ $anio_sesion }}</h4>
@endsection
@section('panel')

<form method="GET" action="{{ route('generateWordOrdenDia') }}">
{{ csrf_field() }}
    
    <h5><b>Datos generales</b></h5>

    <div id="datosSesion" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <td>Comisión</td>
                <td>No.</td>
                <td>Tipo</td>
                <td>Caracter</td>
                <td>Fecha y Hora de Inicio</td>
                <td>Fecha y Hora de Termino</td>
            </tr>
            <?php
            use App\Modelos\Transacciones\Tsesion;
            $datosSesion = Tsesion::where('id_tsesion', '=', $tsesion)->get(); ?>
            @foreach ($datosSesion as $datosSesion)
  
            <tr>
                <td>{{$datosSesion->comision->descripcion}}</td>
                <td>{{$datosSesion->num_sesion."/".$datosSesion->anio_sesion }}</td>
                <td>{{$datosSesion->tipoSesion->descripcion}}</td>
                <td>{{$datosSesion->caracterSesion->descripcion}}</td>
                @if(is_null($datosSesion->fecha_programada))
                    <td>Por definir</td>
                    <td>Por definir</td>
                @else
                    <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_programada}}</td>
                    <td>{{$datosSesion->fecha_programada->format('d-m-Y')." - ".$datosSesion->hora_fin_estimada}}</td>
                @endif
            </tr>
            
            @endforeach

        </table>
    </div>

    <h5><b>Temas Capturados</b></h5>

    <div id="temasCapturados" class="table-responsive">
        <table class="table table-hover">
            <tr>
                <td width="10%">No.</td>
                <td width="60%">Descripción</td>
                <td width="30%">Soporte</td>
            </tr>
            
            @foreach ($listaPuntos as $listaPuntos)
            <tr>
                <td>{{$listaPuntos->numero_punto}}</td>
                <td>{!! nl2br(e($listaPuntos->punto)) !!}</td>
                <!--<td>{{$listaPuntos->Soporte_punto}}hola</td>-->
                </form>
                <td>
                    @foreach ($listaSoporte as $datosSoporte)
                        @if($listaPuntos->id_puntoOrdenDia == $datosSoporte->id_puntoOrdenDia)
                        
                            <label>{{ $datosSoporte->nombre }}</label>
                            <form method="GET" action="{{ route('consultaSesion.downloadSoporte' ,$datosSoporte->id_tsoporte) }}"  style="float: left">  
                                <button type="submit" class="btn btn-info" title="Descargar PDF"> 
                                    <span class="glyphicon glyphicon-download-alt"></span>
                                </button>
                            </form>
                        <br>
                        @endif
                    @endforeach
                </td>
                <form method="GET" action="{{ route('generateWordOrdenDia') }}"> 
                </td>
            </tr>
            @endforeach
        </table>
    </div>
	
    <input type="hidden" name="comision" value="{{$comision}}">
    <input type="hidden" name="tipo" value="{{$tipo}}">
    <input type="hidden" name="fecha" value="{{$fecha}}">
    <input type="hidden" name="hora" value="{{$hora}}">
    <input type="hidden" name="sede" value="{{$sede}}">
    <input type="hidden" name="tsesion" value="{{$tsesion}}">
    <input type="hidden" name="num_sesion" value="{{$num_sesion}}">
    <div class="row">
        <div class="col-md-3">
            <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-download-alt"></span> Descargar
            </button>
        </div>
    </div>
</form>
@endsection