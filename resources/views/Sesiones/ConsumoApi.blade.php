@extends('contenedor')
@section('titulo')

<h4>Consumo Api</h4>
@endsection
@section('panel')

<form method="POST" action="{{ route('enviaSesionApi') }}">
{{ csrf_field() }}

    <h5><b>Consumo Api</b></h5>

    <br>
    <div class="row">
        <div class="col-md-3">
            <label>Comisión:</label>

            {!! Form::select('id_tsesion', $listaSesiones, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required']) !!}
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-3">
            <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
            </button>
        </div>
    </div>

</form>

@endsection