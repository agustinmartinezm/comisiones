
@extends('contenedor')
@section('titulo')

<h4>Programar Sesión.</h4>
@endsection
@section('panel')

<form method="POST" action="{{ route('guardarSesion') }}">
{{ csrf_field() }}

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger" >
            {{ $message }}
        </div>
    @endif

    <h5><b>Datos Generales</b></h5>

    <br>

    <div class="row">
        <div class="col-md-3">
        	<label>Comisión:</label>
        	{!! Form::select('comision_id', $listaComisiones, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) !!}
            @if ($errors->has('comision_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comision_id') }}</strong>
                                    </span>
                                @endif
        </div>
        <div class="col-md-3">
        	<label>Sede:</label>
        	{!! Form::select('sede_id', $listaSedes, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required']) !!}
        </div>
        <div class="col-md-3">
        	<label>Tipo de sesión:</label>
        	{!! Form::select('tipoSesion_id', $listaTipoSesiones, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required']) !!}
        </div>
        <div class="col-md-3">
        	<label>Caracter de sesión:</label>
        	{!! Form::select('caracterSesion_id', $listaCaracterSesiones, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required']) !!}
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-3">
        	<label>Fecha programada:</label>
            <input type="text" class="form-control datepicker" name="fechaProg" >
        </div>
        <div class="col-md-3">
        	<label>Hora programada:</label>
            <input type="time" name="horaProg" class="form-control" >
        </div>
        
        <div class="col-md-3 form-group{{ $errors->has('horaTerm') ? ' has-error' : '' }}">
        	<label>Hora estimada de término:</label> 
            <input type="time" name="horaTerm" class="form-control" > 

             @if ($errors->has('horaTerm'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('horaTerm') }}</strong>
                                    </span>
                                @endif    	
        </div>
      
    </div>

    <br>

    <h5><b>Resceso</b></h5>

    <br>

    <div class="row">
        <div class="col-md-3 form-group{{ $errors->has('horaInicioRec') ? ' has-error' : '' }}">
            <label>Hora Inicio:</label>
            <input type="time" name="horaInicioRec" class="form-control">
              @if ($errors->has('horaInicioRec'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('horaInicioRec') }}</strong>
                                    </span>
                                @endif      

        </div>
        <div class="col-md-3 form-group{{ $errors->has('horaTermRec') ? ' has-error' : '' }}">
            <label>Hora Fin:</label> 
            <input type="time" name="horaTermRec" class="form-control">    
              @if ($errors->has('horaTermRec'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('horaTermRec') }}</strong>
                                    </span>
                                @endif      

                


        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-3">
            <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
            </button>
        </div>
    </div>

</form>

@endsection
