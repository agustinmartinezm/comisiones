@extends('contenedor')
@section('titulo',"Usuarios Deshabilitados")


@section('panel')
    <a href="{{ URL::route('admUsuarios') }}" class="btn btn-primary">Regresar</a><br><br>

    @if ($message = Session::get('error'))
        <div class="alert alert-danger" >
            {{ $message }}
        </div>
    @endif

    <div id="UsuariosDeshabilitados" class="table-responsive">  
        
        @if($listaUsuariosDes != null)
            <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Ponencia</th>
                    <th>Rol</th>
                    <th>Habilitar</th>
                   
                </tr>
            @foreach ($listaUsuariosDes as $listaUsuariosDes)
                <tr>
                    <td>{{$listaUsuariosDes->nombre.' '.$listaUsuariosDes->paterno.' '.$listaUsuariosDes->materno}}</td>
                    <td>{{$listaUsuariosDes->ponencia->descripcion}}</td>
                    <td>{{$listaUsuariosDes->rol->descripcion}}</td>
                    <td>
                        <form method="POST" action="{{ route('habilitarUsuario') }}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-success">Habilitar</button>
                            <input type="text" name="idUsuario" value={{$listaUsuariosDes->id}} hidden>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
        </table>
    </div>
@endsection