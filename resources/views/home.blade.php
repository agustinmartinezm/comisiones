@extends('contenedor')
@section('titulo',"Buscador/Bitacora")


@section('panel')

    <form method="POST" action="{{ route('buscador') }}">

        @if ($message = Session::get('error'))
            <div class="alert alert-danger" >
                {{ $message }}
            </div>
        @endif    
    {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">

                <label>Comisión:</label>
                    <!-- Si el usuario es de secretaria general-->
                @if (Auth::user()->id_crol == 1)
                    {!! Form::select('comision_id', $listaComisiones, null, ['class' => 'form-control']) !!}
                @endif
                <!-- Si el usuario es de ponencia o de consulta-->
                @if ((Auth::user()->id_crol == 2) || (Auth::user()->id_crol == 3))
                    <select required="required" name="comision_id" class="form-control">
                            @foreach ($listaComisiones as $listaComisiones)
                            <option value="{{ $listaComisiones->id_ccomision }}">{{ $listaComisiones->descripcion }}</option>
                            @endforeach
                    </select>
                @endif                
                
            </div>
            <div class="col-md-3">
                <label>Busqueda:</label>
                <input type="text" class="form-control" name="busca" placeholder="Ingrese texto" required>
            </div>    
            <div class="col-md-3">
                <label>Año:</label>
                <input type="text" class="form-control" name="anioSesion" >
            </div>
            <div class="col-md-3">
                <br>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>       
        </div>
    </form>
    <br>
    
    <div id="datosSesion" class="table-responsive">


        @if ($message = Session::get('success'))
            <div class="alert alert-success" >
                {{ $message }}
            </div>
        @endif    
        <table class="table table-hover">
            <tr>
                <td>Participante</td>
                <td>Sesión</td>
                <td>Acción</td>
                <td>Fecha</td>
               
            </tr>
         
    @foreach ($listaBitacora as $listaBitacora)
            <tr>
                <td>{{$listaBitacora->sesion->comision->descripcion}}</td>
                <td>{{$listaBitacora->sesion->num_sesion."/".$listaBitacora->sesion->anio_sesion}}</td>
                @if($listaBitacora->id_cmovimiento == 4)
                    <td>{{$listaBitacora->movimiento->descripcion}}</td>
                @else
                    <td>{{$listaBitacora->movimiento->descripcion." ".$listaBitacora->usuario->ponencia->descripcion}}</td>

                @endif

                <td>{{$listaBitacora->created_at}}</td>
               
            </tr>

              @endforeach
            
        </table>
    </div>

   
    @endsection

  
  
          