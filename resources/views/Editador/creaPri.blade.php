@extends('contenedor')

@section('titulo',"Crear Usuario Privilegios")

@section('panel')

<br/>
            
<form class="form-horizontal" method="GET" action="{{route('Insertpri')}}">

    {{ csrf_field() }}

    <br/>

    <div class="form-group">

        <label for="nombre" class="col-md-4 control-label">Nombre</label>

        <div class="col-md-6">
           
            {!! Form::select('id',$listaUsuario,null,array('id' => 'id','name' => 'id', 'class' => 'form-control'))!!} 

        </div> 
    
    </div><br/>

    <div class="form-group">
    
        <label for="comision" class="col-md-4 control-label">Comisión</label>

        <div class="col-md-6">
        
            {!! Form::select('id_ccomision',$listaComisiones,null,array('id' => 'id_ccomision','name' => 'id_ccomision', 'class' => 'form-control'))!!} 
        
        </div>                           
    
    </div><br/>

    <div class="form-group">

        <label for="FechaInicio" class="col-md-4 control-label">Fecha Inicio:</label>
    
        <div class="col-md-6">
        
            <input type="date" name="FechaInicio" class="form-control">
    
        </div>

    </div><br/>
    
    <div class="form-group">

        <label for="FechaFin" class="col-md-4 control-label">Fecha Final:</label>
    
        <div class="col-md-6">
        
            <input type="date" id="FechaFin" name="FechaFin" class="form-control">
    
        </div>

    </div><br/>

    <div class="col text-center" >
            
        <button type="submit" class="btn btn-success">Crear</button>

        <a href="{{ URL::route('consultapri') }}" class="btn btn-primary" >Regresar</a>
    
    </div> <br> 
 
</form>

@endsection