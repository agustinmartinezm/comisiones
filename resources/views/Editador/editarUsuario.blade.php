@extends('contenedor')

@section('titulo',"Editar Usuario")

@section('panel')

<div class="container">
    <div class="row">
        
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{route('update')}}">
                
                {{ csrf_field() }}

                <div class="form-group">

                    <input type="text" name="idUsuario" value={{$usuario->id}} hidden>

                    <label for="nombre" class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                       
                        <input id="nombre" type="text" class="form-control" name="nombre" value="{{isset($usuario->nombre)?$usuario->nombre:old('nombre')}}" style = "text-transform: uppercase">

                    </div> 
                </div>

                <div class="form-group">
                    <label for="paterno" class="col-md-4 control-label">Paterno</label>

                    <div class="col-md-6">
                        <input id="paterno" type="text" class="form-control" name="paterno" value="{{isset($usuario->paterno)?$usuario->paterno:old('paterno')}}" style = "text-transform: uppercase">

                    </div> 
                </div>

                <div class="form-group">
                    <label for="Materno" class="col-md-4 control-label">Materno</label>

                    <div class="col-md-6">

                        <input id="materno" type="text" class="form-control" name="materno" value="{{isset($usuario->materno)?$usuario->materno:old('materno')}}" style = "text-transform: uppercase">

                    </div> 

                </div>
                
                <div class="form-group">

                    <label for="rol" class="col-md-4 control-label">Rol</label>

                    <div class="col-md-6" >   

                        {!! Form::select('id_crol',$listaRolesUsuario,$usuario->id_crol,array('id' => 'id_crol','name' => 'id_crol', 'class' => 'form-control'))!!}               
        
                    </div>                           
                </div> 

                <div class="form-group">

                    <label for="ponencia" class="col-md-4 control-label">Área</label>

                    <div class="col-md-6">  

                        {!! Form::select('id_cponencia',$listaPonenciasUsuarios,$usuario->id_cponencia,array('id' => 'id_cponencia','name' => 'id_cponencia', 'class' => 'form-control'))!!} 

                    </div> 

                    <br><br><br>

                </div>

        <div class="col-md-6">
        </div>  

        <div class="col-md-6">
            
            <button   type="submit" class="btn btn-success" >Editar</button>
            
        <a href="{{ URL::route('admUsuarios') }}" class="btn btn-primary" >Regresar</a>

        
        </div>  
        
    </div>

    </form><br><br>
@endsection