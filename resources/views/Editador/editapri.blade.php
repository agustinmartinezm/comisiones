@extends('contenedor')

@section('titulo',"Editar Usuario Privilegios")

@section('panel')

<br/>
            
<form class="form-horizontal" method="GET" action="{{route('Updatepri')}}">

    {{ csrf_field() }}

    <br/>

    <input type="text" name="idUsuario" value={{$datas->id}} hidden>
    <input type="text" name="defautidcomi" value={{$datas->id_ccomision}} hidden>

    <div class="form-group">

        <label for="nombre" class="col-md-4 control-label">Nombre</label>

        <div class="col-md-6">
           
            <input id="nombre" type="text" class="form-control" name="nombre" value="{{isset($datas->usuario->nombre)?$datas->usuario->nombre:old('nombre')}}" style = "text-transform: uppercase"  readonly onmousedown="return false;">

        </div> 
    
    </div><br/>

    <div class="form-group">
    
        <label for="comision" class="col-md-4 control-label">Comisión</label>

        <div class="col-md-6">
        
            {!! Form::select('id_ccomision',$listaComisiones,$datas->id_ccomision,array('id' => 'id_ccomision','name' => 'id_ccomision', 'class' => 'form-control'))!!} 
        
        </div>                           
    
    </div><br/>

    <div class="form-group">

        <label for="FechaInicio" class="col-md-4 control-label">Fecha Inicio:</label>
    
        <div class="col-md-6">
        
            <input type="date" name="FechaInicio" value="{{$datas->fecha_inicio}}" class="form-control">
    
        </div>

    </div><br/>
    
    <div class="form-group">

        <label for="FechaFin" class="col-md-4 control-label">Fecha Final:</label>
    
        <div class="col-md-6">
        
            <input type="date" id="FechaFin" name="FechaFin" value="{{ $datas->fecha_fin}}" class="form-control">
    
        </div>

    </div><br/>

    <div class="col text-center" >
            
        <button type="submit" class="btn btn-success">Editar</button>

        <a href="{{ URL::route('consultapri') }}" class="btn btn-primary" >Regresar</a>
    
    </div> <br> 
 
</form>

@endsection