@extends('contenedor')

@section('titulo',"Editar Representante")

@section('panel')
            
<form class="form-horizontal" method="GET" action="{{route('editar')}}">

    {{ csrf_field() }}

    <div class="form-group">

        <label for="nombre" class="col-md-4 control-label">Título</label>

        <div class="col-md-6">               

            <input id="nombre" type="text" class="form-control" name="nombre" style = "text-transform: uppercase">

        </div> 

    </div>
    <div class="form-group">

        {{-- <input type="text" name="idUsuario" value={{$datosrepresentante->id_crepresentantep}} hidden> --}}

        <label for="nombre" class="col-md-4 control-label">Nombre</label>

        <div class="col-md-6">
           
            <input id="nombre" type="text" class="form-control" name="nombre" value="{{isset($datosrepresentante->nombre)?$datosrepresentante->nombre:old('nombre')}}" style = "text-transform: uppercase">

        </div> 
    </div>

    <div class="form-group">
        <label for="paterno" class="col-md-4 control-label">Paterno</label>

        <div class="col-md-6">
            <input id="paterno" type="text" class="form-control" name="paterno" value="{{isset($datosrepresentante->paterno)?$datosrepresentante->paterno:old('paterno')}}" style = "text-transform: uppercase">

        </div> 
    </div>

    <div class="form-group">
        <label for="Materno" class="col-md-4 control-label">Materno</label>

        <div class="col-md-6">

            <input id="materno" type="text" class="form-control" name="materno" value="{{isset($datosrepresentante->materno)?$datosrepresentante->materno:old('materno')}}" style = "text-transform: uppercase">

        </div> 

    </div>

    <div class="form-group">
    
        <label for="materno" class="col-md-4 control-label">Sexo</label>

        <div class="col-md-6">
    
            <select class="form-control" aria-label=".form-select-sm example">
            
                <option selected>Seleccionar una de las opciones</option>
            
                <option value="F">Femenino</option>
            
                <option value="M">Masculino</option>
            
            </select>
    
        </div> 

    </div><br> 

    <div class="form-group">
        <label for="ponencia" class="col-md-4 control-label">Ponencia</label>

        <div class="col-md-6">
                {!! Form::select('id_cponencia',$listaPonencias,null,array('id' => 'id_cponencia','name' => 'id_cponencia', 'class' => 'form-control'))!!} 
        </div>                           
    </div>
    
    
    <div class="col text-center" >
            
        <button type="submit" class="btn btn-success">Editar</button>

        <a href="{{ URL::route('consultar') }}" class="btn btn-primary" >Regresar</a>
    
    </div> <br> 
 
</form>

@endsection