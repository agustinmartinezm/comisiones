@extends('contenedor')

@section('titulo',"Consulta de Usuarios Privilegios")

@section('panel')

<form class="form-horizontal" method="GET" action="{{route('consultapri')}}">

    {{ csrf_field() }}

    @if ($message = Session::get('success'))
    
    <div class="alert alert-success">
    
            {{ $message }}
    
        </div>
    
        @endif
    
    @if ($message = Session::get('error'))
        
    <div class="alert alert-danger" >
        
            {{ $message }}
        
        </div>

    @endif

    {{-- <div class="col text-center" >

        <a href="{{ URL::route('CrearPri') }}" class="btn btn-success">Registrar Fecha de comisión Usuario</a>
        
    </div><br/><br/> --}}

    <table class="table table-striped">    

        <thead>

        <tr>

            <th class="text-center">Nombre</th>
           
            <th class="text-center">Comisión</th>

            <th class="text-center">Fehca de Inicio</th>

            <th class="text-center">Fehca de Fin</th>
           
            <th class="text-center">Acción</th>
               
        </tr> 

    </thead>
        
        @foreach($datosprivi as $dat)

        <tbody>

            <tr>
            
            {{-- <th class="text-center">{{$dat->id}}</th> --}}

            <th class="text-center">{{$dat->usuario->nombre}}</th>

            <th class="text-center">{{$dat->comision->descripcion}}</th>

            <th class="text-center">{{\Carbon\Carbon::parse($dat->fecha_inicio)->format('d/m/Y')}}</th> 
        
            <th class="text-center">{{\Carbon\Carbon::parse($dat->fecha_fin)->format('d/m/Y')}}</th>

            <td class="text-center">

                <div class="col-md-4">

                    <form method="GET" action="{{route('EditarPrivilegios')}}"> 
                    
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-warning">Editar</button>
                   
                        <input type="text" name="idUsuario" value="{{$dat->id}}" hidden>

                    </form> 
                
                </div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <div class="col-md-4">

                    <form method="GET" action="{{ route('EliminarPrivilegios') }}">
                    
                        {{ csrf_field() }}
                    
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    
                        <input type="text" name="idUsuario" value="{{$dat->id}}" hidden>

                        <input type="text" name="defautidcomi" value={{$dat->id_ccomision}} hidden>
                    
                    </form>

                </div>
            
            </td>

        </tr>

        
    </tbody>
    
    
    @endforeach
        
    
</table>        
        

</form><br><br>

@endsection

