@extends('contenedor')

@section('titulo',"Consulta de Representantes")

@section('panel')
            
<form class="form-horizontal" method="GET" action="{{route('consultar')}}">

    {{ csrf_field() }}

    @if ($message = Session::get('success'))
    
    <div class="alert alert-success">
    
            {{ $message }}
    
        </div>
    
        @endif
    
    @if ($message = Session::get('error'))
        
    <div class="alert alert-danger" >
        
            {{ $message }}
        
        </div>

    @endif
    
    <div class="col text-center" >
            
        <a  href="{{ URL::route('crear') }}" class="btn btn-success" >Agregar nuevo representante</a>
    
    </div> <br> 
 
    
    <table class="table table-hover" >
                
        <tr>
    
            {{-- <th class="text-center">ID</td> --}}

            <th class="text-center">Títular</td>

            <th class="text-center">Nombre</td>
           
            <th class="text-center">Paterno</td>
           
            <th class="text-center">Materno</td>
                
            <th class="text-center">Sexo</td>
    
            <th class="text-center">Ponencia</td>
           
            <th class="text-center">Comisión</td>
           
            <th class="text-center">Acción</td>
               
        </tr> 
        
        @foreach ($datosrepresentante as $dat)
            
        <tr>

            <th class="text-center">{{$dat->titulo}}</th>

            <th class="text-center">{{$dat->nombre}}</th>

            <th class="text-center">{{$dat->paterno}}</th>

            <th class="text-center">{{$dat->materno}}</th>

            <th class="text-center">{{$dat->sexo}}</th>

            <th class="text-center">{{$dat->ponencia->descripcion}}</th>

            <th class="text-center">{{$dat->comision->descripcion}}</th>

            <td class="text-center">

                <div class="text-center">

                    {{-- <form method="GET" action="{{route('editar')}}"> Solo quitar el comentario para que aparezca el boton
                    
                        {{ csrf_field() }}
                    
                        <button type="submit" class="btn btn-warning">Editar</button>
                    
                        <input type="text" name="id_crepresentantep" value={{$dat->id_crepresentantep}} hidden>
                    
                    </form> --}}
                    
                    <form method="GET" action="{{ route('Eliminar') }}">
                    
                        {{ csrf_field() }}
                    
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    
                        <input type="text" name="idUsuario" value={{$dat->id_crepresentantep}} hidden>
                    
                    </form>
                
                </div>

            </td>

        </tr>

        @endforeach
        
    </table>        
        
</form><br><br>

@endsection