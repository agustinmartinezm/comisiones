<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionesMail extends Mailable
{
    use Queueable, SerializesModels;

    public $notificacion;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($notificacion)
    {
        $this->notificacion = $notificacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('daffmendez06@gmail.com')
                    ->view($this->notificacion->vista)
                    ->subject('Notificación Comisiones Transitorias');
    }
}
