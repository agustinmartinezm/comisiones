<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','nombre','paterno','materno', 'email', 'id_crol','id_cponencia','usuario','password','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ponencia(){
        return $this->belongsto('App\Modelos\Catalogos\Cponencia', 'id_cponencia', 'id_cponencia');
    }

    public function rol(){
        return $this->belongsto('App\Modelos\Catalogos\Crol', 'id_crol', 'id_crol');
    }
}
