<?php 
namespace App\Http\ViewComposers;
 
use Illuminate\Contracts\View\View;
use DB;
use Auth;
 
class ProfileComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check()){
            $listaMenu = DB::table('menu_modulos')
                ->join('menu_privilegios', 'menu_modulos.id_menuModulo', '=', 'menu_privilegios.id_menuModulo')->where('menu_privilegios.id_crol', '=', Auth::user()->id_crol)
                ->where('menu_modulos.estatus', '=', 1)
                ->get();

            $view->with('listaMenu', $listaMenu);            
        }
        else{
            $listaMenu = array();
        }
    }
}