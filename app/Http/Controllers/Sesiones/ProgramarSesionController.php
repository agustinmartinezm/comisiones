<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Catalogos\Csede;
use App\Modelos\Catalogos\CtipoSesion;
use App\Modelos\Catalogos\CcaracterSesion;
use App\Modelos\Transacciones\Tsesion;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use App\Modelos\Catalogos\Crepresentantep;
use App\Modelos\Transacciones\TasignacionRepresentante;
use Illuminate\Support\Facades\Input;
use App\Mail\NotificacionesMail;
use Illuminate\Support\Facades\Mail;
use App\User; 
use DB;

use DateTime;
use Auth;

class ProgramarSesionController extends Controller
{

    public function index()
    {
    	$listaComisiones       = Ccomision::pluck('descripcion','id_ccomision');
    	$listaSedes            = Csede::pluck('descripcion','id_csede');
    	$listaTipoSesiones     = CtipoSesion::pluck('descripcion','id_ctipoSesion');
    	$listaCaracterSesiones = CcaracterSesion::pluck('descripcion','id_ccaracterSesion');
        return view('Sesiones/ProgramarSesiones',compact('listaComisiones','listaSedes','listaTipoSesiones','listaCaracterSesiones'));
    }

    public function guardarSesion(Request $request)
    {
        
        $this->validate($request, [
            'comision_id' => 'required'
            //'horaTerm' => 'after_or_equal:horaProg'              
        ]);


        $horaTermRec = Input::get('horaTermRec');
        $horaInicioRec = Input::get('horaInicioRec');

        if(strlen($horaTermRec) >0 || strlen($horaInicioRec)>0 ){
          
            $this->validate($request, [
                    'horaTermRec' => 'after_or_equal:horaInicioRec|before_or_equal:horaTerm',
                    'horaInicioRec' => 'after_or_equal:horaProg|before_or_equal:horaTerm'
                                     
            ]);     
        }
      
        $comision_id       = Input::get('comision_id');
        $sede_id           = Input::get('sede_id');
        $tipoSesion_id     = Input::get('tipoSesion_id');
        $caracterSesion_id = Input::get('caracterSesion_id');
        $fechaProg         = Input::get('fechaProg');
        $horaProg          = Input::get('horaProg');
        $horaTerm          = Input::get('horaTerm');
        $horaInicioRec     = Input::get('horaInicioRec');
        $horaTermRec       = Input::get('horaTermRec');

        if ( is_null($fechaProg)){
            $nfechaProg = null;
        }else{
            $nfechaProg = explode('/',$fechaProg); 
            $fechaProg  = $nfechaProg[2].'-'.$nfechaProg[1].'-'.$nfechaProg[0];

            $validarFechas = Tsesion::where('fecha_programada', '=', $fechaProg)->where('id_ccomision', '=', $comision_id)->get();
            foreach ($validarFechas as $validarFechas) {
                if($validarFechas->id_ccomision == $comision_id){
                    //print_r($validarFechas->id_ccomision);
                    return redirect()->route('programarSesion')->with('error','Error al registrar la sesión. La fecha programada no esta disponible.');
                }
            }

        }

        /*$nfechaProg = explode('/',$fechaProg); 
        $fechaProg  = $nfechaProg[2].'-'.$nfechaProg[1].'-'.$nfechaProg[0];

        $validarFechas = Tsesion::where('fecha_programada', '=', $fechaProg)->where('id_ccomision', '=', $comision_id)->get();
        foreach ($validarFechas as $validarFechas) {
            if($validarFechas->id_ccomision == $comision_id){
                //print_r($validarFechas->id_ccomision);
                return redirect()->route('programarSesion')->with('success','La fecha programada no esta disponible');
            }
        }*/

         $hoy=now();
         $nanio=explode('-',$hoy); 
         $anio=$nanio[0];
         //echo $anio."<br>";
 
         $ultima_sesion = Tsesion::where('id_ccomision','=', $comision_id)->where('anio_sesion','=', $anio)->max('num_sesion');
         
         $num_sesion=$ultima_sesion+1;
         //echo $num_sesion."<br>";

     //Inserta sesion.

        Tsesion::create([
            'id_ccomision' => $comision_id,
            'id_csede' => $sede_id,
            'id_ctipoSesion' => $tipoSesion_id,
            'id_ccaracterSesion' => $caracterSesion_id,
            'fecha_programada' =>$fechaProg,
            'hora_programada' => $horaProg,
            'hora_fin_estimada' => $horaTerm,
            'resceso_inicio' => $horaInicioRec,
            'resceso_fin' => $horaTermRec,
            'id_cestatusSesion' => 1,
            'num_sesion' => $num_sesion,
            'anio_sesion' => $anio,
            'id' => Auth::user()->id,
        ]);

        $max_sesion = Tsesion::max('id_tsesion');

        $representantes = Crepresentantep::where('id_ccomision','=',$comision_id)->where('estatus','=',1)->get();

        foreach ($representantes as $repre) {
            TasignacionRepresentante::create([
                'fk_tsesion' =>$max_sesion,                 
                'fk_crepresentantep'=>$repre->id_crepresentantep,
                'estatus'=>1,
            ]);
        }

//Actualiza numero de seion dependiendo la fecha programada.
        DB::statement(DB::raw('SET @numero = 0'));

        DB::update('UPDATE tsesiones 
              set num_sesion=@numero:=@numero+1 
              where id_ccomision= ? 
              and anio_sesion = ?
              order by fecha_programada is null, fecha_programada ASC', [$comision_id, $anio]);

//Concatena 0 al numero  de sesion 1-9.

        DB::update('UPDATE tsesiones
             SET num_sesion=CONCAT(0, num_sesion)
             where id_ccomision=?
             and `num_sesion`<=9' , [$comision_id]);


        
        //echo "max_sesion: ".$max_sesion."<br>";


//Inserta los dos primeros puntos al orden del dia.
        TpuntoOrdenDia::create([
            'id_tsesion' =>$max_sesion,                 
            'id_ctema'=>1,
            'numero_punto'=>1,
            'punto'=>'Lista de presentes',
            'id_cestatusPunto'=>1,
            'id' => Auth::user()->id      
        ]);


        TpuntoOrdenDia::create([
            'id_tsesion' =>$max_sesion,                 
            'id_ctema'=>1,
            'numero_punto'=>2,
            'punto'=>'Aprobación del orden del día',
            'id_cestatusPunto'=>1,
            'id' => Auth::user()->id
        ]);


        $emails = array();

        $datosActualizados = DB::table('tsesiones')
                       ->where('id_tsesion', '=', $max_sesion)
                       ->get();

        $datosUsuarios = DB::table('users')
                       ->join('usuario_privilegios', 'usuario_privilegios.id', '=', 'users.id')
                       ->where('usuario_privilegios.id_ccomision', '=', $comision_id)
                       ->where('users.status' , '!=', 0)
                       ->where('users.id_cponencia', '!=', 7)
                       ->where('usuario_privilegios.fecha_inicio','<=',$fechaProg)
                       ->where('usuario_privilegios.fecha_fin','>=',$fechaProg)
                       //->orwhere('users.id_cponencia' , '=', 7)
                       ->get();

        foreach ($datosUsuarios as $datosUsuarios) {
            array_push($emails, $datosUsuarios->email);
        }

        //print_r($emails);

        $comision = Ccomision::where('id_ccomision', '=', $comision_id)->get();
        $notificacion           = new \stdClass();
        $notificacion->vista    = 'Mail.SesionRegistrada';
        $notificacion->comision = $comision[0]->descripcion;

        //$emails = ['agustin_gux@hotmail.com','agustin.martinez@tsjcdmx.gob.mx'];
 
        //Mail::to($emails)->send(new NotificacionesMail($notificacion));

        return redirect()->route('seguimientoSesion.index')->with('success','Sesión programada'); 
    }
}
 