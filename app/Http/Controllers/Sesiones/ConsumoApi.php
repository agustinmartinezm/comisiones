<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Transacciones\Tsesion;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use App\Modelos\Transacciones\Tsoporte;


class ConsumoApi extends Controller
{
    public function index()
    {
    	$listaSesiones = Tsesion::where('id_ccomision','=', 6)->pluck('fecha_programada','id_tsesion');


        return view('Sesiones/ConsumoApi',compact('listaSesiones'));
    }

    public function enviaSesion()
    {
    	$id_tsesion = Input::get('id_tsesion');
    	$sesion     = Tsesion::where('id_tsesion', '=', $id_tsesion)/*->where('id_cestatusSesion', '=', 2)*/->get();
        $puntos     = TpuntoOrdenDia::where('id_tsesion', '=', $id_tsesion)/*->where('id_cestatusPunto', '=', 2)*/->get();
        $soportes   = Tsoporte::where('id_tsesion', '=', $id_tsesion)->where('estatus', '=', 1)->get();

        

        $info_sesion = array(
        	"datosSesion"   => json_decode($sesion),
        	"datosPuntos"   => json_decode($puntos),
        	"datosSoportes" => json_decode($soportes),
        	);

        $info_sesionJson = json_encode($info_sesion);

        //echo json_encode($info_sesion, JSON_PRETTY_PRINT);

        /*echo $sesion;*/

        /*$json = '{
	"id_tsesion": 4,
	"id_ccomision": 6,
	"id_csede": 1,
	"id_ctipoSesion": 1,
	"id_ccaracterSesion": 2,
	"fecha_programada": "2019-01-07 00:00:00",
	"hora_programada": null,
	"hora_fin_estimada": "11:00:00",
	"resceso_inicio": null,
	"resceso_fin": null,
	"id_cestatusSesion": 1,
	"num_sesion": "0001",
	"anio_sesion": 2019,
	"id": 3,
	"created_at": "2019-05-08 13:08:15",
	"updated_at": "2019-06-27 14:13:45"
}';*/

        /*$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://172.19.40.58:8000/api?sesion=".$sesionArray."&puntos=".$puntosArray,
		  CURLOPT_URL => "http://172.19.40.58:8000/api?id_tsesion=1&id_Sede=2",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		));
		$response = curl_exec($curl);
		curl_close($curl);
		echo $response;*/



		$curl = curl_init();
		curl_setopt_array($curl, array(
		  //CURLOPT_URL => "http://10.17.5.197:5050/api?=",
		  CURLOPT_URL => "http://172.19.40.58:8000/api?=",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $info_sesionJson,
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json"
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response);
		print("<pre>".print_r($response,true)."</pre>");
		//echo $response;



	    /*$ch = curl_init('http://172.19.40.58:8000/api');                                                                      
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $sesion);                                                                  
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	        'Content-Type: application/json',                                                                                
	        'Content-Length: '.strlen($sesionArray))                                                                       
	    );                                                                                                                   
	                                                                                                                         
	    $resultPrimerLogin = curl_exec($ch);
	    $resultPrimerLoginArray = json_decode($resultPrimerLogin, TRUE);

	    echo $resultPrimerLogin;*/


        /*return view('Sesiones/ConsumoApi',compact('listaSesiones'));*/
    }
}

