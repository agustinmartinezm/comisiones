<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\PhpWord;
use App\Modelos\Transacciones\Tsesion;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Catalogos\Csede;
use App\Modelos\Catalogos\CtipoSesion;
use App\Modelos\Catalogos\CcaracterSesion;
use App\Modelos\Catalogos\Csiglascomision;
use App\Modelos\Catalogos\CpuntosOrdenDia;
use App\Modelos\Catalogos\Cacuerdos;
use App\Modelos\Catalogos\Cleyendas;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use App\Modelos\Catalogos\Crepresentantep;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\Tcomision;
use Illuminate\Support\Facades\Storage;
use App\Modelos\Transacciones\TsesionPonencia;
use App\Modelos\Transacciones\TasignacionRepresentante;
use PhpOffice\PhpWord\Style\Language;
use DateTime;
use DB;
use Auth;

class createMinutaWordDocx extends Controller
{
	public function createMinutaWordDocx()
	{
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		$dias = array("7"=>"domingo","1"=>"lunes","2"=>"martes","3"=>"miércoles","4"=>"jueves","5"=>"viernes","6"=>"sábado");//&eacute;
        $sesion = Input::get('id_tsesion');
		$wordTest = new \PhpOffice\PhpWord\PhpWord();
		$wordTest->getSettings()->setThemeFontLang(new Language(Language::ES_ES));

		$newSection = $wordTest->addSection();
		$header  	= $newSection->addHeader();
		$footer 	= $newSection->addFooter();

		$Tsesion 		= Tsesion::where('id_tsesion', '=', $sesion)->get();
		$Csede 			= Csede::where('id_csede','=',$Tsesion[0]->id_csede)->get();
		$tipoSesion 	= CtipoSesion::where('id_ctipoSesion','=',$Tsesion[0]->id_ctipoSesion)->get();
		$caracterSesion = CcaracterSesion::where('id_ccaracterSesion','=',$Tsesion[0]->id_ccaracterSesion)->get();

		$Comision       = Ccomision::where('id_ccomision','=', $Tsesion[0]->id_ccomision)->get();
		$Puntos 		= CpuntosOrdenDia::where('id_tsesion', '=', $sesion)->where('id_cestatusPunto', '=', 2)->where('id_ctema', '=', 1)->orderBy('numero_punto')->get();
		$PuntosNumerar 	= CpuntosOrdenDia::where('id_tsesion', '=', $sesion)->where('id_cestatusPunto', '!=', 3)->where('numero_punto', '>', 2)->orderBy('numero_punto')->get();

    	$Acuerdos		= Cacuerdos::where('id_tsesion', '=', $sesion)->get();
    	$Leyenda 		= Cleyendas::where('anio_leyenda', '=', $Tsesion[0]->anio_sesion)->where('estatus', '=', 1)->get();
		//La ponencia 7 es la secretaria general
    	$secretariaGral = Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('id_cponencia', '=', 7)->where('estatus', '=', 1)->where('firma', '=', 1)->get();

    	$newSection ->addTextBreak(1);
		$desc1 	= "MINUTA DE LA ". $Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion." SESIÓN ". mb_strtoupper($tipoSesion[0]->descripcion)." DE LA ". mb_strtoupper($Comision[0]->descripcion);		//."<w:br/><w:br/>"
			//Se da el formato necesario a la fecha y se guarda en $desc2
			$fecha 	= $Tsesion[0]->fecha_programada;
			//date("l", $fecha);
			//Recuperar valor numerico del día de la semana
			$diaFecha = strftime('%u', strtotime($fecha));
			//Asignar el valor de acuerdo al arreglo díass
			$diaFecha = $dias[$diaFecha];
			$nfecha   = explode('-',$fecha);

	        $fecha  = $nfecha[2].' de '.$meses[$nfecha[1]-1].' de '.$nfecha[0];
	        $fecha2 = $fecha." ";
			$fec  	= substr($fecha2, 0, 2);
			$ha 	= substr($fecha2, 11, -1);

			$desc2 	= mb_strtoupper($diaFecha.' '.$fec.$ha);

		$table = $header->addTable();
		$table->addRow(1200);
		/*$cell = $table->addCell(2000);
		$cell->addImage(
				    'imagenes/LOGO_PJ.jpg', 
				    array(
				        'width'         => 130,
				        'height'        => 70,
				        'marginTop'     => -1,
				        'marginLeft'    => -1,
				        'wrappingStyle' => 'behind',
				        'align' 		=> 'left',
		  				)
					);*/
		$cell2 = $table->addCell(9500);
		$cell2->addText($Leyenda[0]->leyenda."<w:br/>", array('italic' => true, 'name' => 'Book Antiqua', 'size' => 11), array('alignment' => 'right'));
		//$table->addRow(1200);
		$cell2->addImage(
				    'imagenes/SECRETARIAGRAL.jpg',
				    array(
				        'width'         => 160,
				        'height'        => 30,
				        'marginTop'     => -1,
				        'marginLeft'    => -1,
				        'wrappingStyle' => 'behind',
				        'align' 		=> 'right'
		  				)
					);

		$newSection ->addText($desc1, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
		$newSection ->addText($desc2, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));

		$newSection ->addTextBreak(1);
		$textrun = $newSection->addTextRun(array('alignment' => 'both', 'lineHeight' => 1.5));

		$textrun->addText("En la Ciudad de México, siendo las ".substr($Tsesion[0]->hora_programada,0,5)." hrs del día ".$fec.$ha, array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText(", en términos de lo dispuesto por los numerales 209 y 216 de la ", array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText("Ley Orgánica del Poder Judicial de la Ciudad de México, ", array('name' => 'Book Antiqua', 'size' => 12, 'italic' => true,));
		$textrun->addText("así como a lo establecido en los artículos 7 fracción II, 27, 28, fracción II, 48, 49, 50, 55 fracción V, y 62 fracciones I, VI, XVI y XXII del ", array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText("Reglamento Interior del Consejo de la Judicatura de la Ciudad de México,", array('name' => 'Book Antiqua', 'size' => 12, 'italic' => true,));
		$textrun->addText(" al acuerdo general 21-32/2017, emitido en sesión del cuatro de agosto de dos mil diecisiete, por el cual se aprobaron los", array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText(" “Lineamientos para la Operación de los Sistemas a cargo de la Secretaria General del Consejo de la Judicatura de la Ciudad de México, así como el trámite para la elaboración, revisión y aprobación de las Actas Plenarias y las Minutas de Trabajo de las Comisiones Transitorias“, ", array('name' => 'Book Antiqua', 'size' => 12,'italic' => true));
		$textrun->addText("así como a la integración de las Comisiones Transitorias de ese Órgano Colegiado, aprobadas mediante acuerdo 78-03/2020 y actualizado por el diverso 31-39/2020, emitidos en sesión de fecha  catorce de enero y tres de noviembre, ambos dos mil veinte, respectivamente encontrándose reunidos los consejeros integrantes de ", array('name' => 'Book Antiqua', 'size' => 12, 'lineHeight' => 1.5));
		$textrun->addText("la ".$Comision[0]->descripcion." del H. Consejo, ", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12));
		if($Csede[0]->id_csede == 5){//SI ES POR VIDEOLLAMADA
			$textrun->addText("mediante videoconferencia a través del Sistema Webex,", array('bold' => true,'name' => 'Book Antiqua', 'size' => 12, 'underline' => 'single'));
		}else{
			$textrun->addText("en el inmueble ubicado en ".$Csede[0]->descripcion."," , array('bold' => true,'name' => 'Book Antiqua', 'size' => 12, 'underline' => 'single'));
		}
		//$textrun->addText( $Csede[0]->descripcion , array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText(" se constituyeron en sesión ".$tipoSesion[0]->descripcion." ".$caracterSesion[0]->descripcion.", ante la presencia de la ".$secretariaGral[0]->titulo." ".$secretariaGral[0]->nombre." ".$secretariaGral[0]->paterno." ".$secretariaGral[0]->materno.", secretaria general del propio Consejo de la Judicatura, a efecto de atender y resolver asuntos de su competencia que se encuentran asentados en los puntos del orden del día, respetando con ello las medidas de mitigación, a fin de garantizar, el cumplimiento de las medidas de prevención, establecidas por el órgano colegiado, de conformidad con los ", array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText("“Lineamientos de Seguridad Sanitaria en el Poder Judicial de la Ciudad de México”, ", array('name' => 'Book Antiqua', 'size' => 12, 'italic' => true,));
		$textrun->addText("aprobados mediante acuerdo ", array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText("08-19/2020", array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true,'underline' => 'single'));
		$textrun->addText(" y en relación con el diverso ", array('name' => 'Book Antiqua', 'size' => 12));
		$textrun->addText("05-19/2020", array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true,'underline' => 'single'));
		$textrun->addText(" de fecha nueve de junio, ambos del año dos mil veinte. ---------------", array('name' => 'Book Antiqua', 'size' => 12));
		$newSection->addText("El orden del día, se circuló entre los señores consejeros, a razón de lo siguiente: ------------", array('name' => 'Book Antiqua', 'size' => 12));

		$segundoParrafo = "1. Lista de presentes. -------------------------------------------------------------------------------------------------------------------";
		$newSection->addText($segundoParrafo, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
		$tercerParrafo = "2. Presentación y aprobación del orden del día, de conformidad con los siguientes puntos a tratar. -------------";
		$newSection->addText($tercerParrafo, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));

		$cont = 3;
		foreach ($PuntosNumerar as $PuntosNumerar) {
			$newSection ->addText($cont++.". ".$PuntosNumerar->punto, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
		}

		$newSection ->addTextBreak(2);

		/*PARA OBTENER LOS TITULOS DE CADA PUNTO, AL PRINCIPIO DEL DOCUMENTO*/
		

		$tituloPresentes = "1. LISTA DE PRESENTES";
		$newSection->addText($tituloPresentes, array('bold' => true,'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
		$primerParrafoPresentes = "La Secretaria General del Consejo tomó lista de asistencia de los integrantes de la ".$Comision[0]->descripcion." del Tribunal Superior de Justicia de la Ciudad de México. ----------------";
		$newSection->addText($primerParrafoPresentes, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));

		$asignacionRepresentante = TasignacionRepresentante::where('fk_tsesion', '=', $sesion)->where('estatus', '=', 1)->count();

		if($asignacionRepresentante == 0){

			$Representanteg 	= Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('estatus', '=', 1)->where('firma', '=', 1)->where('id_cponencia', '!=', 7)->where('id_cponencia', '!=', 8)->get();

			foreach ($Representanteg as $Representante) {
				if($Representante->sexo == "M") {
					$textrun = $newSection->addTextRun();
					$repres = $Representante->nombre." ".$Representante->paterno." ".$Representante->materno;
					$textrun->addText("Consejero ", array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
					$textrun->addText($repres, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
				}else if ($Representante->sexo == "F") {
					$textrun = $newSection->addTextRun();
					$repres = $Representante->nombre." ".$Representante->paterno." ".$Representante->materno;
					$textrun->addText("Consejera ", array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
					$textrun->addText($repres, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
				}
		    }
		}else{
			$Representanteg = TasignacionRepresentante::with('representante','sesion')->where('fk_tsesion', '=', $sesion)->where('estatus', '=', 1)->get();

			foreach ($Representanteg as $Representante) {
				if($Representante->representante->sexo == "M" && $Representante->representante->id_cponencia != 7) {
					$textrun = $newSection->addTextRun();
					$repres = $Representante->representante->nombre." ".$Representante->representante->paterno." ".$Representante->representante->materno;
					$textrun->addText("Consejero ", array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
					$textrun->addText($repres, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
				}else if ($Representante->representante->sexo == "F" && $Representante->representante->id_cponencia != 7) {
					$textrun = $newSection->addTextRun();
					$repres = $Representante->representante->nombre." ".$Representante->representante->paterno." ".$Representante->representante->materno;
					$textrun->addText("Consejera ", array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
					$textrun->addText($repres, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
				}
		    }
		}

		/*$Representanteg 	= Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('estatus', '=', 1)->where('firma', '=', 1)->where('id_cponencia', '!=', 7)->where('id_cponencia', '!=', 8)->get();
		foreach ($Representanteg as $Representante) {
			if ($Representante->sexo == "F") {
				$textrun = $newSection->addTextRun();
				$repres = $Representante->nombre." ".$Representante->paterno." ".$Representante->materno;
				$textrun->addText("Consejera ", array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
				$textrun->addText($repres, array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
			}
	    }*/

	    $segundoParrafoPresentes = "Registrada que fue la asistencia y encontrándose presentes los señores consejeros mencionados, se declaró formalmente abierta e instaurada la sesión de la presente Comisión-----------------------------------------------------";
	    $newSection->addText($segundoParrafoPresentes, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));

	    $newSection ->addTextBreak(2);
	    $tituloPresentes = "2. COMENTARIOS Y APROBACIÓN, EN SU CASO, DE LOS PUNTOS DE ACUERDO DEL ORDEN DEL DÍA";
		$newSection->addText($tituloPresentes, array('bold' => true,'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));

		$segundoParrafoPresentes = "Registrada que fue la asistencia y encontrándose presentes los señores consejeros mencionados, se declaró formalmente abierta e instaurada la sesión de la presente Comisión-----------------------------------------------------";
	    $newSection->addText($segundoParrafoPresentes, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    $newSection ->addTextBreak(2);

		//$newSection ->addText("INICIO: ".$Tsesion[0]->hora_programada." HRS", array('name' => 'Calibri', 'size' => 12), array('alignment' => 'both'));
		//$newSection -> addText("Acuerdo:", array('name' => 'Calibri', 'size' => 12), array('alignment' => 'both'));
		$valida3 = 0;
		$valida4 = 0;
		$valida5 = 0;
		$valida6 = 0;
		$cont = 3;
		foreach ($Puntos as $Puntos) {
			if (($Puntos->id_ctema == 1) && ($Puntos->numero_punto >= 3)) {
				//$newSection ->addText(mb_strtoupper($cont++.". ".$Puntos->punto), array('bold' => true, 'name' => 'Calibri', 'size' => 11), array('alignment' => 'left'));
				$newSection ->addText("Acuerdo ".$Comision[0]->siglas."-0".$Puntos->numero_punto."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true), array('alignment' => 'center'));
	    		/*foreach ($Acuerdos as $Acuerdos1) {
	    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
	    				$acuerdo = explode("\n", $Acuerdos1->acuerdo);	
	    				foreach($acuerdo as $acuerdo){
	    					$newSection ->addText(htmlspecialchars($acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    				} 
	    				
	    			}
	    		}*/
	    	}else if($Puntos->id_ctema == 3){
	    		if ($valida3 == 0) {
	    			$newSection -> addText('REPORTE DE VISITA DE SUPERVISIÓN', array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			$valida3 = 1;
	    		}
	    		//$newSection ->addText(mb_strtoupper($cont++.". ".$Puntos->punto), array('bold' => true, 'name' => 'Calibri', 'size' => 11), array('alignment' => 'left'));
	    		$newSection ->addText("Acuerdo ".$Comision[0]->siglas."-0".$Puntos->numero_punto."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true), array('alignment' => 'center'));
	    		/*foreach ($Acuerdos as $Acuerdos1) {
	    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
	    				$newSection ->addText(htmlspecialchars($Acuerdos1->acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			}
	    		}*/
	    	}else if($Puntos->id_ctema == 4){
	    		if ($valida4 == 0) {
	    			$newSection -> addText('REPORTE DE SIMULACRO', array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'left'));
	    			$valida4 = 1;
	    		}
	    		//$newSection ->addText(mb_strtoupper($cont++.". ".$Puntos->punto), array('bold' => true, 'name' => 'Calibri', 'size' => 10), array('alignment' => 'left'));
	    		$newSection ->addText("Acuerdo ".$Comision[0]->siglas."-0".$Puntos->numero_punto."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true), array('alignment' => 'center'));
	    		/*foreach ($Acuerdos as $Acuerdos1) {
	    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
	    				$newSection ->addText(htmlspecialchars($Acuerdos1->acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			}
	    		}*/
	    	}else if($Puntos->id_ctema == 5){
	    		if ($valida5 == 0) {
	    			$newSection -> addText('SISTEMA INFORMÁTICO DE INTERCAMBIO DE PEDIMENTOS Y LIBERTADES', array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			$valida5 = 1;
	    		}
	    		//$newSection ->addText(mb_strtoupper($cont++.". ".$Puntos->punto), array('name' => 'Calibri', 'size' => 10), array('alignment' => 'left'));
	    		$newSection ->addText("Acuerdo ".$Comision[0]->siglas."-0".$Puntos->numero_punto."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true), array('alignment' => 'center'));
	    		/*foreach ($Acuerdos as $Acuerdos1) {
	    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
	    				$newSection ->addText(htmlspecialchars($Acuerdos1->acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			}
	    		}*/
	    	}else if($Puntos->id_ctema == 6){
	    		if ($valida6 == 0) {
	    			$newSection -> addText('RESGUARDO DE VIDEOS', array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			$valida6 = 1;
	    		}
	    		//$newSection ->addText(mb_strtoupper($cont++.". ".$Puntos->punto), array('bold' => true, 'name' => 'Calibri', 'size' => 10), array('alignment' => 'left'));
	    		$newSection ->addText("Acuerdo ".$Comision[0]->siglas."-0".$Puntos->numero_punto."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true), array('alignment' => 'center'));
	    		/*foreach ($Acuerdos as $Acuerdos1) {
	    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
	    				$newSection ->addText(htmlspecialchars($Acuerdos1->acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    			}
	    		}*/
	    	}
	    	
	    	foreach ($Acuerdos as $Acuerdos1) {
    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
    				$acuerdo = explode("\n", $Acuerdos1->acuerdo);  
    				foreach($acuerdo as $acuerdo){
    					$newSection ->addText(htmlspecialchars($acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
    					
    				} 
    			}
    		}
    		
		}
		$newSection ->addTextBreak(2);
		$newSection-> addText("ASUNTOS ADICIONALES", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
		$Puntos 		= CpuntosOrdenDia::where('id_tsesion', '=', $sesion)->get();
		foreach ($Puntos as $Puntos) {
			if ($Puntos->id_ctema == 2) {
			//$newSection ->addText(mb_strtoupper($cont++.". ".$Puntos->punto), array('bold' => true, 'name' => 'Calibri', 'size' => 11));
			$newSection ->addText("Acuerdo ".$Comision[0]->siglas."-0".$Puntos->numero_punto."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('name' => 'Book Antiqua', 'size' => 12, 'bold' => true), array('alignment' => 'center'));
	    	foreach ($Acuerdos as $Acuerdos1) {
	    			if ($Acuerdos1->id_puntoOrdenDia == $Puntos->id_puntoOrdenDia) {
	    				$acuerdo = explode("\n", $Acuerdos1->acuerdo);
	    				
	    				foreach($acuerdo as $acuerdo){
	    					$newSection ->addText(htmlspecialchars($acuerdo), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'both'));
	    				} 
	    				//$newSection ->addText(htmlspecialchars($Acuerdos1->acuerdo), array('name' => 'Book Antiqua', 'size' => 1), array('alignment' => 'both'));
	    			}
	    		}
	    	}
		}

		$newSection ->addTextBreak(2);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if($asignacionRepresentante == 0){

			$Representanteg 	= Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('estatus', '=', 1)->where('firma', '=', 1)->where('id_cponencia', '!=', 7)->where('id_cponencia', '!=', 8)->get();
			foreach ($Representanteg as $Representante) {
				if ($Representante->sexo == "M") {
					$newSection ->addText("CONSEJERO", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
					$newSection ->addTextBreak(2);
			    	$repres = mb_strtoupper($Representante->nombre." ".$Representante->paterno." ".$Representante->materno);
			    	$newSection ->addText($repres, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
			    	$newSection ->addTextBreak(2);
				}else if ($Representante->sexo == "F") {
					$newSection ->addText("CONSEJERA", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
					$newSection ->addTextBreak(2);
			    	$repres = mb_strtoupper($Representante->nombre." ".$Representante->paterno." ".$Representante->materno);
			    	$newSection ->addText($repres, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
			    	$newSection ->addTextBreak(2);
				}
		    }
		}else{

			$Representanteg = TasignacionRepresentante::with('representante','sesion')->where('fk_tsesion', '=', $sesion)->where('estatus', '=', 1)->get();

			foreach ($Representanteg as $Representante) {
				if ($Representante->representante->sexo == "M" && $Representante->representante->id_cponencia != 7) {
					$newSection ->addText("CONSEJERO", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
					$newSection ->addTextBreak(2);
			    	$repres = mb_strtoupper($Representante->representante->nombre." ".$Representante->representante->paterno." ".$Representante->representante->materno);
			    	$newSection ->addText($repres, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
			    	$newSection ->addTextBreak(2);
				}else if ($Representante->representante->sexo == "F" && $Representante->representante->id_cponencia != 7) {
					$newSection ->addText("CONSEJERA", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
					$newSection ->addTextBreak(2);
			    	$repres = mb_strtoupper($Representante->representante->nombre." ".$Representante->representante->paterno." ".$Representante->representante->materno);
			    	$newSection ->addText($repres, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
			    	$newSection ->addTextBreak(2);
				}
		    }
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		//////////////////////////////CODIGO ANTERIOR////////////////////////////////////////////////////////////
		/*$Representanteg 	= Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('estatus', '=', 1)->where('firma', '=', 1)->where('id_cponencia', '!=', 7)->where('id_cponencia', '!=', 8)->get();
		foreach ($Representanteg as $Representante) {
			if ($Representante->sexo == "M") {
				$newSection ->addText("CONSEJERO", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
				$newSection ->addTextBreak(2);
		    	$repres = mb_strtoupper($Representante->nombre." ".$Representante->paterno." ".$Representante->materno);
		    	$newSection ->addText($repres, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
		    	$newSection ->addTextBreak(2);
			}
	    }

		$Representanteg 	= Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('estatus', '=', 1)->where('firma', '=', 1)->where('id_cponencia', '!=', 7)->where('id_cponencia', '!=', 8)->get();
		foreach ($Representanteg as $Representante) {
			if ($Representante->sexo == "F") {
				$newSection ->addText("CONSEJERA", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
				$newSection ->addTextBreak(2);
		    	$repres = mb_strtoupper($Representante->nombre." ".$Representante->paterno." ".$Representante->materno);
		    	$newSection ->addText($repres, array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
		    	$newSection ->addTextBreak(2);
			}
	    }*/
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	   $RepresentanteDEGT = Crepresentantep::where('id_ccomision', '=', $Tsesion[0]->id_ccomision)->where('id_cponencia', '=', 8)->where('estatus', '=', 1)->where('firma', '=', 1)->get();
	  //  dd($RepresentanteDEGT);

	    /*if ($Comision[0]->id_ccomision == 6) {
	    	$newSection ->addText("DIRECTOR EJECUTIVO DE GESTIÓN TECNOLÓGICA DEL TRIBUNAL SUPERIOR DE JUSTICIA DE LA CIUDAD DE MÉXICO", array('bold' => true, 'name' => 'Calibri', 'size' => 10), array('alignment' => 'center'));
			$newSection ->addTextBreak(2);
			$newSection ->addText(mb_strtoupper($RepresentanteDEGT[0]->nombre." ".$RepresentanteDEGT[0]->paterno." ".$RepresentanteDEGT[0]->materno), array('name' => 'Calibri', 'size' => 10), array('alignment' => 'center'));
			$newSection ->addTextBreak(2);
	    }//$RepresentanteDEGT[0]->titulo." ".*/

		
		$newSection ->addText("SECRETARIA GENERAL DEL CONSEJO DE LA JUDICATURA DE LA CIUDAD DE MÉXICO", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));
		$newSection ->addTextBreak(2);

		if($asignacionRepresentante == 0){
			$newSection ->addText("ZAIRA LILIANA JIMÉNEZ SEADE", array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));//'italic' => true, 
			$newSection ->addTextBreak(2);
		}else{
			$Representanteg = TasignacionRepresentante::with('representante','sesion')->where('fk_tsesion', '=', $sesion)->where('estatus', '=', 1)->get();

			foreach ($Representanteg as $Representante) {
				if ($Representante->representante->id_cponencia == 7) {
					$newSection ->addText(mb_strtoupper($Representante->representante->nombre." ".$Representante->representante->paterno." ".$Representante->representante->materno), array('name' => 'Book Antiqua', 'size' => 12), array('alignment' => 'center'));//'italic' => true, 
				}
		    }		    
			
			$newSection ->addTextBreak(2);
		}

		/*if ($Comision[0]->id_ccomision != 7) {
			$textRun = $footer->addTextRun(array('alignment' => 'right', 'name' => 'Book Antiqua', 'size' => 12, 'spaceAfter' => 0));
			$textRun->addField('PAGE');
			$footer ->addText("MINUTA DE TRABAJO CORRESPONDIENTE A LA ". $Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion." SESIÓN DE LA ". mb_strtoupper($Comision[0]->descripcion)." DEL TRIBUNAL SUPERIOR DE JUSTICIA DE LA CIUDAD DE MÉXICO, CELEBRADA EL ".$desc2.".", array('italic' => true, 'name' => 'Book Antiqua', 'size' => 12, 'spaceAfter' => 0), array('alignment' => 'both'));
		}elseif ($Comision[0]->id_ccomision == 7) {
			$textRun = $footer->addTextRun(array('alignment' => 'right', 'name' => 'Book Antiqua', 'size' => 12, 'spaceAfter' => 0));
			$textRun->addField('PAGE');
			$footer ->addText("MINUTA DE TRABAJO CORRESPONDIENTE A LA ". mb_strtoupper($Comision[0]->descripcion)." DEL TRIBUNAL SUPERIOR DE JUSTICIA DE LA CIUDAD DE MÉXICO, CELEBRADA EL ".$desc2.".", array('italic' => true, 'name' => 'Book Antiqua', 'size' => 12, 'spaceAfter' => 0), array('alignment' => 'both'));
			
		}*/
		$table = $footer->addTable();
		$table->addRow(1200);
		$cell = $table->addCell(2000);
		$cell->addText("MINUTA DE TRABAJO<w:br/>".$Comision[0]->siglas."-".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion, array('bold' => true,'italic' => true, 'name' => 'Book Antiqua', 'size' => 7.5), array('alignment' => 'left'));
		$cell2 = $table->addCell(9500);
		//$cell2->addField('PAGE');
		$cell2->addPreserveText("{PAGE}<w:br/>".$Tsesion[0]->num_sesion."/".$Tsesion[0]->anio_sesion." SESIÓN ". mb_strtoupper($tipoSesion[0]->descripcion)."<w:br/>".mb_strtoupper($Comision[0]->descripcion)."<w:br/>".$desc2, array('bold' => true,'italic' => true, 'name' => 'Book Antiqua', 'size' => 7.5), array('alignment' => 'right'));
		//$table->addRow(1200);

		$nombre = $Tsesion[0]->num_sesion."a. Minuta".mb_substr($Comision[0]->descripcion,11, 33)." ".$fec.$ha;
		
		$objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
		try {
			$objectWriter->save($nombre.'.docx');
			//$objectWriter->save(storage_path($nombre.'.docx'));
		} catch (Exception $e) {
		}

		header('Content-Description: File Transfer');
		header('Content-type: application/force-download');
		header('Content-Disposition: attachment; filename ='.basename($nombre.'.docx'));
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize($nombre.'.docx'));
		readfile($nombre.'.docx');

		//return response()->download(storage_path($nombre.'.docx'));
		unlink ($nombre.'.docx');
		//unlink (storage_path ($nombre.'.docx'));
		//Storage::delete($nombre.'.docx');
		
	}
}