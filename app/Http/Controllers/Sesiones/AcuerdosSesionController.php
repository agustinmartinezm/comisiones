<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Transacciones\Tsesion;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use App\Modelos\Transacciones\Tacuerdo;
use App\Modelos\Transacciones\Tobservacion;
use App\Modelos\Transacciones\TsesionPonencia;
use App\Modelos\Catalogos\Cponencia;
use App\Modelos\Catalogos\Crepresentantep;
use App\Modelos\Transacciones\Tbitacora;
use Illuminate\Support\Facades\Input;
use App\Modelos\Catalogos\Ccomision;
use App\Mail\NotificacionesMail;
use Illuminate\Support\Facades\Mail;
use DB;
use Auth;

class AcuerdosSesionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $id_puntoOrdenDia = Input::get('id_puntoOrdenDia');
      $id_tsesion       = Input::get('id_tsesion');
      $acuerdo          = Input::get('acuerdo');
      $validarStatus    = Tsesion::where('id_tsesion', '=', $id_tsesion)->get();
      //if ($validarStatus[0]->id_cestatusSesion != 3) {

        $acuerdoC = Tacuerdo::where("id_puntoOrdenDia", "=", $id_puntoOrdenDia)->count();//verifica si hay acuerdo capturado para ese punto

        if ($acuerdoC > 0){
            $acuerdoR = Tacuerdo::where("id_puntoOrdenDia", "=", $id_puntoOrdenDia)->get();
            $actualizaAcuerdo = new AcuerdosSesionController();
            $actualizaAcuerdo->update($request, $acuerdoR[0]->id_tacuerdo,$acuerdo);
        }else{
            Tacuerdo::create([
                'id_tsesion'       => $id_tsesion,
                'id_puntoOrdenDia' => $id_puntoOrdenDia,
                'acuerdo'          => $acuerdo,
                'version'          => 1,
                'estatus'          => 1,
                'id'               => Auth::user()->id,
            ]);

            return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('success','Acuerdo Guardado'); 

        }
      /*}else{
            return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('error','No es posible editar, hasta que las ponencias liberen observaciones');
      }    */
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)//id_tsesion
    {
      $rol = Auth::user()->id_crol;//Recupera rol de usuario
      if($rol == 1){ //si el usuario tiene rol de secretaría general se meustra la captura de acuerdos
          $id_puntoOrdenDia = NULL;
          $listaTemas    = TpuntoOrdenDia::where('id_tsesion', "=", $id)->where('id_cestatusPunto', "=", 2)->orderBy('numero_punto')->get();
          $datosSesion   = Tsesion::where('id_tsesion','=', $id)->get();
          $estatusSesion = $datosSesion[0]->id_cestatusSesion;
          //print_r($estatusSesion);
          return view('AcuerdosSesiones/show',compact('id_puntoOrdenDia','listaTemas','datosSesion', 'estatusSesion'));
      }
      else
      {// si no , muestra la pantalla de captura de observaciones
          $id_puntoOrdenDia = NULL;
          $listaAcuerdos = Tacuerdo::where('id_tsesion', "=", $id)->where('estatus', "=", 2)->count();//cuenta los acuerdos liberados
          if(0 < $listaAcuerdos){//valida si hay acuedos liberados
            $id_puntoOrdenDia = NULL;
            $listaTemas    = TpuntoOrdenDia::where('id_tsesion', "=", $id)->where('id_cestatusPunto', "=", 2)->orderBy('numero_punto')->get();
            $datosSesion   = Tsesion::where('id_tsesion','=', $id)->get();
        
            $TsesionPonencia = TsesionPonencia::where('id_tsesion', '=', $id)->where('id_cponencia', '=', Auth::user()->id_cponencia)->get();
            $TsesionPonenciaEstatus = $TsesionPonencia[0]->estatus;
              //print_r($TsesionPonenciaEstatus);
            return view('AcuerdosSesiones/showPonencia',compact('id_puntoOrdenDia','listaTemas','datosSesion', 'TsesionPonenciaEstatus'));
          }else
          {
            return redirect()->route('seguimientoSesion.consultaSesion')->with('error','Sin Acuerdos Liberados');
          }
      }     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $acuerdo)
    {
      $datosAcuerdo = Tacuerdo::where("id_tacuerdo", "=", $id)->get();
      $id_tsesion   = $datosAcuerdo[0]->id_tsesion;
      $tacuerdo     = Tacuerdo::find($id);
      $tacuerdo->acuerdo = $acuerdo;
      $tacuerdo->save();
      return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('success','Acuerdo Actualizado')->send();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function capturaAcuerdo(Request $request)
    {
      $id_puntoOrdenDia   = Input::get('id_puntoOrdenDia');
      $id_tsesion         = Input::get('id_tsesion');
      $listaTemas         = TpuntoOrdenDia::where('id_tsesion', "=", $id_tsesion)->where('id_cestatusPunto', "=", 2)->orderBy('numero_punto')->get();//array con los temas de la sesión que tienen acuerdos liberados
      $datosSesion        = Tsesion::where('id_tsesion','=', $listaTemas[0]->id_tsesion)->get();//infromación de la sesión
      $datosTema          = TpuntoOrdenDia::where('id_puntoOrdenDia','=', $id_puntoOrdenDia)->get();//datos cada uno de los temas de la sesión
      $listaObservaciones = Tobservacion::where('id_tsesion', "=", $id_tsesion)->where('id_puntoOrdenDia','=', $id_puntoOrdenDia)->get();//datos de las observaciones de los acuerdos
      //$datosSesion   = Tsesion::where('id_tsesion','=', $id_tsesion)->get();
      $estatusSesion = $datosSesion[0]->id_cestatusSesion;
      return view('AcuerdosSesiones/show',compact('id_puntoOrdenDia','listaTemas','datosSesion','datosTema','listaObservaciones', 'estatusSesion'));
    }

   

    public function liberarMinuta(){
      $id_tsesion         = Input::get('id_tsesion');
      $datosSesion        = Tsesion::where('id_tsesion', '=', $id_tsesion)->get();
      $puntosCapturados   = TpuntoOrdenDia::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->count();
      $acuerdosCapturados = Tacuerdo::where('id_tsesion', '=', $id_tsesion)->where('estatus', '!=', 0)->count();
      $contador = $puntosCapturados;


      if($contador == $acuerdosCapturados){
        //$sesionLiberada   = Tsesion::where('id_tsesion', "=", $id_tsesion)->where('id_cestatusSesion', '=', 2)->update(['id_cestatusSesion' => 3]);//Cambia estatus en tsesiones a "Acuerdos Liberados"

        $sesionLiberada   = Tsesion::where('id_tsesion', "=", $id_tsesion)->where(function ($query) {
                              $query->where('id_cestatusSesion', '=', 2)
                              ->orWhere('id_cestatusSesion', '=', 4);
                            })->update(['id_cestatusSesion' => 4]);
        $acuerosLiberados = Tacuerdo::where('id_tsesion', '=', $id_tsesion)->where('estatus', '=', 1)->update(['estatus' => 2]);//Cambia estatus de tacuerdos a "Liberado"
        $sesion = TsesionPonencia::where("id_tsesion", "=", $id_tsesion)->where('estatus', '=', 2)->update(['estatus' => 1]);


        $ponencias = Crepresentantep::where('id_ccomision', '=', $datosSesion[0]->id_ccomision)->where('estatus','=', 1)->get();
        DB::update('UPDATE tacuerdos
                    set version=version+1 
                    where id_tsesion = ?', [$id_tsesion]);//Actualiza versión de los acuerdos

        foreach ($ponencias as $ponencias) {
          $validacion = TsesionPonencia::where('id_tsesion', '=', $id_tsesion)->where('id_cponencia', '=', $ponencias->id_cponencia)->where('estatus', '=', 1)->where('id', '=', Auth::user()->id)->count();
          if ($validacion > 0) {
              TsesionPonencia::where('id_tsesion', '=', $id_tsesion)->where('id_cponencia', '=', $ponencias->id_cponencia)->update(['estatus' => 2]);

          }else{
            TsesionPonencia::create([
                'id_tsesion'   => $id_tsesion,
                'id_cponencia' => $ponencias->id_cponencia,
                'estatus'      => 1,
                'id'           => Auth::user()->id,
              ]);
          }
        }

        if($datosSesion[0]->id_cestatusSesion==2 || $datosSesion[0]->id_cestatusSesion==4) { 
          Tbitacora::create([
              'id_cmovimiento'       => 2,
              'id'                   => Auth::user()->id,
              'id_tsesion'           => $id_tsesion,
          ]);                 
        }

        $emails = array();
        $comision_id=$datosSesion[0]->id_ccomision;
        $num_sesion=$datosSesion[0]->num_sesion;
        $anio_sesion=$datosSesion[0]->anio_sesion;
        $datosUsuarios = DB::table('users')
                       ->join('usuario_privilegios', 'usuario_privilegios.id', '=', 'users.id')
                       ->where('usuario_privilegios.id_ccomision', '=', $comision_id)
                       ->where('users.status' , '!=', 0)
                       ->where('users.id_cponencia', '!=', 7)
                       ->where('usuario_privilegios.fecha_inicio','<=',$datosSesion[0]->fecha_programada)
                       ->where('usuario_privilegios.fecha_fin','>=',$datosSesion[0]->fecha_programada)
                       //->orwhere('users.id_cponencia' , '=', 7)
                       ->get();

        foreach ($datosUsuarios as $datosUsuarios){
          array_push($emails, $datosUsuarios->email);
        }
        $comision = Ccomision::where('id_ccomision', '=', $comision_id)->get();
        $notificacion           = new \stdClass();
        $notificacion->vista    = 'Mail.LiberarMinuta';
        $notificacion->comision = $comision[0]->descripcion;
        $notificacion->num_sesion=$num_sesion;
        $notificacion->anio_sesion=$anio_sesion;

       //Mail::to($emails)->send(new NotificacionesMail($notificacion));

        return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('success','Minuta Liberada');
      }
      else{
        return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('error','Error al liberar minuta, no todos los temas tienen acuerdo.');
      }

     

      /*$id_tsesion   = Input::get('id_tsesion');
      $sesion = Tsesion::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusSesion', '=', 1)->update(['id_cestatusSesion' => 2]);
      $puntos = TpuntoOrdenDia::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusPunto', '=', 1)->update(['id_cestatusPunto' => 2]);
      return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('success','Sesión Liberada');*/
    }
     public function capturaObservacion(Request $request)
    {
      $id_puntoOrdenDia = Input::get('id_puntoOrdenDia');
      $id_tsesion       = Input::get('id_tsesion');
      $listaTemas       = TpuntoOrdenDia::where('id_tsesion', "=", $id_tsesion)->where('id_cestatusPunto', "=", 2)->orderBy('numero_punto')->get();
      $datosSesion      = Tsesion::where('id_tsesion','=', $listaTemas[0]->id_tsesion)->get();
      $datosTema        = TpuntoOrdenDia::where('id_puntoOrdenDia','=', $id_puntoOrdenDia)->get();
      $observaciones    = Tobservacion::where('id_cponencia','=',  Auth::user()->id_cponencia)->where('id_puntoOrdenDia','=', $id_puntoOrdenDia)->get();
      $TsesionPonencia  = TsesionPonencia::where('id_tsesion', '=', $id_tsesion)->where('id_cponencia', '=', Auth::user()->id_cponencia)->get();
      $TsesionPonenciaEstatus = $TsesionPonencia[0]->estatus;
      return view('AcuerdosSesiones/showPonencia',compact('id_puntoOrdenDia','listaTemas','datosSesion','datosTema','observaciones', 'TsesionPonenciaEstatus'));
    }

    public function guardaObservacion(Request $request)
    {
      $id_puntoOrdenDia = Input::get('id_puntoOrdenDia');
      $id_tsesion       = Input::get('id_tsesion');
      $id_tacuerdo      = Input::get('id_tacuerdo');
      $observacion      = Input::get('observacion');
      $user             = Auth::user()->id;
      $id_cponencia     = Auth::user()->id_cponencia;

      //$observaC = Tobservacion::where("id_tacuerdo", "=", $id_tacuerdo)->where("id", "=", $user)->count();
      $observaC = Tobservacion::where("id_tacuerdo", "=", $id_tacuerdo)->where("id_cponencia", "=", $id_cponencia)->count();
      //verifica si hay observaciones capturadas para ese punto 
       echo $observaC;

      //$datosObservaC = Tobservacion::where("id_tacuerdo", "=", $id_tacuerdo)->where("id_cponencia", "=", $id_cponencia)->get();TsesionPonencia
       $datosObservaC = TsesionPonencia::where("id_tsesion", "=", $id_tsesion)->where("id_cponencia", "=", $id_cponencia)->get();

      if ($observaC > 0){

        if($datosObservaC[0]->estatus ==1){
          Tobservacion::where('id_tacuerdo', $id_tacuerdo)
                    ->where("id", "=", $user)
                    ->update(['observacion' => $observacion]); 

          return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('success','Observación actualizada');  
        }else{
          return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('error','No es posible editar las observaciones una vez liberadas.');
        }  
      }else{
          Tobservacion::create([
              'id_tsesion'       => $id_tsesion,
              'id_cponencia'     => Auth::user()->id_cponencia,
              'id_puntoOrdenDia' => $id_puntoOrdenDia,
              'id_tacuerdo'      => $id_tacuerdo,
              'observacion'      => $observacion,                
              'estatus'          => 1,
              'id'               => Auth::user()->id,
          ]);

          return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('success','Observación guardada');   
      }  
      } 

    

    public function liberarObserva(){
      $id_tsesion      = Input::get('id_tsesion');
      /*  $puntosCapturados   = TpuntoOrdenDia::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->count();
        $acuerdosCapturados = Tacuerdo::where('id_tsesion', '=', $id_tsesion)->count();*/
      $datosSesion  = Tsesion::where('id_tsesion','=',$id_tsesion)->get();
      $user         = Auth::user()->id;
      $ponenciaUser = Auth::user()->id_cponencia;
      $sesion = TsesionPonencia::where("id_tsesion", "=", $id_tsesion)->where("id_cponencia", "=", $ponenciaUser)->where('estatus', '=', 1)->update(['estatus' => 2]);
      $ponencia = Cponencia::where("id_cponencia","=",$ponenciaUser)->get();

      $datosSesionPon = TsesionPonencia::where("id_tsesion", "=", $id_tsesion)->where("id_cponencia", "=", $ponenciaUser)->where('estatus', '=', 2)->get();
       // $observacionesLiberadas = Tobservacion::where('id_tsesion', '=', $id_tsesion)->where('estatus', '=', 1)->update(['estatus' => 2]);//Cambia estatus de tobservaciones a "Liberado"

      if($datosSesionPon[0]->estatus==2) { 
        Tbitacora::create([
            'id_cmovimiento'       => 3,
            'id'                   => Auth::user()->id,
            'id_tsesion'           => $id_tsesion,
        ]);                                    
      } 

      $ponenciasIntegrantes = TsesionPonencia::where("id_tsesion", "=", $id_tsesion)->where("id_cponencia", "!=", 7)->count();//Cuenta el total de ponencias
      $ponenciasObservaciones = TsesionPonencia::where("id_tsesion", "=", $id_tsesion)->where('estatus', '=', 2)->where("id_cponencia", "!=", 7)->count();//Cuenta el total de ponencias con observaciones liberadas

      if($ponenciasIntegrantes == $ponenciasObservaciones){//si todas las ponencias tienen observaciones liberadas
        Tsesion::where('id_tsesion', "=", $id_tsesion)->where('id_cestatusSesion', '=', 3)->update(['id_cestatusSesion' => 4]);//Cambia estatus en tsesiones a "Observaciones Liberadas"

        $status_sesion = Tsesion::where('id_tsesion','=', $id_tsesion)->get();
        if($status_sesion[0]->id_cestatusSesion==4) { 
          Tbitacora::create([
              'id_cmovimiento'       => 4,
              'id'                   => NULL,
              'id_tsesion'           => $id_tsesion,
          ]);    
        } 
      }

      $emails = array();
      $comision_id =$datosSesion[0]->id_ccomision;
      $ponenciaUser=$datosSesion[0]->id_cponencia;
      $num_sesion  =$datosSesion[0]->num_sesion;
      $anio_sesion =$datosSesion[0]->anio_sesion;
      /*$datosUsuarios = DB::table('users')
                       ->join('usuario_privilegios', 'usuario_privilegios.id', '=', 'users.id')
                       ->where('usuario_privilegios.id_ccomision', '=', $comision_id)
                       ->where('users.status' , '!=', 0)
                       //->orwhere('users.id_cponencia' , '=', 7)
                       ->get();*/
        $datosUsuarios = DB::table('users')
                       ->where('users.status' , '!=', 0)
                       ->where('users.id_cponencia' , '=', 7)
                       ->get();

        foreach ($datosUsuarios as $datosUsuarios){
          array_push($emails, $datosUsuarios->email);
        }
        $comision = Ccomision::where('id_ccomision', '=', $comision_id)->get();
        $notificacion           = new \stdClass();
        $notificacion->vista    = 'Mail.ObservacionesLiberadasPorPonencias';
        $notificacion->comision = $comision[0]->descripcion;
        $notificacion->ponencia = $ponencia[0]->descripcion;
        $notificacion->num_sesion=$num_sesion;
        $notificacion->anio_sesion=$anio_sesion;

        //Mail::to($emails)->send(new NotificacionesMail($notificacion));

      return redirect()->route('acuerdoSesion.show',$id_tsesion)->with('success','Observaciones Liberadas');
    }
}
