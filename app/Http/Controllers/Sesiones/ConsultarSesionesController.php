<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Catalogos\Csede;
use App\Modelos\Catalogos\CtipoSesion;
use App\Modelos\Catalogos\CcaracterSesion;
use App\Modelos\Transacciones\Tsesion;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\Tcomision;
use App\Modelos\Transacciones\Tsoporte;
use App\Modelos\Transacciones\Tacuerdo;
use DB;
use DateTime;
use Auth;

class ConsultarSesionesController extends Controller
{
    public function index()
    {	
    	$num_sesion 		= Input::get('num_sesion');
    	$anio_sesion 		= Input::get('anio_sesion');
    	$comision 			= Input::get('comision');
    	$descComision 		= Input::get('descComision');
    	$tipo 				= Input::get('tipo');
    	$fecha 				= Input::get('fecha');
    	$hora 				= Input::get('hora');
    	$sede 				= Input::get('sede');
        $tsesion            = Input::get('tsesion');
        //consulta lista puntos con estatus 2
        if(Auth::user()->id_crol == 1){
            //$listaPuntos = TpuntoOrdenDia::where('id_tsesion', '=', $tsesion)->where('id_cestatusPunto', '=', 1)->orwhere('id_cestatusPunto', '=', 2)->orderBy('numero_punto')->get();
            $listaPuntos =  TpuntoOrdenDia::
                                where('id_tsesion', '=', $tsesion)
                                    ->where(function ($query) {
                                        $query->where('id_cestatusPunto', '=', 1)
                                        ->orwhere('id_cestatusPunto', '=', 2);
                                    })
                                    ->orderBy('numero_punto')->get();
        }else{
            $listaPuntos = TpuntoOrdenDia::where('id_tsesion', '=', $tsesion)->where('id_cestatusPunto', '=', 2)->orderBy('numero_punto')->get();
        }
        
        $listaSoporte       = Tsoporte::where('id_tsesion', '=', $tsesion)->where('estatus', '=', 1)->get();
        /*print_r($listaSoporte);
        foreach ($listaSoporte as $listaSoporte) {
            echo $listaSoporte->id_tsoporte;
        }*/


        return view('Sesiones/ConsultarSesiones',compact('num_sesion','anio_sesion','comision','descComision','tipo', 'fecha','hora','sede','tsesion','listaPuntos','listaSoporte'));

    }

    public function downloadSoporte($id)
    {
        $Soporte    = Tsoporte::where('id_tsoporte','=', $id)->get();
        $pathtoFile = public_path().'/'.$Soporte[0]->ruta;
       // echo $pathtoFile;
        //print_r($Soporte);

        return response()->download($pathtoFile);
    }

    public function ConsutaSesionFinal()
    {
        $id_tsesion    = Input::get('id_tsesion');
        $datosSesion   = Tsesion::where('id_tsesion','=', $id_tsesion)->get();
        $listaPuntos   = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->orderBy('numero_punto')->get();
        $acuerdos      = Tacuerdo::where('id_tsesion','=', $id_tsesion)->get();
        $minuta        = Tsoporte::where('id_tsesion','=', $id_tsesion)->whereNull('id_puntoOrdenDia')->get();
        //print_r($minuta);
        return view('Sesiones/ConsutaSesionFinal',compact('datosSesion','listaPuntos','acuerdos','minuta'));
    }

    public function downloadMinuta()
    {
        $id           = Input::get('id');
        $listaSoporte = Tsoporte::where('id_tsoporte','=', $id)->get();
        $pathtoFile   = public_path().'/'.$listaSoporte[0]->ruta;
        // echo $pathtoFile;
        //print_r($listaSoporte);
        return response()->download($pathtoFile);
    }
}
