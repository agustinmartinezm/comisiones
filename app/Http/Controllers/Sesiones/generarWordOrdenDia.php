<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\PhpWord;
use App\Modelos\Transacciones\Tsesion;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Catalogos\Csede;
use App\Modelos\Catalogos\CtipoSesion;
use App\Modelos\Catalogos\CcaracterSesion;
use App\Modelos\Catalogos\CpuntosOrdenDia;
use App\Modelos\Catalogos\Crepresentantep;
use App\Modelos\Catalogos\Cponencia;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\Tcomision;
use PhpOffice\PhpWord\Style\Language;
use App\Modelos\Transacciones\TasignacionRepresentante;
use DateTime;
use DB;
use Auth, Session;

class generarWordOrdenDia extends Controller
{
    public function createWordDocx()
	{
		$meses 		= array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		//$nfecha		=explode('/',Input::get('fecha')); 
		$nfecha		= Input::get('fecha'); 

		if($nfecha == "Por Definir"){
			$fecha  = "Por Definir";
			$fecha2 = "Por Definir";
		}else{
			$nfechaexp  = explode('/',$nfecha);
			$fecha 		= $nfechaexp[0].' de '.$meses[$nfechaexp[1]-1].' de '.$nfechaexp[2];
        	$fecha2 	= mb_strtoupper($fecha);
		}
        
    	$hora 		= Input::get('hora');

		$wordTest 	= new \PhpOffice\PhpWord\PhpWord();
		 
		$wordTest->getSettings()->setThemeFontLang(new Language(Language::ES_ES));

		$newSection = $wordTest->addSection();
		//$header  	= $newSection->addHeader();
		$footer 	= $newSection->addFooter();

		$Comision         = Ccomision::where('id_ccomision','=', Input::get('comision'))->get();
    	$Sede        	  = Csede::where('id_csede', '=', Input::get('sede'))->get();
    	$TipoSesion       = CtipoSesion::where('id_ctipoSesion', '=', Input::get('tipo'))->get();
    	$CaracterSesione  = CcaracterSesion::where('id_ccaracterSesion', '=', Input::get('caracter'))->get();

    	if(Auth::user()->id_crol == 1){//Si es secretaria general puede ver puntos capturados y liberados
    		$Puntos =   TpuntoOrdenDia::
                            where('id_tsesion', '=', Input::get('tsesion'))
                                ->where(function ($query) {
                                    $query->where('id_cestatusPunto', '=', 1)
                                    ->orwhere('id_cestatusPunto', '=', 2);
                                })
                                ->orderBy('numero_punto')->get();
    	}else{
    		$Puntos = CpuntosOrdenDia::where('id_tsesion', '=', Input::get('tsesion'))->where('id_cestatusPunto', '=', 2)->orderBy('numero_punto')->get();
    	}

    	$asignacionRepresentante = TasignacionRepresentante::where('fk_tsesion', '=', Input::get('tsesion'))->where('estatus', '=', 1)->count();

    	if($asignacionRepresentante == 0){//Obtener representantes antes del cambio para asignarlos por sesion (sesiones del 2022 para atras)
    		$Representante = Crepresentantep::where('id_ccomision', '=', Input::get('comision'))->where('estatus', '=', 1)->orderBy('id_cponencia')->get();

    		$footer ->addText("Consejeros Integrantes de la ".$Comision[0]->descripcion.".", array('name' => 'Calibri', 'size' => 11, 'spaceAfter' => 0));
	    	foreach ($Representante as $Representante) {
	    		$Cponencia = Cponencia::where('id_cponencia', '=', $Representante->id_cponencia)->get();
	    		$ponencia = $Cponencia[0]->descripcion;
		    		$repres = $ponencia." ".$Representante->titulo." ".$Representante->nombre." ".$Representante->paterno." ".$Representante->materno;
		    		$footer ->addText($repres, array('name' => 'Calibri', 'size' => 11, 'spaceBefore' => 0));
		    	$Cponencia = "";
	    	}
    	}else{
    		$Representante = TasignacionRepresentante::with('representante','sesion')->where('fk_tsesion', '=', Input::get('tsesion'))->where('estatus', '=', 1)->get()->sortBy('representante.id_cponencia');

    		$footer ->addText("Consejeros Integrantes de la ".$Comision[0]->descripcion.".", array('name' => 'Calibri', 'size' => 11, 'spaceAfter' => 0));
	    	foreach ($Representante as $Representante) {
	    		$Cponencia = Cponencia::where('id_cponencia', '=', $Representante->representante->id_cponencia)->get();
	    		$ponencia = $Cponencia[0]->descripcion;
		    		$repres = $ponencia." ".$Representante->representante->titulo." ".$Representante->representante->nombre." ".$Representante->representante->paterno." ".$Representante->representante->materno;
		    		$footer ->addText($repres, array('name' => 'Calibri', 'size' => 11, 'spaceBefore' => 0));
		    	$Cponencia = "";
	    	}
	    	
    	}

    	$comision 	= mb_strtoupper($Comision[0]->descripcion);
    	$tipoSesion = mb_strtoupper($TipoSesion[0]->descripcion);
    	$sede 		= mb_strtoupper($Sede[0]->descripcion);

		$desc1 = $comision;
		$desc2 = $tipoSesion;

			$newSection ->addText($desc1, array('name' => 'Calibri', 'size' => 12), array('alignment' => 'center'));
			$newSection -> addImage(
					    'imagenes/cinta_orden_dia.png',
					    array(
					        'width'         => 450,
					        //'height'        => ,
					        'marginTop'     => -1,
					        'marginLeft'    => -1,
					        'wrappingStyle' => 'behind',
					        'align' 		=> 'center'
			  				)
						);
			$newSection ->addText("SESIÓN ".$desc2, array('name' => 'Calibri', 'size' => 12), array('alignment' => 'center'));
			$newSection ->addText($desc1, array('name' => 'Calibri', 'size' => 12), array('alignment' => 'center'));
			$newSection ->addText($fecha2." ".$hora, array('name' => 'Calibri', 'size' => 12), array('alignment' => 'center'));
			$newSection ->addText($sede, array('name' => 'Calibri', 'size' => 12), array('alignment' => 'center'));
			$newSection ->addText("ORDEN DEL DÍA", array('bold' => true, 'name' => 'Book Antiqua', 'size' => 11.5), array('alignment' => 'center'));
			$newSection ->addText("LA ".$desc1." POR CONDUCTO DE LA SECRETARÍA GENERAL, SOMETE A CONSIDERACIÓN EL SIGUIENTE: ------------", array('name' => 'Calibri', 'size' => 11), array('alignment' => 'both'));

			//$num = 1;
	    	foreach ($Puntos as $Puntos) {
	    		$punto = $Puntos->numero_punto.".-".$Puntos->punto;
	    		$punto = explode("\n", $punto);	    		
	    		foreach($punto as $punto) {
	    			$newSection ->addText(htmlspecialchars($punto), array('name' => 'Calibri', 'size' => 11));
	    		}
	    		$newSection ->addTextBreak(1);
	    		//$num = $num + 1;
	    		//$newSection ->addTextBreak(1);
	    	}

	    	
	    	$nombre = Input::get('num_sesion')."a. Orden del Día".mb_substr($Comision[0]->descripcion,11, 33)." ".$fecha;

		$objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
		try {
			$objectWriter->save($nombre.'.docx');
			//$objectWriter->save(storage_path($nombre.'.docx'));
		} catch (Exception $e) {
		}

		header('Content-Description: File Transfer');
		header('Content-type: application/force-download');
		header('Content-Disposition: attachment; filename ='.basename($nombre.'.docx'));
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize($nombre.'.docx'));
		readfile($nombre.'.docx');
		
		//return response()->download(storage_path($nombre.'.docx'));
		unlink ($nombre.'.docx');
	}
}
