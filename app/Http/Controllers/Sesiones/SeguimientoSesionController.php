<?php

namespace App\Http\Controllers\Sesiones; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Transacciones\Tsesion;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use App\Modelos\Transacciones\Tsoporte;
use App\Modelos\Catalogos\Cprioridad;
use App\Modelos\Catalogos\Ctema;
use App\Modelos\Catalogos\TemaPrivilegio;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\Tcomision;
use App\Modelos\Catalogos\CusuarioPrivilegios;
use App\Modelos\Transacciones\Tbitacora;
use App\Mail\NotificacionesMail;
use Illuminate\Support\Facades\Mail;
use DateTime;
use DB;
use Auth;

class SeguimientoSesionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ((Auth::user()->id_crol == 2) || (Auth::user()->id_crol == 3)) {//Si el usuario es de ponencia o de consulta
            $privilegios = Auth::user()->id;
            $listaComisiones = DB::table('ccomisiones')
            ->join('usuario_privilegios', 'ccomisiones.id_ccomision', '=', 'usuario_privilegios.id_ccomision')
            ->join('users', 'usuario_privilegios.id', '=', 'users.id')
            ->where('users.id', '=', $privilegios)
            ->get();
           /*foreach ($listaComisiones as $listaComisiones) {
                echo $listaComisiones->id_ccomision.",";
                echo $listaComisiones->descripcion.",";
            }*/
            //print_r($listaComisiones);
            //print_r(Auth::user()->id);

            $listaSesiones = NULL;
        }elseif (Auth::user()->id_crol == 1) {//Si el usuario es de secretaria general
            $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');
            $listaSesiones = NULL;
        }
        return view('SeguimientoSesiones/index',compact('listaComisiones', 'listaSesiones'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_tsesion    = Input::get('id_tsesion');
        $datos_tsesion = Tsesion::where('id_tsesion', "=", $id_tsesion)->get();//Recupera datos de la sesión
        $id_ctema      = Input::get('id_ctema');
        $tema          = $request->input('tema');
        $id_ccomision  = $datos_tsesion[0]->id_ccomision;//Recupera id_ccomision para utilizar en el switch 

        switch($id_ccomision){
            case 1://Comisión de Vigilancia del Instituto de Ciencias Forenses
            case 4://Comisión de Medios Alternos
            case 5://Comisión de Seguridad de los CENDI
            case 7://Comisión de Seguimiento de Asuntos Jurídicos del Consejo de la Judicatura de la Ciudad de México (Acuerdo 41-14/2017)
                switch ($id_ctema) {//Orden de temas: punto y asunto adicional 
                    case 1://Punto
                        $ultimoPunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 
                        $ultimoPunto = $ultimoPunto+1;

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoPunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);

                        $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 2)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');

                        if($ultimoAsunto > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ?
                                  and id_ctema= 2  
                                  and id_cestatusPunto != 3
                                  order by numero_punto', [$id_tsesion]);
                        }             
                        break;

                    case 2://Asunto Adicional
                        $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoAsunto)){
                            $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }else{
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoAsunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);
                        break;
                }
                break;
            case 2://Comisión de Protección Civil
                switch ($id_ctema) {//Orden de temas: punto, reportes de simulacros, reportes de visitas de supervisión y asunto adicional
                    case 1://Punto
                        $ultimoPunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 
                        $ultimoPunto = $ultimoPunto+1;

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoPunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','!=', 1)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ?
                                  and id_ctema != 1  
                                  and id_cestatusPunto != 3
                                  order by numero_punto', [$id_tsesion]);
                        }             
                        break;

                    case 2://Asunto Adicional
                        $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoAsunto)){
                            $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }else{
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoAsunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);
                        break;

                    case 4://Reporte de Simulacro
                        $ultimoReporteSimulacro = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoReporteSimulacro)){
                            $ultimoReporteSimulacro = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 1)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoReporteSimulacro = $ultimoReporteSimulacro + 1;
                        }else{
                            $ultimoReporteSimulacro = $ultimoReporteSimulacro + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoReporteSimulacro,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);                        

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 2)->orWhere('id_ctema','=', 3)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ?
                                  and id_ctema = 2  
                                  or id_ctema = 3
                                  and id_cestatusPunto != 3
                                  order by numero_punto', [$id_tsesion]);
                        }    
                        break;

                    case 3://Reporte de Visitas de Supervisión
                        $ultimoReporteVisita = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoReporteVisita)){
                            $ultimoReporteVisita = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 1)->orWhere('id_ctema','=', 4)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoReporteVisita = $ultimoReporteVisita + 1;
                        }else{
                            $ultimoReporteVisita = $ultimoReporteVisita + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoReporteVisita,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);                        

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 2)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ?
                                  and id_ctema = 2  
                                  and id_cestatusPunto != 3
                                  order by numero_punto', [$id_tsesion]);
                        }    
                        break;   
                }
                break;

            case 3://Comisión de Seguridad
                switch ($id_ctema) {//Orden de temas: punto, resguardo de videos y asunto adicional
                    case 1://Punto
                        $ultimoPunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 
                        $ultimoPunto = $ultimoPunto+1;

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoPunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','!=', 1)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ?
                                  and id_ctema != 1  
                                  and id_cestatusPunto != 3
                                  order by numero_punto', [$id_tsesion]);
                        }             
                        break;

                    case 2://Asunto Adicional
                        $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoAsunto)){
                            $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }else{
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoAsunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);
                        break;

                    case 6://Resguardo de Videos
                        $ultimoResguardo = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoResguardo)){
                            $ultimoResguardo = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 1)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoResguardo = $ultimoResguardo + 1;
                        }else{
                            $ultimoResguardo = $ultimoResguardo + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoResguardo,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);                        

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 2)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ? 
                                  and id_ctema = 2  
                                  and id_cestatusPunto != 3
                                  order by numero_punto' , [$id_tsesion]);
                        }    
                        break;
                }
                break;

            case 6://Comisión de Seguimiento a la Implementación de Tecnología Judicial
                switch ($id_ctema) {//Orden de temas: punto, Sistema Informático de Intercambio de Pedimentos y Libertades y asunto adicional
                    case 1://Punto
                        $ultimoPunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 
                        $ultimoPunto = $ultimoPunto+1;

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoPunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','!=', 1)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ?
                                  and id_ctema != 1  
                                  and id_cestatusPunto != 3
                                  order by numero_punto', [$id_tsesion]);
                        }             
                        break;

                    case 2://Asunto Adicional
                        $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoAsunto)){
                            $ultimoAsunto = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }else{
                            $ultimoAsunto = $ultimoAsunto + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoAsunto,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);
                        break;

                    case 5://Sistema Informático de Intercambio de Pedimentos y Libertades
                        $ultimoSIPEL = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', $id_ctema)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');  

                        if (!isset($ultimoSIPEL)){
                            $ultimoSIPEL = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 1)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');
                            $ultimoSIPEL = $ultimoSIPEL + 1;
                        }else{
                            $ultimoSIPEL = $ultimoSIPEL + 1;
                        }

                        $tema = TpuntoOrdenDia::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_ctema'         => $id_ctema,
                            'numero_punto'     => $ultimoSIPEL,
                            'punto'            => $tema,
                            'id_cestatusPunto' => 1,
                            'id' => Auth::user()->id,
                        ]);                        

                        $otrosTemas = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_ctema','=', 2)->where('id_cestatusPunto', '!=', 3)->max('numero_punto'); 

                        if($otrosTemas > 0){
                            DB::update('UPDATE tpuntos_orden_dia 
                                  set numero_punto=numero_punto+1 
                                  where id_tsesion = ? 
                                  and id_ctema = 2  
                                  and id_cestatusPunto != 3
                                  order by numero_punto' , [$id_tsesion]);
                        }    
                        break;
                }
                break;
        }

        $anio        = date("Y");//año de la sesión
        $datosSesion = Tsesion::where('id_tsesion','=', $id_tsesion)->get();//consulta para recuperar id de la comisión

        foreach($_FILES["archivo"]['tmp_name'] as $key => $tmp_name)
        {           
            if($_FILES["archivo"]["name"][$key]) {//Validamos que el archivo exista
                $filename   = $_FILES["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                $source     = $_FILES["archivo"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo   

                $tipo = $_FILES["archivo"]["type"][$key];
                $peso = $_FILES["archivo"]["size"][$key];

                if (($tipo == "application/pdf" || $tipo == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && $peso < 15000000){
                    $directorio = 'soportes/'.$anio.'/'.$datosSesion[0]->id_ccomision.'/'.$id_tsesion.'/'. $tema->id_puntoOrdenDia;//Declaramos un  variable con la ruta donde guardaremos los archivos.                    
                    if(!file_exists($directorio)){//Validamos si la ruta de destino existe, en caso de no existir la creamos
                        mkdir($directorio, 0777,true) or die("No se puede crear el directorio de extracci&oacute;n");    
                    }
                    
                    $dir=opendir($directorio); //Abrimos el directorio de destino
                    $target_path = $directorio.'/'.$filename; //Indicamos la ruta de destino, así como el nombre del archivo
                    
                    //Movemos y validamos que el archivo se haya cargado correctamente
                    //El primer campo es el origen y el segundo el destino
                    if(move_uploaded_file($source, $target_path)) { 
                        Tsoporte::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_puntoOrdenDia' => $tema->id_puntoOrdenDia,
                            'ruta'             => $target_path,
                            'nombre'           => $filename,
                            'id'               => Auth::user()->id,
                            'estatus'          => 1,
                        ]);
                        
                        } 

                    else{    
                        echo "Ha ocurrido un error, por favor inténtelo de nuevo.<br>";
                    }
                    closedir($dir); //Cerramos el directorio de destino
                    
                }else{
                    return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('error','Tema Agregado, error al cargar archivo(s). No es un tipo de archivo valido y/o a es demasiado pesado.');
                }
            }
        }
        return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('success','Tema Agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datosSesion  = Tsesion::where('id_tsesion','=', $id)->get();
        $listaTemas   = TemaPrivilegio::join('ctemas', 'tema_privilegios.id_ctema', '=', 'ctemas.id_ctema')->where('tema_privilegios.id_ccomision', '=', $datosSesion[0]->id_ccomision)->get();
        $listaPuntos  = TpuntoOrdenDia::where('id_tsesion','=', $id)->where('id_cestatusPunto', '!=', 3)->orderBy('numero_punto')->get();
        $listaSoporte = Tsoporte::where('id_tsesion','=', $id)->where('estatus', '=', 1)->get();

        return view('SeguimientoSesiones/show',compact('datosSesion','listaTemas','listaPuntos','listaSoporte'));


       // print_r($listaSoporte);

        return view('SeguimientoSesiones/show',compact('datosSesion','listaPuntos'));


    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function consultaSesion()
    {
        $comision_id     = Input::get('comision_id');
        $anio            = Input::get('anio');
        $comision        = Ccomision::where('id_ccomision','=', $comision_id)->get();
        $listaSesiones   = NULL;

        if (Auth::user()->id_crol == 1) {//Si el usuario es de secretaria general
            $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');
            $listaSesiones   = Tsesion:: where('id_ccomision','=', $comision_id)->where('anio_sesion', '=', $anio)->with('tipoSesion','caracterSesion','comision')->orderByRaw('fecha_programada is null, fecha_programada ASC')->get();//->orderBy('fecha_programada', 'asc')->get();
        }elseif ((Auth::user()->id_crol == 2) || (Auth::user()->id_crol == 3)) {//Si el usuario es de ponencia o de consulta
            $privilegios = Auth::user()->id;
            $listaComisiones = DB::table('ccomisiones')
            ->join('usuario_privilegios', 'ccomisiones.id_ccomision', '=', 'usuario_privilegios.id_ccomision')
            ->join('users', 'usuario_privilegios.id', '=', 'users.id')
            ->where('users.id', '=', $privilegios)
            ->get();
            $user = CusuarioPrivilegios::where('id', '=', $privilegios)->get();
            foreach ($user as $user) {
                if ($user->id_ccomision == $comision_id) {
                    /*$listaSesiones   = Tsesion:: where('id_ccomision','=', $comision_id)->where('anio_sesion', '=', $anio)->where('id_cestatusSesion', '!=', 1)->with('tipoSesion','caracterSesion','comision')->orderByRaw('fecha_programada is null, fecha_programada ASC')->get();*/
                    $listaSesiones   = Tsesion:: where('id_ccomision','=', $comision_id)->where('anio_sesion', '=', $anio)->where('id_cestatusSesion', '!=', 1)->whereBetween('fecha_programada', [$user->fecha_inicio, $user->fecha_fin])->with('tipoSesion','caracterSesion','comision')->orderByRaw('fecha_programada is null, fecha_programada ASC')->get();
                }
            }
        } 

        if (is_null($listaSesiones)) {
            return view('SeguimientoSesiones/index',compact('listaComisiones','comision','listaSesiones'));
        }
        if (isset($listaSesiones)) {
            return view('SeguimientoSesiones/index',compact('listaComisiones','comision','listaSesiones'));
        }
        //return view('SeguimientoSesiones/index',compact('listaComisiones','comision','listaSesiones'));
    }

    public function acciones($id_tsesion,$id_puntoOrdenDia,$num_punto,$bandera) 
    {
        switch ($bandera) {
            case 1://Sube una posición
                if(($num_punto -1) <= 0){
                    return redirect()->route('seguimientoSesion.show',$id_tsesion);
                }
                else{
                    DB::update('UPDATE tpuntos_orden_dia 
                                SET numero_punto = numero_punto+1 
                                WHERE id_tsesion = ?
                                AND numero_punto = ?
                                AND id_cestatusPunto != 3', [$id_tsesion,($num_punto-1)]);
                    $punto = TpuntoOrdenDia::find($id_puntoOrdenDia);
                    $punto->numero_punto = $num_punto-1;
                    $punto->save();
                return redirect()->route('seguimientoSesion.show',$id_tsesion);
                }
                
                break;
            case 2://Baja una posición
                $ultimOPunto  = TpuntoOrdenDia::where('id_tsesion','=', $id_tsesion)->where('id_cestatusPunto', '!=', 3)->max('numero_punto');

                if($ultimOPunto == $num_punto){
                    return redirect()->route('seguimientoSesion.show',$id_tsesion);
                }
                else{
                    DB::update('UPDATE tpuntos_orden_dia 
                                SET numero_punto = numero_punto-1 
                                WHERE id_tsesion = ?
                                AND numero_punto = ?
                                AND id_cestatusPunto != 3', [$id_tsesion,($num_punto+1)]);
                    $punto = TpuntoOrdenDia::find($id_puntoOrdenDia);
                    $punto->numero_punto = $num_punto+1;
                    $punto->save();

                    return redirect()->route('seguimientoSesion.show',$id_tsesion);
                }
                
                break;
            case 3://Elimina 
                $punto = TpuntoOrdenDia::find($id_puntoOrdenDia);
                $punto->id_cestatusPunto = 3;
                $punto->save();

                DB::update('UPDATE tacuerdos
                            SET estatus = 0 
                            WHERE id_puntoOrdenDia = ?', [$id_puntoOrdenDia]);

                DB::update('UPDATE tpuntos_orden_dia 
                            SET numero_punto = numero_punto-1 
                            WHERE id_tsesion = ?
                            AND numero_punto > ?
                            AND id_cestatusPunto != 3', [$id_tsesion,$num_punto]);

                

                return redirect()->route('seguimientoSesion.show',$id_tsesion);             

                break;
        }
    }



    public function downloadSoporte($id)
    {
        $listaSoporte = Tsoporte::where('id_tsoporte','=', $id)->get();
        $pathtoFile = public_path().'/'.$listaSoporte[0]->ruta;
        return response()->download($pathtoFile);
    }

    public function editaPunto(Request $request)
    {
        $tema       = Input::get('tema1');
        $id_orden   = Input::get('id_orden1');
        $id_tsesion = Input::get('id_sesion');

        TpuntoOrdenDia::where('id_puntoOrdenDia', $id_orden)->update(['punto' => $tema]);

        $anio        = date("Y");//año de la sesión
        $datosSesion = Tsesion::where('id_tsesion','=', $id_tsesion)->get();//consulta para recuperar id de la comisión

        foreach($_FILES["archivo"]['tmp_name'] as $key => $tmp_name)
        {           
            if($_FILES["archivo"]["name"][$key]) {//Validamos que el archivo exista
                $filename   = $_FILES["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                $source     = $_FILES["archivo"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo   

                $tipo = $_FILES["archivo"]["type"][$key];
                $peso = $_FILES["archivo"]["size"][$key];

                if ($tipo == "application/pdf" && $peso < 15000000){
                    $directorio = 'soportes/'.$anio.'/'.$datosSesion[0]->id_ccomision.'/'.$id_tsesion.'/'. $id_orden;//Declaramos un  variable con la ruta donde guardaremos los archivos.                    
                    if(!file_exists($directorio)){//Validamos si la ruta de destino existe, en caso de no existir la creamos
                        mkdir($directorio, 0777,true) or die("No se puede crear el directorio de extracci&oacute;n");    
                    }
                    
                    $dir=opendir($directorio); //Abrimos el directorio de destino
                    $target_path = $directorio.'/'.$filename; //Indicamos la ruta de destino, así como el nombre del archivo
                    
                    //Movemos y validamos que el archivo se haya cargado correctamente
                    //El primer campo es el origen y el segundo el destino
                    if(move_uploaded_file($source, $target_path)) { 
                        Tsoporte::create([
                            'id_tsesion'       => $id_tsesion,
                            'id_puntoOrdenDia' => $id_orden,
                            'ruta'             => $target_path,
                            'nombre'           => $filename,
                            'id'               => Auth::user()->id,
                            'estatus'          => 1,
                        ]);
                        
                        } 

                    else{    
                        echo "Ha ocurrido un error, por favor inténtelo de nuevo.<br>";
                    }
                    closedir($dir); //Cerramos el directorio de destino
                    
                }else{
                    return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('error','Tema actualizado, error al cargar archivo(s). No es un tipo de archivo valido y/o a es demasiado pesado.');
                }
            }
        }
        return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('success','Tema actualizado');
    }  

      public function eliminaArchivo($id)
    {
        $id_orden   = Input::get('id_orden1');
        $id_tsesion = Input::get('id_sesion');

        Tsoporte::where('id_tsoporte', $id)
                      ->update(['estatus' => 0]);

        return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('success','El archivo ha sido eliminado exitosamente');
    }

    public function liberar(){        
        $id_tsesion = Input::get('id_tsesion');
        $sesion     = Tsesion::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusSesion', '=', 1)->update(['id_cestatusSesion' => 2]);
        $puntos     = TpuntoOrdenDia::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusPunto', '=', 1)->update(['id_cestatusPunto' => 2]);
        $status_sesion = Tsesion::where('id_tsesion','=', $id_tsesion)->get();
        if($status_sesion[0]->id_cestatusSesion==2) {
            Tbitacora::create([
                'id_cmovimiento'       => 1,
                'id'                   => Auth::user()->id,
                'id_tsesion'           => $id_tsesion,
            ]);          
        }

        if($status_sesion[0]->id_ccomision == 6){
            $sesion   = Tsesion::where('id_tsesion', '=', $id_tsesion)->get();
            $puntos   = TpuntoOrdenDia::where('id_tsesion', '=', $id_tsesion)->where('id_cestatusPunto', '=', 2)->get();
            $soportes = Tsoporte::where('id_tsesion', '=', $id_tsesion)->where('estatus', '=', 1)->get();

            $info_sesion = array(
                "datosSesion"   => json_decode($sesion),
                "datosPuntos"   => json_decode($puntos),
                "datosSoportes" => json_decode($soportes),
            );

            $info_sesionJson = json_encode($info_sesion);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://10.17.5.197:5050/api?=",
                //CURLOPT_URL => "http://172.19.40.58:8000/api?=",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $info_sesionJson,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
        }

        $emails = array();
        $comision_id=$status_sesion[0]->id_ccomision;
        $num_sesion=$status_sesion[0]->num_sesion;
        $anio_sesion=$status_sesion[0]->anio_sesion;
        $datosUsuarios = DB::table('users')
                       ->join('usuario_privilegios', 'usuario_privilegios.id', '=', 'users.id')
                       ->where('usuario_privilegios.id_ccomision', '=', $comision_id)
                       ->where('users.status' , '!=', 0)
                       ->where('users.id_cponencia', '!=', 7)
                       ->where('usuario_privilegios.fecha_inicio','<=',$status_sesion[0]->fecha_programada)
                       ->where('usuario_privilegios.fecha_fin','>=',$status_sesion[0]->fecha_programada)
                       //->orwhere('users.id_cponencia' , '=', 7)
                       ->get();

        foreach ($datosUsuarios as $datosUsuarios){

             array_push($emails, $datosUsuarios->email);

        }
        $comision = Ccomision::where('id_ccomision', '=', $comision_id)->get();
        $notificacion           = new \stdClass();
        $notificacion->vista    = 'Mail.LiberarOrdenDelDia';
        $notificacion->comision = $comision[0]->descripcion;
        $notificacion->num_sesion=$num_sesion;
        $notificacion->anio_sesion=$anio_sesion;

        //Mail::to($emails)->send(new NotificacionesMail($notificacion));


        return redirect()->route('seguimientoSesion.show',$id_tsesion)->with('success','Sesión Liberada'); 
    }       
}