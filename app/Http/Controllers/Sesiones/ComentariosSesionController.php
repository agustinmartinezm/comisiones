<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Transacciones\Tsesion;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\Tbitacora;
use App\Modelos\Transacciones\Tcomentario;
use DateTime;
use DB;
use Auth;

class ComentariosSesionController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $datosSesion      = Tsesion::where('id_tsesion','=', $id)->get();
        $listaComentarios = Tcomentario::where('id_tsesion','=', $id)->where('estatus','=', 1)->get();
        return view('SeguimientoSesiones/comentarios',compact('datosSesion','listaComentarios'));
    }

    public function guardaComentario(){

    	$id_tsesion  = Input::get('id_tsesion');
        $comentario  = Input::get('comentario');

        Tcomentario::create([
            'id_tsesion' => $id_tsesion,                 
            'comentario' => $comentario,
            'estatus'    => 1,
            'id'         => Auth::user()->id
        ]);

        Tbitacora::create([
            'id_cmovimiento'       => 6,
            'id'                   => Auth::user()->id,
            'id_tsesion'           => $id_tsesion,
        ]);

        return redirect()->route('comentariosSesion', ['id'=> $id_tsesion])->with('success','Comentario Agregado');
    }

    public function eliminaComentario(){

    	$id_tcomentario = Input::get('id_tcomentario');
    	$id_tsesion     = Input::get('id_tsesion');

    	Tcomentario::where('id_tcomentario', "=", $id_tcomentario)->update(['estatus' => 0]);

        return redirect()->route('comentariosSesion', ['id'=> $id_tsesion])->with('success','Comentario Eliminado');
    }
}
