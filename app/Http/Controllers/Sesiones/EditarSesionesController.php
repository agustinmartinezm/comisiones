<?php

namespace App\Http\Controllers\Sesiones;

use Illuminate\Http\Request;
use App\Modelos\Catalogos\Crepresentantep;
use App\Http\Controllers\Controller;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Catalogos\Csede;
use App\Modelos\Catalogos\CtipoSesion;  
use App\Modelos\Catalogos\CcaracterSesion;
use App\Modelos\Transacciones\Tsesion;
use App\Modelos\Catalogos\Cponencia;
use App\Modelos\Catalogos\CusuarioPrivilegios;
use App\Modelos\Transacciones\TpuntoOrdenDia;
use Illuminate\Support\Facades\Input;
use App\Modelos\Transacciones\Tcomision;
use App\Modelos\Transacciones\Tsoporte;
use App\Mail\NotificacionesMail;
use App\Modelos\Catalogos\Crol;
use App\User;
use Illuminate\Support\Facades\Mail;
use DB;
use DateTime;
use Auth;

class EditarSesionesController extends Controller
{
    public function index()
    {
    	$listaComisiones        = Ccomision::where('id_ccomision','!=', Input::get(base64_decode('comision')))->get();
    	$listaSedes            = Csede::where('id_csede', '!=', Input::get('sede'))->get();
    	$listaTipoSesiones     = CtipoSesion::where('id_ctipoSesion', '!=', Input::get('tipo'))->get();
    	$listaCaracterSesiones   = CcaracterSesion::where('id_ccaracterSesion', '!=', Input::get('caracter'))->get();


        $descComision 		= Input::get('descComision');
        $comision 			= Input::get('comision');
        $descSede 			= Input::get('descSede');
        $sede 				= Input::get('sede');
        $descTipo 			= Input::get('descTipo');
        $tipo 				= Input::get('tipo');
        $descCaracter		= Input::get('descCaracter');
    	$caracter 			= Input::get('caracter');
    	$fecha 				= Input::get('fecha');
    	$hora 				= Input::get('hora');
    	$horaF 				= Input::get('horaF');
    	$num_sesion 		= Input::get('num_sesion');
    	$anio_sesion 		= Input::get('anio_sesion');
    	$id_tsesion 		= Input::get('id_tsesion');
    	$resceso_inicio 	= Input::get('resceso_inicio');
    	$resceso_fin 		= Input::get('resceso_fin');
		
        return view('Sesiones/EditarSesiones',compact('listaComisiones','listaSedes','listaTipoSesiones','listaCaracterSesiones','comision','sede','tipo','caracter','fecha','hora','horaF','descComision','descSede','descCaracter','descTipo','num_sesion','anio_sesion','id_tsesion','resceso_inicio','resceso_fin'));
    }

    public function editarSesion(Request $request)
    {

    	$this->validate($request, [
            'horaTerm' => 'after_or_equal:horaProg'              
        ]);

        $horaTermRec = Input::get('horaTermRec');
        $horaInicioRec = Input::get('horaInicioRec');

        if(strlen($horaTermRec) >0 || strlen($horaInicioRec)>0 ){
        	//echo $horaTermRec;
        	//echo $horaInicioRec;
 			$this->validate($request, [
                      'horaTermRec' => 'after_or_equal:horaInicioRec|before_or_equal:horaTerm',
                      'horaInicioRec' => 'after_or_equal:horaProg|before_or_equal:horaTerm'                     
            ]);
        }


    	$comision_id       	= Input::get('comision_id');
    	$sede_id           	= Input::get('sede_id');
        $tipoSesion_id     	= Input::get('tipo_id');
        $caracterSesion_id 	= Input::get('caracterSesion_id');
        $fechaProg         	= Input::get('fechaProg');
        $horaProg          	= Input::get('horaProg');
        $horaTerm          	= Input::get('horaTerm');
        $horaInicioRec     	= Input::get('horaInicioRec');
        $horaTermRec       	= Input::get('horaTermRec');
        $tsesion_id 	   	= Input::get('id_tsesion');

    	//echo $fechaProg;
    	$nfechaProg=explode('/',$fechaProg); 
        $fechaProg =$nfechaProg[2].'-'.$nfechaProg[1].'-'.$nfechaProg[0];


    	$sede = Tsesion::find($tsesion_id);
        $sede->id_csede 			= $sede_id;
        $sede->id_ctipoSesion 		= $tipoSesion_id;
        $sede->id_ccaracterSesion 	= $caracterSesion_id;
        $sede->fecha_programada 	= $fechaProg;
        $sede->hora_programada 		= $horaProg;
        $sede->hora_fin_estimada 	= $horaTerm;
        $sede->resceso_inicio 		= $horaInicioRec;
        $sede->resceso_fin 			= $horaTermRec;
        $sede->save();


//Actualiza numero de seion dependiendo la fecha programada.
        DB::statement(DB::raw('SET @numero = 0'));

        DB::update('UPDATE tsesiones 
              set num_sesion=@numero:=@numero+1 
              where id_ccomision= ? 
              and anio_sesion = ?
              order by fecha_programada', [$comision_id, $anio]);

//Concatena 0 al numero  de sesion 1-9.

        DB::update('UPDATE tsesiones
             SET num_sesion=CONCAT(0, num_sesion)
             where id_ccomision=?
             and `num_sesion`<=9' , [$comision_id]);

    	return redirect()->route('seguimientoSesion.index')->with('success','Sesión editada con exito');
    	//return redirect()->route('editarSesion');
    }

    public function CerrarSesion(){
        $listaComisiones  = Ccomision::pluck('descripcion','id_ccomision');
        $listaSesiones    = NULL;
        $id_tsesion       = Input::get('id_tsesion');
        $datosSesion      = Tsesion::where('id_tsesion', '=', $id_tsesion)->get();
        $anio             = $datosSesion[0]->anio_sesion;
        $id_ccomision     = $datosSesion[0]->id_ccomision;
        $N=0;
        foreach($_FILES["archivo"]['tmp_name'] as $key => $tmp_name)
        {   
            if($N < 1){
            if($_FILES["archivo"]["name"][$key]) {//Validamos que el archivo exista

                Tsesion::where('id_tsesion', '=', $id_tsesion)
                        ->update(['id_cestatusSesion' => 5]);//si el archivo de miuta fue adjuntado, cambiamos el estatus de la sesion a 5

                $filename   = $_FILES["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                $source     = $_FILES["archivo"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo   

                $tipo = $_FILES["archivo"]["type"][$key];
                $peso = $_FILES["archivo"]["size"][$key];

                if (($tipo == "application/pdf") && $peso < 15000000){
                    $directorio = 'soportes/'.$anio.'/'.$id_ccomision.'/'.$id_tsesion;//Declaramos un  variable con la ruta donde guardaremos los archivos.
                    //$directorio = 'soportes/'.$anio.'/'.$datosSesion[0]->id_ccomision.'/'.$id_tsesion.'/'. $tema->id_puntoOrdenDia;//Declaramos un  variable con la ruta donde guardaremos los archivos.                     
                    if(!file_exists($directorio)){//Validamos si la ruta de destino existe, en caso de no existir la creamos
                        mkdir($directorio, 0777,true) or die("No se puede crear el directorio de extracci&oacute;n");    
                    }
                    
                    $dir=opendir($directorio); //Abrimos el directorio de destino
                    $target_path = $directorio.'/'.$filename; //Indicamos la ruta de destino, así como el nombre del archivo
                    
                    //Movemos y validamos que el archivo se haya cargado correctamente
                    //El primer campo es el origen y el segundo el destino
                    if(move_uploaded_file($source, $target_path)) { 
                        Tsoporte::create([
                            'id_tsesion'       => $id_tsesion,
                           // 'id_puntoOrdenDia' => $tema->id_puntoOrdenDia,
                            'ruta'             => $target_path,
                            'nombre'           => $filename,
                            'id'               => Auth::user()->id,
                            'estatus'          => 1,
                        ]);
                        
                        } 

                    else{    
                        echo "Ha ocurrido un error, por favor inténtelo de nuevo.<br>";
                    }
                    closedir($dir); //Cerramos el directorio de destino
                    
                }else{
                    return redirect()->route('seguimientoSesiones.index',$id_tsesion)->with('error','Tema Agregado, error al cargar archivo(s). No es un tipo de archivo valido y/o a es demasiado pesado.');
                }
            }else{
                //return view('SeguimientoSesiones.index',compact('listaComisiones','listaSesiones'))->with('error','Es necesario adjuntar el archivo correspondiente a la minuta de esta sesión');
                return redirect()->route('seguimientoSesion.index',compact('listaComisiones','listaSesiones'))->with('error','Es necesario adjuntar el archivo correspondiente a la minuta de esta sesión');
            }
        $N = $N+1;
        }
        }

      $emails = array();
    
      $num_sesion   =$datosSesion[0]->num_sesion;
      
      $datosUsuarios = DB::table('users')
                       ->join('usuario_privilegios', 'usuario_privilegios.id', '=', 'users.id')
                       ->where('usuario_privilegios.id_ccomision', '=', $id_ccomision)
                       ->where('users.status' , '!=', 0)
                       ->where('users.id_cponencia', '!=', 7)
                       ->where('usuario_privilegios.fecha_inicio','<=',$datosSesion[0]->fecha_programada)
                       ->where('usuario_privilegios.fecha_fin','>=',$datosSesion[0]->fecha_programada)
                       //->orwhere('users.id_cponencia' , '=', 7)
                       ->get();

        foreach ($datosUsuarios as $datosUsuarios){
          array_push($emails, $datosUsuarios->email);
        }
        $comision = Ccomision::where('id_ccomision', '=', $id_ccomision)->get();
        $notificacion           = new \stdClass();
        $notificacion->vista    = 'Mail.SesiónTerminada';
        $notificacion->comision = $comision[0]->descripcion;
        $notificacion->num_sesion=$num_sesion;
        $notificacion->anio     =$anio;

        //Mail::to($emails)->send(new NotificacionesMail($notificacion));

        return redirect()->route('seguimientoSesion.index',compact('listaComisiones','listaSesiones'))->with('success','Sesión cerrada con exito');
        //return redirect()->route('seguimientoSesion.index')->with('success','Sesión programada');
    }

    public function consultar(){
       
        $datosrepresentante = Crepresentantep::where('estatus', '=' ,'1')->get();

        $listaComisiones = Ccomision::pluck('estatus','id_ccomision');

        $listaPonencias = Cponencia::pluck('descripcion','id_cponencia');

        return view('Editador.repreUsuario',compact('datosrepresentante','listaPonencias','listaComisiones'));

    }

    public function crear(){ 

        $datosrepresentante = Crepresentantep::all();

        $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');

        $listaPonencias = Cponencia::pluck('descripcion','id_cponencia');

        return view('Editador.creaRe',compact('datosrepresentante','listaPonencias','listaComisiones'));

    }

    public function gene(){

         $ponencia=Input::get('id_cponencia');
        
         $comision=Input::get('id_ccomision');
         
         $titulo=Input::get('titulo');
 
         $nombre=Input::get('nombre');
 
         $paterno=Input::get('paterno');
         
         $materno=Input::get('materno');
 
         $sexo=Input::get('sexo');
 
         $datosrepresentante = new Crepresentantep;
         
         $datosrepresentante->id_cponencia = $ponencia;
 
         $datosrepresentante->id_ccomision = $comision;
 
         $datosrepresentante->titulo = $titulo;
 
         $datosrepresentante->nombre = $nombre;
 
         $datosrepresentante->paterno = $paterno;
 
         $datosrepresentante->materno = $materno;
 
         $datosrepresentante->sexo = $sexo;
 
         $datosrepresentante->estatus = 1;
 
         $datosrepresentante->firma = 1;
 
         $datosrepresentante->save();

         return redirect()->route('consultar')->with('success','Representante registrado correctamente');

    }

    // public function editar(){
        
    //     $datosrepresentante = Crepresentantep::all();

    //     $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');

    //     $listaPonencias = Cponencia::pluck('descripcion','id_cponencia');

    //     return view('Editador.editaRe',compact('datosrepresentante','listaPonencias','listaComisiones'))->with('');

    // }

    public function Eliminar(){

        $id_user = Input::get('idUsuario');

        $datosrepresentante = Crepresentantep::find($id_user);
               
         $datosrepresentante->estatus = 0;

         $datosrepresentante->firma = 0;

         $datosrepresentante->save();

      return redirect()->route('consultar')->with('error','Representante eliminado correctamente');
        
    }

    public function consultapri (){

        $datosprivi = CusuarioPrivilegios::all();

        $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');

        $listaUsuario = User::pluck('nombre','id');

        return view('Editador.UserPri',compact('datosprivi','listaUsuario','listaComisiones'));

    }

    public function CrearPri(){

        $datas = CusuarioPrivilegios::all();

        $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');

        $listaUsuario = User::pluck('nombre','id');

        return view('Editador.creapri',compact('datas','listaUsuario','listaComisiones','fecha'));

    }

    public function Insertpri(){

        $nombre=Input::get('id');

        $comision=Input::get('id_ccomision');

        $fechainicio=Input::get('FechaInicio');

        $fechafin=Input::get('FechaFin');

        $datosprivi = new CusuarioPrivilegios;

        $datosprivi ->id = $nombre;

        $datosprivi->id_ccomision = $comision;

        $datosprivi->fecha_inicio = $fechainicio;

        $datosprivi->fecha_fin = $fechafin;

        $datosprivi->save();

        return redirect()->route('consultapri')->with('success','Creado');

    }

    public function EditarPrivilegios (){

        $datos=Input::get('idUsuario');

        $datas = CusuarioPrivilegios::find ($datos);

        $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');

        $listaUsuario = User::pluck('nombre','id');

        return view('Editador.editapri',compact('datas','listaUsuario','listaComisiones','fecha'));

    }

    public function Updatepri(){
        
        $id=Input::get('idUsuario');

        $comision=Input::get('id_ccomision');

        $defautidcomi=Input::get('defautidcomi');
        
        $Inicio=Input::get('FechaInicio');
        
        $Final=Input::get('FechaFin');

        $usuario = CusuarioPrivilegios::where(['id'=> $id, 'id_ccomision'=>$defautidcomi])

        ->update(['id_ccomision' => $comision,'id' => $id,'fecha_inicio' => $Inicio,'fecha_fin' => $Final]);

         return redirect()->route('consultapri')->with('success','Actualizado');

    }

    public function EliminarPrivilegios (){
     
        $id=Input::get('idUsuario');

        $defautidcomi=Input::get('defautidcomi');

        $usuario = CusuarioPrivilegios::where(['id'=> $id, 'id_ccomision'=>$defautidcomi])->delete();

        return redirect()->route('consultapri')->with('error','Eliminado');
    }
}
