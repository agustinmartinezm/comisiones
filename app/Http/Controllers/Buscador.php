<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Modelos\Catalogos\Ccomision;
use DB;
use Auth;

class Buscador extends Controller
{
    public function buscar(){

    	$id_comision  = Input::get('comision_id');
        $busca_texto  = Input::get('busca');
        $anio_sesion  = Input::get('anioSesion');

        if(Auth::user()->id_crol==1){
            $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');
            $listaComisiones->prepend('Todas las comisiones', '0');
        }else{
            $privilegios = Auth::user()->id;
            $listaComisiones = DB::table('ccomisiones')
                    ->join('usuario_privilegios', 'ccomisiones.id_ccomision', '=', 'usuario_privilegios.id_ccomision')
                    ->join('users', 'usuario_privilegios.id', '=', 'users.id')
                    ->where('users.id', '=', $privilegios)
                    ->get();
        }

        /*if($id_comision == 0){
        	$resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get();	        
        }else{
        	$resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->where('tsesiones.id_ccomision', '=', $id_comision)
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get();
        }*/

        if($id_comision == 0 && (!is_null($anio_sesion))){
            $resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->where('tsesiones.anio_sesion', '=', $anio_sesion)
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get();            
        }elseif($id_comision == 0 && (is_null($anio_sesion))){
            $resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get(); 
        }elseif($id_comision != 0 && (!is_null($anio_sesion))){
            $resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->where('tsesiones.id_ccomision', '=', $id_comision)
            ->where('tsesiones.anio_sesion', '=', $anio_sesion)
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get();   
            /*$resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->where('tsesiones.id_ccomision', '=', $id_comision)
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get();*/
        }elseif($id_comision != 0 && (is_null($anio_sesion))){
            $resultados_busqueda = DB::table('tsesiones')
            ->selectRaw('tsesiones.*, ccomisiones.descripcion as descripcion_comision, ctipos_sesiones.descripcion as descripcion_tipoSesion,ccaracter_sesiones.descripcion as descripcion_caracterSesion, tacuerdos.*')
            ->join('tacuerdos', 'tsesiones.id_tsesion', '=', 'tacuerdos.id_tsesion')
            ->join('ccomisiones', 'tsesiones.id_ccomision', '=', 'ccomisiones.id_ccomision')
            ->join('ctipos_sesiones', 'tsesiones.id_ctipoSesion', '=', 'ctipos_sesiones.id_ctipoSesion')
            ->join('ccaracter_sesiones', 'tsesiones.id_ccaracterSesion', '=', 'ccaracter_sesiones.id_ccaracterSesion')
            ->where('tacuerdos.acuerdo', 'LIKE', "%$busca_texto%")
            ->where('tsesiones.id_ccomision', '=', $id_comision)
            //->where('tsesiones.anio_sesion', '=', $anio_sesion)
            ->orderByRaw('tacuerdos.created_at DESC')
            ->get();   
        }

        if(!$resultados_busqueda->isEmpty()){
        	//print("<pre>".print_r($resultados_busqueda,true)."</pre>");
        	return view('buscador',compact('resultados_busqueda', 'listaComisiones'));
        }else{
        	return redirect()->route('home')->with('error','No se encontraron resultados.');
        }

        
        
    }
}
