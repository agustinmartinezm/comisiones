<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class vueController extends Controller
{
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('/vue');
    }
}
