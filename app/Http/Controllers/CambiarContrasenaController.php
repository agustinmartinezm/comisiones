<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Modelos\Catalogos\Menu_modulo;
use App\Modelos\Catalogos\CusuarioPrivilegios;
use App\Modelos\Transacciones\Tbitacora;
use App\User;
//use App\Modelos\Transacciones\Tbitacora;
use App\Modelos\Transacciones\Tsesion;
use DB;
use Auth;

class CambiarContrasenaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request){    
            $validator = request()->validate([
                'password' => 'required|confirmed|min:6|max:18'
            ]);
    
            $password = Input::get('password');
            
            $user = new User;
            $user->where('usuario', '=', Auth::user()->usuario)
            ->update(['password' => bcrypt($password), 
                     'status' => 1
                    ]);


            if(Auth::user()->id_crol==1){
                $listaBitacora = Tbitacora::orderby('created_at', 'DESC')->take(20)->get();
                return redirect()->route('home',compact('listaBitacora'))->with('success', 'Contraseña cambiada con exito.');
                //return view('/home',compact('listaBitacora'))->with('success', 'contraseña cambiada con exito, por favor reinicie sesion');

            }else{
                $usuario    = Auth::user()->id;     
                $comisiones = CusuarioPrivilegios::where('id','=', $usuario)->get();
                  
                $user_comision = array();

                foreach ($comisiones as $comisiones2) {
                    array_push($user_comision,$comisiones2->id_ccomision);
                }
                
                $listaBitacora = Tbitacora::whereHas('sesion', function ($query) use ($user_comision) {
                $query->whereIn('id_ccomision', $user_comision);
                })->orderby('created_at', 'DESC')->take(20)->get();

                return redirect()->route('home',compact('listaBitacora'))->with('success', 'Contraseña cambiada con exito.');
            }
    }
}