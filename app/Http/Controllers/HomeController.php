<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Catalogos\Menu_modulo;
use App\Modelos\Catalogos\CusuarioPrivilegios;
use App\Modelos\Transacciones\Tbitacora;
use App\Modelos\Transacciones\Tsesion;
use App\Modelos\Catalogos\Ccomision;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$status = DB::select('SELECT status FROM `users` WHERE usuario ='.Auth::user()->usuario);
        //$status1= json_decode(json_encode($status),true);

        //echo $data1[0]['status'];

        if(Auth::user()->status==1){


            if(Auth::user()->id_crol==1){

               $listaBitacora = Tbitacora::orderby('created_at', 'DESC')->take(10)->get();
               $listaComisiones = Ccomision::pluck('descripcion','id_ccomision');
               $listaComisiones->prepend('Todas las comisiones', '0');

               // print_r($listaBitacora);
                return view('/home',compact('listaBitacora','listaComisiones'));

            }else{
                $usuario=Auth::user()->id;     
                $comisiones    = CusuarioPrivilegios::where('id','=', $usuario)->get();
                  
                $user_comision = array();
                $privilegios = Auth::user()->id;
                $listaComisiones = DB::table('ccomisiones')
                    ->join('usuario_privilegios', 'ccomisiones.id_ccomision', '=', 'usuario_privilegios.id_ccomision')
                    ->join('users', 'usuario_privilegios.id', '=', 'users.id')
                    ->where('users.id', '=', $privilegios)
                    ->get();

                foreach ($comisiones as $comisiones2) {

                    array_push($user_comision,$comisiones2->id_ccomision);
                    
                }
         //       print_r($user_comision);


              /*  $listaBitacora = DB::table('tbitacora')
                ->join('tsesiones','tbitacora.id_tsesion', '=', 'tsesiones.id_tsesion')
                ->whereIn('id_ccomision', $user_comision)->get();*/
                
                $listaBitacora = Tbitacora::whereHas('sesion', function ($query) use ($user_comision) {
                $query->whereIn('id_ccomision', $user_comision);
                })->orderby('created_at', 'DESC')->take(20)->get();

                //echo $listaBitacora;

                return view('/home',compact('listaBitacora','listaComisiones'));
            }
        }


        elseif
            (Auth::user()->status==2){

            return view('auth/passwords/restablecer_contrasena');
        }
        elseif(Auth::user()->status==0){

            return view('auth/login');
        }
    }
}
