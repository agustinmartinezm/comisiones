<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User; 
use App\Modelos\Catalogos\Cponencia;
use App\Modelos\Catalogos\Crol;

class AdmUsuarios extends Controller
{
    public function index()
    {
        $listaPonencias = Cponencia::pluck('descripcion','id_cponencia');
        $listaPonencias->prepend('Todas las areas', '0');
        $listaRoles = Crol::pluck('descripcion','id_crol');
        $listaRoles->prepend('Todas los roles', '0');
    	$listaUsuarios = null;
        return view('AdmUsuarios',compact('listaPonencias','listaRoles','listaUsuarios'));
    }

    public function eliminarUsuario()
    {
    	$id_user = Input::get('idUsuario');

    	$usuario = User::find($id_user);
		$usuario->status = 0;
		$usuario->save();

		$listaUsuarios = User::whereNotIn('status', [0])->get();
        return redirect()->route('admUsuarios')->with('success','Usuario eliminado correctamente');
    }

    public function consultaUsuarios(){
        $listaPonencias = Cponencia::pluck('descripcion','id_cponencia');
        $listaPonencias->prepend('Todas las areas', '0');
        $listaRoles = Crol::pluck('descripcion','id_crol');
        $listaRoles->prepend('Todas los roles', '0');

        $id_ponencia = Input::get('ponencia_id');
        $id_rol = Input::get('rol_id');

        if($id_ponencia == 0 && $id_rol == 0){
            $listaUsuarios = User::whereNotIn('status', [0])->get();
        }elseif($id_ponencia == 0 && $id_rol != 0){
            $listaUsuarios = User::whereNotIn('status', [0])->where('id_crol','=', $id_rol)->get();
        }elseif($id_ponencia != 0 && $id_rol == 0){
            $listaUsuarios = User::whereNotIn('status', [0])->where('id_cponencia','=', $id_ponencia)->get();
        }elseif($id_ponencia != 0 && $id_rol != 0){
            $listaUsuarios = User::whereNotIn('status', [0])->where('id_crol','=', $id_rol)->where('id_cponencia','=', $id_ponencia)->get();
        }

        if(!$listaUsuarios->isEmpty()){
            return view('AdmUsuarios',compact('listaPonencias','listaRoles','listaUsuarios'));
        }else{
            $listaUsuarios = null;
            return redirect()->route('admUsuarios')->with('error','No se encontraron resultados.');
        }
        
    }

    public function usuariosDeshabilitadosIndex()
    {
        $listaUsuariosDes = User::where('status', '=',0)->get();
        return view('UsuariosDeshabilitados',compact('listaUsuariosDes'));
    }

    public function habilitarUsuario()
    {
        $id_user = Input::get('idUsuario');

        $usuario = User::find($id_user);
        $usuario->status = 1;
        $usuario->save();

        return redirect()->route('admUsuarios')->with('success','Usuario habilitado correctamente');
    }

    public function resetearContrasenia()
    {
        $id_user = Input::get('idUsuario');

        $usuario = User::find($id_user);
        $usuario->status = 2;
        $usuario->password = bcrypt('12345678');
        $usuario->save();

        return redirect()->route('admUsuarios')->with('success','Contraseña reseteada');
    }

    public function editarUsuario()
    {   

        $id_user = Input::get('idUsuario');
       
        $usuario = User::find($id_user); 
        
        $listaRolesUsuario = Crol::pluck('descripcion','id_crol');

        $listaPonenciasUsuarios = Cponencia::pluck('descripcion','id_cponencia');

         return view('Editador.editarUsuario',compact('usuario','listaRolesUsuario','listaPonenciasUsuarios'));

    }

    public function update(){

        $id=Input::get('idUsuario');
        $nombre=Input::get('nombre');
        $paterno=Input::get('paterno');
        $materno=Input::get('materno');
        $ponencia=Input::get('id_cponencia');
        $rol=Input::get('id_crol');

        $usuario = User::find($id);
        $usuario->nombre = $nombre;
        $usuario->paterno = $paterno;
        $usuario->materno = $materno;
        $usuario->id_cponencia = $ponencia;
        $usuario->id_crol = $rol;
        $usuario->save();

        return redirect()->route('admUsuarios')->with('success','Usuario Actualizado');
    }
}
