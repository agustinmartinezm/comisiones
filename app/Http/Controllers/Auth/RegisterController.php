<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Modelos\Catalogos\Crol;
use App\Modelos\Catalogos\Cponencia;
use App\Modelos\Catalogos\Ccomision;
use App\Modelos\Catalogos\CusuarioPrivilegios;
use Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function showRegistrationForm()
    {
        $listaRoles     = Crol::pluck('descripcion','id_crol');
        $listaPonencias = Cponencia::where('id_cponencia', '!=', 8)->pluck('descripcion','id_cponencia');
        $listaComisiones = Ccomision::where('estatus','=', 1)->get();
        return view('auth.register',compact('listaRoles','listaPonencias','listaComisiones'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre'   => 'required|string|max:255',
            'paterno'  => 'required|string|max:255',
            'materno'  => 'string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'id_crol'  => 'required|integer|max:255',
            'id_cponencia'  => 'required|integer|max:255',
            'usuario'  => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
         User::create([
            'nombre'   => strtoupper($data['nombre']),
            'paterno'  => strtoupper($data['paterno']),
            'materno'  => strtoupper($data['materno']),
            'email'    => $data['email'],
            'id_crol'  => $data['id_crol'],
            'id_cponencia'  => $data['id_cponencia'],
            'usuario'  => $data['usuario'],
            'status'  => 2,
            'password' => Hash::make($data['password']),
        ]);

        $noComisiones = $data['countComisiones'] - 1;
        $id_usuario = User::max('id');

        for($i=1;$i <= $noComisiones; $i++){
            if(isset($data['comision'.$i])){
                CusuarioPrivilegios::create([
                    'id'           => $id_usuario,
                    'id_ccomision' => $data['comision'.$i],
                    'fecha_inicio' => date('Y-m-d'),
                    'fecha_fin'    => '2050-12-31',
                ]);
            }
        }
    }
}
