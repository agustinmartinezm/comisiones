<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tobservacion extends Model
{
    protected $table      = 'tobservaciones';
    protected $primaryKey = 'id_tobservacion';
    protected $fillable   = ['id_tsesion', 'id_cponencia', 'id_puntoOrdenDia', 'id_tacuerdo','observacion','estatus', 'id'];

    public function ponencia(){
		return $this->belongsto('App\Modelos\Catalogos\Cponencia', 'id_cponencia', 'id_cponencia');
	}
}


