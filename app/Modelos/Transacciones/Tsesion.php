<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tsesion extends Model
{
    protected $table      = 'tsesiones';
    protected $primaryKey = 'id_tsesion';
    protected $dates      = ['fecha_programada'];
    protected $fillable   = ['id_ccomision','id_csede','id_ctipoSesion','id_ccaracterSesion','fecha_programada','hora_programada','hora_fin_estimada','resceso_inicio','resceso_fin','id_cestatusSesion','num_sesion','anio_sesion','id'];

	public function comision(){
		return $this->belongsto('App\Modelos\Catalogos\Ccomision', 'id_ccomision', 'id_ccomision');
	}

    public function tipoSesion(){
		return $this->belongsto('App\Modelos\Catalogos\CtipoSesion', 'id_ctipoSesion', 'id_ctipoSesion');
	}

	public function caracterSesion(){
		return $this->belongsto('App\Modelos\Catalogos\CcaracterSesion', 'id_ccaracterSesion', 'id_ccaracterSesion');
	}

	public function sede(){
		return $this->belongsto('App\Modelos\Catalogos\Csede', 'id_csede', 'id_csede');
	}

	public function estatusSesion(){
		return $this->belongsto('App\Modelos\Catalogos\CestatusSesion', 'id_cestatusSesion', 'id_cestatusSesion');
	}

}
