<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class TasignacionRepresentante extends Model
{
    protected $table      = 'tasignacion_representante';
    protected $primaryKey = 'id';
    protected $fillable   = ['fk_tsesion','fk_crepresentantep','estatus'];

    public function sesion(){
        return $this->belongsto('App\Modelos\Transacciones\Tsesion',  'fk_tsesion', 'id_tsesion');
    }

    public function representante(){
        return $this->belongsto('App\Modelos\Catalogos\Crepresentantep',  'fk_crepresentantep', 'id_crepresentantep');
    }
}
