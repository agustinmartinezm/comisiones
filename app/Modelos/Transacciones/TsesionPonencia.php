<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class TsesionPonencia extends Model
{
    protected $table      = 'tsesion_ponencia';
    protected $primaryKey = 'id_tsesion_ponencia';
    protected $fillable   = ['id_tsesion','id_cponencia','estatus','id'];
}
