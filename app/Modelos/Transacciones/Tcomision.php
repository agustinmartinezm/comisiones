<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tcomision extends Model
{
    protected $table      = 'ccomisiones';
    protected $primaryKey = 'id_ccomision';
    //protected $dates      = ['fecha_programada'];
    protected $fillable   = ['id_ccomision','descripcion','estatus'];

	public function comision(){
		return $this->belongsto('App\Modelos\Catalogos\Ccomision', 'id_ccomision', 'id_ccomision');
	}
}