<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tcomentario extends Model
{
    protected $table      = 'tcomentarios';
    protected $primaryKey = 'id_tcomentario';
    protected $fillable   = ['id_tsesion','comentario','estatus','id'];

	public function usuario(){
		return $this->belongsto('App\User', 'id', 'id');
	}

	public function sesion(){
		return $this->belongsto('App\Modelos\Transacciones\Tsesion', 'id_tsesion', 'id_tsesion');
	}   
}
