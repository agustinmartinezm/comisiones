<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tacuerdo extends Model
{
    protected $table      = 'tacuerdos';
    protected $primaryKey = 'id_tacuerdo';
    protected $fillable   = ['id_tsesion','id_puntoOrdenDia','acuerdo', 'version', 'estatus', 'id'];

    public function observacion(){
		return $this->belongsto('App\Modelos\Transacciones\Tobservacion', 'id_tacuerdo', 'id_tacuerdo');
	}

	
}
