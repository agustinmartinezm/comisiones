<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class TpuntoOrdenDia extends Model
{
    protected $table      = 'tpuntos_orden_dia';
    protected $primaryKey = 'id_puntoOrdenDia';
    protected $fillable   = ['id_tsesion','id_ctema','numero_punto','punto','id_cprioridad','id_cestatusPunto','id'];

    public function sesion(){
		return $this->belongsto('App\Modelos\Transacciones\Tsesion', 'id_tsesion', 'id_tsesion');
	}

	public function tema(){
		return $this->belongsto('App\Modelos\Catalogos\Ctema', 'id_ctema', 'id_ctema');
	}

	public function prioridad(){
		return $this->belongsto('App\Modelos\Catalogos\Cprioridad', 'id_cprioridad', 'id_cprioridad');
	}

	public function estatusPunto(){
		return $this->belongsto('App\Modelos\Catalogos\CestatusPunto', 'id_cestatusPunto', 'id_cestatusPunto');
	}

	public function acuerdo(){
		return $this->belongsto('App\Modelos\Transacciones\Tacuerdo', 'id_puntoOrdenDia', 'id_puntoOrdenDia');
	}
}
