<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tsoporte extends Model
{
    protected $table      = 'tsoportes';
    protected $primaryKey = 'id_tsoporte';
    protected $fillable   = ['id_tsesion','id_puntoOrdenDia','ruta','nombre','id','estatus'];
}
