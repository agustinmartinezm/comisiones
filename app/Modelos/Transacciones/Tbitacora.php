<?php

namespace App\Modelos\Transacciones;

use Illuminate\Database\Eloquent\Model;

class Tbitacora extends Model
{
    protected $table      = 'tbitacora';
    protected $primaryKey = 'id_tbitacora';
    protected $fillable   = ['id_cmovimiento','id','id_tsesion'];

    public function movimiento(){
		return $this->belongsto('App\Modelos\Catalogos\Cmovimiento', 'id_cmovimiento', 'id_cmovimiento');
	}

	public function usuario(){
		return $this->belongsto('App\User', 'id', 'id');
	}

	public function sesion(){
		return $this->belongsto('App\Modelos\Transacciones\Tsesion', 'id_tsesion', 'id_tsesion');
	}   
}


