<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Crol extends Model
{
    protected $table = 'croles';
    protected $fillable = ['descripcion'];
}
