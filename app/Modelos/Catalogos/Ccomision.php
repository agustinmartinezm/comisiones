<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Ccomision extends Model
{
    protected $table = 'ccomisiones';
    protected $fillable = ['descripcion','estatus'];
}
