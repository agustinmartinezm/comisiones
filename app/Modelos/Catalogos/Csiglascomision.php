<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Csiglascomision extends Model
{
    protected $table = 'csiglascomision';
    protected $fillable = ['id_ccomision','descripcion'];
}