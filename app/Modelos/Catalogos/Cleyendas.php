<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cleyendas extends Model
{
    protected $table = 'tleyendas';
    protected $fillable = ['leyenda','estatus'];
}