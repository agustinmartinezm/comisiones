<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Menu_modulo extends Model
{
    protected $table = 'menu_modulos';
    protected $fillable = ['modulo','ruta','estatus'];
}
