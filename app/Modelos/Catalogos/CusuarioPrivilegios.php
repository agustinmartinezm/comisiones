<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CusuarioPrivilegios extends Model
{
    protected $table = 'usuario_privilegios';
    protected $fillable = ['id', 'id_ccomision', 'fecha_inicio', 'fecha_fin' , 'created_at' ,'updated_at'];

    public function comision(){
        return $this->belongsto('App\Modelos\Catalogos\Ccomision', 'id_ccomision', 'id_ccomision');
    }
     
    public function usuario(){
        return $this->belongsto('App\User', 'id', 'id');
    }

}