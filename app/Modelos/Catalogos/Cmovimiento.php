<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cmovimiento extends Model
{
    protected $table = 'cmovimiento';
    protected $fillable = ['descripcion'];

}
