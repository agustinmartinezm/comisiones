<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cponencia extends Model
{
    protected $table = 'cponencias';
    protected $fillable = ['descripcion'];
}
