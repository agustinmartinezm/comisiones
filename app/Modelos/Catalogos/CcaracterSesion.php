<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CcaracterSesion extends Model
{
    protected $table = 'ccaracter_sesiones';
    protected $fillable = ['descripcion','estatus'];
}
