<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class TemaPrivilegio extends Model
{
    protected $table = 'tema_privilegios';
    protected $fillable = ['id_ccomision','id_ctema','estatus'];

    public function comision(){
		return $this->belongsto('App\Modelos\Catalogos\Ctema', 'id_ccomision', 'id_ccomision');
	}
}
