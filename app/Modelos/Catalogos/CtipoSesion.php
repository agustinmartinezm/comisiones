<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CtipoSesion extends Model
{
    protected $table = 'ctipos_sesiones';
    //protected $primaryKey = 'id_ctipoSesion';
    protected $fillable = ['descripcion','estatus'];
}
