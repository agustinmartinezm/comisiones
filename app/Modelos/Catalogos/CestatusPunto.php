<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CestatusPunto extends Model
{
    protected $table = 'cestatus_puntos';
    protected $fillable = ['descripcion','estatus'];
}
