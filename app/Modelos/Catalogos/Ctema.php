<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Ctema extends Model
{
    protected $table = 'ctemas';
    protected $fillable = ['descripcion'];
}
