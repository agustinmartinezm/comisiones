<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CpuntosOrdenDia extends Model
{
    protected $table = 'tpuntos_orden_dia';
    protected $fillable = ['punto'];
}