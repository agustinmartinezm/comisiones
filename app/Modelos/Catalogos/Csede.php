<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Csede extends Model
{
    protected $table = 'csedes';
    protected $fillable = ['descripcion','estatus'];
}
