<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cacuerdos extends Model
{
    protected $table = 'tacuerdos';
    protected $fillable = ['id_tsesion','id_puntoOrdenDia','acuerdo'];
}