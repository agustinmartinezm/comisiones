<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CestatusSesion extends Model
{
    protected $table = 'cestatus_sesiones';
    protected $fillable = ['descripcion','estatus'];
}
