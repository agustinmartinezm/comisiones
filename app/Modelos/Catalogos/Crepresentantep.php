<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Crepresentantep extends Model
{
    protected $table = 'crepresentantesp';

    protected $primaryKey = 'id_crepresentantep';

    protected $fillable = ['id_crepresentantep','id_cponencia','id_ccomision', 'titulo', 'nombre', 'paterno', 'materno', 'estatus','sexo'];

    public function ponencia(){
        return $this->belongsto('App\Modelos\Catalogos\Cponencia', 'id_cponencia', 'id_cponencia');
    }

    public function comision(){
        return $this->belongsto('App\Modelos\Catalogos\Ccomision', 'id_ccomision', 'id_ccomision');
    }
}
