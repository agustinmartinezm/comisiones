<?php

namespace App\Modelos\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cprioridad extends Model
{
    protected $table = 'cprioridades';
    protected $fillable = ['descripcion'];
}
